#include <userint.h>
#include <utility.h>
#include <cviogl.h>
#include <toolbox.h>

#include "Def.h"
#include "ReadSaveJpeg.h"
#include "ReadSavePng.h"

#include "SwapEndian.h"
#include "LargeImage.h"
#include "Externs.h"

int OglCtrl;
BOOL DoOgl=FALSE, OglPossible=TRUE;	// Because default is 8 bits
static void *OglMatrix=NULL;
static int Plot=0;
	
///////////////////////////////////////////////////////////////////////////////
// FUNCTION: SetOglControlAttributes 
// Define some default params (only once)
///////////////////////////////////////////////////////////////////////////////
void SetOglControlAttributes(void) {
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_LIGHTING_ENABLE, 1);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_LIGHT_SELECT, 1);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_LIGHT_ENABLE, 1);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_LIGHT_DISTANCE, 3.0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_LIGHT_SPECULAR, 0.7);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_VIEW_DISTANCE,2.5);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_VIEW_DIRECTION, OGLVAL_AUTO_XY);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_PROJECTION_TYPE,OGLVAL_PERSPECTIVE);
	
	// Setup Axis Labels    
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_XNAME_VISIBLE,0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_YNAME_VISIBLE,0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_ZNAME_VISIBLE,0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_XNAME,"X axis");
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_YNAME,"Y axis");
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_ZNAME,"File value / Elevation");
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_XLABEL_VISIBLE,0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_YLABEL_VISIBLE,0);
	OGLSetCtrlAttribute(PnlOgl, OglCtrl, OGLATTR_ZLABEL_VISIBLE,0);

	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_ZSTART, -ZBox/200);
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_ZSIZE,   ZBox/100);
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_YZ_PLANE_VISIBLE, 0);
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_XZ_PLANE_VISIBLE, 0);

	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_YREVERSE, OGLVAL_TRUE);
}

void SetZBox(void) {
	static double LastZBox=0;
	if (ZBox==LastZBox) return;
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_ZSTART, -ZBox/200);
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_ZSIZE,   ZBox/100);
	LastZBox=ZBox;
	if (DoOgl) DrawOgl();
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: AddToOgl 
// Add a value to the OGL matrix. The type of value is given by FilePixDepth
///////////////////////////////////////////////////////////////////////////////
void AddToOgl(void* Value, int i, int j) {
	void *V;
	static int LastWidth=0, LastHeight=0, LastPixDepth=0;
	
	if (ViewWidth!=LastWidth or ViewHeight!=LastHeight or FilePixDepth!=LastPixDepth) {
		OglMatrix= realloc(OglMatrix, (LastWidth=ViewWidth)*(LastHeight=ViewHeight)*(LastPixDepth=FilePixDepth)/8);
		if (OglMatrix==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
	}
	
	V= Swapped ? SwapEndian(Value, FilePixDepth/8) : Value;
	switch (FilePixDepth) {
		case  8: if (Signed)  ((char* )OglMatrix)[i+j*ViewWidth]=*(         char*)V;
				else ((unsigned char* )OglMatrix)[i+j*ViewWidth]=*(unsigned char*)V;
				break;	// B->B
		case 16: if (Signed)  ((short* )OglMatrix)[i+j*ViewWidth]=*(         short*)V;
				else ((unsigned short* )OglMatrix)[i+j*ViewWidth]=*(unsigned short*)V;
				break;	// W->B
		case 24: DoOgl=FALSE; Breakpoint(); break;								// RGB->RGB, not possible
		case 32: switch (ImgPixDepth+FromFloat) {
					case 8: if (Signed)   ((long* )OglMatrix)[i+j*ViewWidth]=*(         long*)V;
							else ((unsigned long* )OglMatrix)[i+j*ViewWidth]=*(unsigned long*)V;
							break;	// L->B
					case 32:Breakpoint(); break;		// RGBA->RGBA, not possible
					case 9: ((float*)OglMatrix)[i+j*ViewWidth]=*(float*)V; break;	// F->D
				} break;
		
		case 64: ((double*)OglMatrix)[i+j*ViewWidth]=*(double*)V; break;				// D->D
		default: DoOgl=FALSE; Breakpoint();
	}
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: DrawOgl
// Draws an OpenGL 3D surface plot
// IN: Matrix: matrix of ViewHeight x ViewWidth size and type whatever the file is
///////////////////////////////////////////////////////////////////////////////
void DrawOgl(void) {
	int Type=0, Prog, Res=0;
	static int IDM=0;
	double Ratio;

	// Plot characteristics
	int Visible=TRUE, ColorScheme=OGLVAL_COLOR_SPECTRUM, NumColMap=0;
	static ColorMapEntry *ColorMap=NULL;
	int HighColor=0xFFFFFF, XsizeColor=0, YsizeColor=0;
	BOOL InterpolateColorMap=FALSE;
	int *ColorArray=NULL;
	BOOL ProjectXY=FALSE, VProjectYZ=FALSE, ProjectXZ=FALSE, ProjectOnly=FALSE;
	double PointSize=0.;
	BOOL PointSmooth=TRUE;
	int PointColor=OGLVAL_RED;
	BOOL WireSmooth=TRUE;
	int WireStyle=OGLVAL_NONE, WireColor=OGLVAL_RED;
	int SurfaceStyle=OGLVAL_SMOOTH, SurfaceColor=OGLVAL_CYAN, SurfaceTransparency=0, SurfaceShininess=40;
	double SurfaceSpecular=0.5;
	long i,j, X, Y, Xmap, Ymap;
	char Dest[4], *Pos, *V;
	unsigned long Seek;
	
	if (!DoOgl) {
		HidePanel(PnlOgl);
		Plot=0;
		return;
	}
	
	SetWaitCursor (TRUE);
	SetZoom(Zoom);	// Just to change the display
	
	if (Plot!=0) {	// Memorize plot characteristics
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSIZE,  &XSize);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_YSTART, &YStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &YSize);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &ZStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &ZSize);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PROJECTION_TYPE, &XProjectionType);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_DIRECTION,  &ViewDirection);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_LATITUDE,   &ViewLatitude);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_LONGITUDE,  &XViewLongitude);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_AUTO_DISTANCE,&ViewAutoDist);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_DISTANCE,   &ViewDistance);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_CENTERX, &ViewCenterX);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_CENTERX, &ViewCenterY);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_VIEW_CENTERX, &ViewCenterZ);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHTING_ENABLE, &Lighting);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_AMBIENT_COLOR,   &AmbientColor);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_SELECT,	   &LightSelect);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_ENABLE,	   &LightEnable);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_COLOR,	   &LightColor);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_AMBIENT,   &AmbientFactor);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_DIFFUSE,   &DifuseFactor);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_SPECULAR,  &SpecularFactor);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_LATITUDE,  &XLightLatitude);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_LONGITUDE, &XLightLongitude);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_DISTANCE,  &LightDistance);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_ATTENUATION,&XLightAttenuation);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LIGHT_SPOT_ENABLE,&XSpotEnable);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		OGLGetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART, &XStart);
//		
		
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PLOT_VISIBLE,    &Visible);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLOR_SCHEME,    &ColorScheme);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORMAP_NUMCOLORS,&NumColMap);
		ColorMap=(ColorMapEntry*)realloc(ColorMap, NumColMap*sizeof(ColorMapEntry));
		if (ColorMap!=NULL) 
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORMAP,        ColorMap);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORMAP_HICOLOR,&HighColor);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORMAP_INTERP, &InterpolateColorMap);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORARRAY_XSIZE,&XsizeColor);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORARRAY_YSIZE,&YsizeColor);
//		ColorArray=(int*)realloc(ColorMap, XsizeColor*YsizeColor*sizeof(int));
//		if (ColorMap!=NULL) 
//		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_COLORARRAY,      ColorArray);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_XY_PLANE, &ProjectXY);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_YZ_PLANE, &VProjectYZ);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_XZ_PLANE, &ProjectXZ);	   
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECTIONS_ONLY,&ProjectOnly);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SIZE,	     &PointSize);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SMOOTH,    &PointSmooth);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_COLOR,     &PointColor);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_WIRE_STYLE,	     &WireStyle);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SMOOTH,    &WireSmooth);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_COLOR,     &WireColor);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_STYLE,   &SurfaceStyle);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_COLOR,   &SurfaceColor);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_TRANSPARENCY,    &SurfaceTransparency);
		OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_SPECULAR_FACTOR, &SurfaceSpecular);
	    OGLGetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_SHININESS, &SurfaceShininess);
	}
	
	if (MapFile!=NULL) {
		Prog = CreateProgressDialog ("Preparing 3D mapping", "Percent Complete", 1, VAL_FULL_MARKERS, "");
		ColorArray=realloc(ColorArray, (XsizeColor=ViewWidth)*(YsizeColor=ViewHeight)*sizeof(int));
		for (j=0; j<ViewHeight; j++) {
			Y=Ymin+(unsigned long)(Zoom*j);
			Ymap=YViewToMap(Xmin, Y);
			UpdateProgressDialog (Prog, 100*j/ViewHeight, 0);
			for (i=0, X=Xmin; i<ViewWidth; i++, X+=Zoom) {
				Pos=(char*)&(ColorArray[j*ViewWidth+i]);
				Xmap=XViewToMap(X,Y);
				Seek=MapPosition(Xmap, Ymap);
				if (0</*=*/Seek and Seek<MapSize and 
					0==fseek (MapFile, Seek, SEEK_SET)) {
					fread(Dest, MapPixDepth/8, 1, MapFile);
					if (MapSwapped) V=SwapEndian(Dest, MapPixDepth/8);
					else V=Dest;
					switch (ImgPixDepth) {
						case 32:*(int*)Pos=*(int*)V; break;
						case 24:Pos[3]=0;
								Pos[2]=V[2];
								Pos[1]=V[1];
								Pos[0]=V[0];	break;
						case 8: Pos[3]=0; 
								Pos[2]=Pos[1]=Pos[0]=V[0];	break;
					}
				} else *(int*)Pos=MissingColor;
			}
		}

		if (IDM!=0) DiscardBitmap (IDM); IDM=0;
		Res = NewBitmap (XsizeColor*4, 32, XsizeColor, YsizeColor, NULL, (unsigned char*)ColorArray, NULL, &IDM);
		Res=CanvasDrawBitmap(PnlView, VIEW_MAP, IDM, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);

		DiscardProgressDialog (Prog);
		ColorScheme=OGLVAL_COLORARRAY;
		InterpolateColorMap=InterpolateColors;
	} else if (ColorScheme==OGLVAL_COLORARRAY) {
		ColorScheme=OGLVAL_COLOR_SPECTRUM;
		free(ColorArray); ColorArray=NULL;
		XsizeColor=YsizeColor=0;
	}

	
	
	///////////////////////////////////////////////////////////////////////////
	// Delete any existing plots
	if (Plot!=0) OGLDeletePlot (PnlOgl, OglCtrl, Plot, 0);
	Plot=0;
	
	if (!DoOgl) { SetWaitCursor(FALSE); return; }
	
	// Give plot data to control
	switch (FilePixDepth) {
		case  8: Type=Signed ? OGLVAL_CHAR          : OGLVAL_UNSIGNED_CHAR;	         break; // B->B
		case 16: Type=Signed ? OGLVAL_SHORT_INTEGER : OGLVAL_UNSIGNED_SHORT_INTEGER; break;	// W->B
		case 24: DoOgl=FALSE; Breakpoint(); return;		// RGB->RGB, not possible
		case 32: switch (ImgPixDepth+FromFloat) {
					case 8: Type=Signed ? OGLVAL_INTEGER : OGLVAL_UNSIGNED_INTEGER;	break;	// L->B
					case 32:DoOgl=FALSE; Breakpoint(); return;	// RGBA->RGBA, not possible
					case 9: Type=OGLVAL_FLOAT; break;	// F->D
				} break;
		case 64: Type=OGLVAL_DOUBLE; break;
		default: DoOgl=FALSE; Breakpoint(); return;
	}

	Plot=OGLPlot3DUniform (PnlOgl, OglCtrl, OglMatrix, ViewWidth, ViewHeight, Type, 1., 0., 1., 0.);

	// Setup plot attributes                                         
	Ratio=(double)ViewHeight/ViewWidth;
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSTART,  -0.5/sqrt(Ratio));
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_XSIZE,      1/sqrt(Ratio));
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_YSTART,  -0.5*sqrt(Ratio));
	OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_PLOTAREA_YSIZE,        sqrt(Ratio));

	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PLOT_VISIBLE,    Visible);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_XY_PLANE, ProjectXY);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_YZ_PLANE, VProjectYZ);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECT_TO_XZ_PLANE, ProjectXZ);	   
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_PROJECTIONS_ONLY,ProjectOnly);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SIZE,	     PointSize);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SMOOTH,    PointSmooth);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_COLOR,     PointColor);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_WIRE_STYLE,	     WireStyle);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_SMOOTH,    WireSmooth);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_POINT_COLOR,     WireColor);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_STYLE,   SurfaceStyle);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_COLOR,   SurfaceColor);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_TRANSPARENCY,    SurfaceTransparency);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_SPECULAR_FACTOR, SurfaceSpecular);
	OGLSetPlotAttribute (PnlOgl, OglCtrl, Plot, OGLATTR_SURFACE_SHININESS, SurfaceShininess);
		
	OGLSetPlotColorScheme (PnlOgl, OglCtrl, Plot, ColorScheme, ColorMap, NumColMap, HighColor,
							   InterpolateColorMap, ColorArray, XsizeColor, YsizeColor);


	// Refresh
	OGLRefreshGraph(PnlOgl, OglCtrl);

	SetWaitCursor(FALSE); 
}


///////////////////////////////////////////////////////////////////////////////
// CALLBACK: cb_OglProperties 
// Property box for OpenGL plot
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_OglProperties (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			OGLPropertiesPopup(PnlOgl, OglCtrl);
			break;
	}
	return 0;
}

int CVICALLBACK cb_OglClose(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT: ActivateOGL(FALSE); break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: PrintOgl 
///////////////////////////////////////////////////////////////////////////////
void PrintOgl(void) {
	SetWaitCursor (TRUE);
	OGLCopyScaledCtrlBitmap (PnlOgl, OglCtrl, 0, -1, -1, -1, -1);
//	PrintPanel (PnlOgl, "", 1, VAL_FULL_PANEL, 1);
	PrintCtrl (PnlOgl, OglCtrl, "", 1, 1);
	SetWaitCursor (FALSE);
}

void CVICALLBACK cbm_PrintOgl (int menuBar, int menuItem, void *callbackData, int panel) {
	PrintOgl();
}

void CVICALLBACK cbm_SaveOglAs (int menuBar, int menuItem, void *callbackData, int panel) {
	SetWaitCursor (TRUE);
	OGLCopyScaledCtrlBitmap (PnlOgl, OglCtrl, 0, -1, -1, -1, -1);
	SaveAs(PnlOgl, OglCtrl, "Save 3D view as...", NULL, 0,0,0,0);
	SetWaitCursor (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
// CALBACK: cbp_OGL 
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_OGL(int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int pWidth, pHeight, width, height, top, left;
	int Bitmap=0;
	
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute (PnlOgl, ATTR_HEIGHT, &pHeight);
			GetPanelAttribute (PnlOgl, ATTR_WIDTH,  &pWidth);
			
			GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_TOP, &top);        
			GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_LEFT, &left);        
			SetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_WIDTH, width=pWidth-left);        
			SetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_HEIGHT, height=pHeight-top);        
			
			OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_TOP, top);
			OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_LEFT, left);
			OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_WIDTH, width);
			OGLSetCtrlAttribute (PnlOgl, OglCtrl, OGLATTR_HEIGHT, height);
			
			if (DoOgl) OGLRefreshGraph(PnlOgl,OglCtrl);
	        break;
		
		case EVENT_RIGHT_CLICK:
			break;

		case EVENT_RIGHT_DOUBLE_CLICK:
//			RemovePopup (1);
//            OGLPropertiesPopup(PnlOgl, OglCtrl);
			break;

		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.


				case VAL_MENUKEY_MODIFIER | 'P':	// Print OGL View
				case VAL_MENUKEY_MODIFIER | 'p':	// Print OGL View
		            PrintOgl();
		            return 1;

				case VAL_MENUKEY_MODIFIER | 'R':	// Reset OGL View
				case VAL_MENUKEY_MODIFIER | 'r':	// Reset OGL View
		            SetOglControlAttributes();
		            Plot=0;			// Cancel current plot
		   			if (DoOgl) DrawOgl();
		            return 1;

				case VAL_MENUKEY_MODIFIER | 'S':	// Save OGL View
				case VAL_MENUKEY_MODIFIER | 's':	// Save OGL View
					SetWaitCursor (TRUE);
					OGLCopyScaledCtrlBitmap (PnlOgl, OglCtrl, 0, -1, -1, -1, -1);
					SaveAs(PnlOgl, OglCtrl, "Save 3D view as...", NULL, 0,0,0,0);
		            SetWaitCursor (FALSE);
					return 1;

				case VAL_MENUKEY_MODIFIER | 'C':	// Copy to clipboard
				case VAL_MENUKEY_MODIFIER | 'c':	// Copy to clipboard
					SetWaitCursor (TRUE);
					OGLCopyScaledCtrlBitmap (PnlOgl, OglCtrl, 0, -1, -1, -1, -1);
					GetCtrlBitmap (PnlOgl, OglCtrl, 0, &Bitmap);
					ClipboardPutBitmap (Bitmap);
					DiscardBitmap (Bitmap);
		            SetWaitCursor (FALSE);
					return 1;


				case VAL_F1_VKEY:
					MessagePopup("OpenGL 3D View Help",
						"This is available for monochrome files only (byte, word, long, float, double).\n"
						"The area represented is the same as the View window.\n"
						"The data used is the raw data from the file,\n"
						"with possible byte swap and nothing else.\n"
						"Actions:\n"
						"  Rotate = Mouse Move\n"
						"  Zoom = Ctrl + Mouse Move\n"
						"  Pan = Shift + Mouse Move\n"
						"  Properties = Right Click (many options in there)."
					); 
					return 1;
			}
			// Key not recognised, passed to Main window
			SetActivePanel(PnlCtrl);
			FakeKeystroke (eventData1);
			return 1;
	}
	return 0;
}


void CVICALLBACK cbm_CopyOGL (int menuBar, int menuItem, void *callbackData, int panel) {
	int Bitmap=0;
	SetWaitCursor (TRUE);
	OGLCopyScaledCtrlBitmap (PnlOgl, OglCtrl, 0, -1, -1, -1, -1);
	GetCtrlBitmap (PnlOgl, OglCtrl, 0, &Bitmap);
	ClipboardPutBitmap (Bitmap);
	DiscardBitmap (Bitmap);
	SetWaitCursor (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: ActivateOgl 
///////////////////////////////////////////////////////////////////////////////
void ActivateOGL(BOOL Activate) {
	DoOgl=Activate;
	(DoOgl?DisplayPanel:HidePanel)(PnlOgl);
	SetMenuBarAttribute(Menu, MENU_FILE_OGL,       ATTR_CHECKED, DoOgl);
	SetMenuBarAttribute(Menu, MENU_FILE_PRINTOGL,  ATTR_DIMMED, !DoOgl);
	SetMenuBarAttribute(Menu, MENU_FILE_SAVEOGLAS, ATTR_DIMMED, !DoOgl);
	SetMenuBarAttribute(Menu, MENU_FILE_COPYOGL,   ATTR_DIMMED, !DoOgl);
	SetMenuBarAttribute(Menu, MENU_FILE_MAPPING,   ATTR_DIMMED, !DoOgl);
	if (DoOgl) {
		NeedViewUpdate=TRUE;
	    OglCtrl = OGLConvertCtrl(PnlOgl, OGL_PICTURE);
	    SetOglControlAttributes();		// Setup CVIOGL control once and for all    
		CreateView();
	} else {
		OGLDiscardCtrl (PnlOgl, OglCtrl);
		Plot=OglCtrl=0;
	}
}

// Activate OGL view
void CVICALLBACK cbm_OGL (int menuBar, int menuItem, void *callbackData, int panel) {
	ActivateOGL(!DoOgl);
}

