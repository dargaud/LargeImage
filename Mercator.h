#ifndef _MERCATOR
#define _MERCATOR

extern void Mercator(double Lat, double Lon, double *X, double *Y);
extern void InverseMercator(double X, double Y, double *Lat, double *Lon);
extern void Stereographic(double Lat, double Lon, double *X, double *Y);
extern void InverseStereographic(double X, double Y, double *Lat, double *Lon);

#endif
