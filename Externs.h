#ifndef _EXTERNS
#define _EXTERNS

#include <stdio.h>

// required by LargeImage.c
extern int GridColor, GridBcgColor;
extern void SetWaypoint(int X, int Y);
extern void DisplayWaypoints(void);

// Required by Grid.c
extern int PnlCtrl, PnlView, Menu, PnlGrid;
extern unsigned long Width, Height;
extern long Xmin, Xmax, Ymin, Ymax, Zoom;
extern float UnitsPerPixel;
extern char Unit[255];
extern unsigned long ViewWidth, ViewHeight;
extern char RegPath[], FileName[MAX_FILENAME_LEN];
extern BOOL NeedViewUpdate, AutoUpdate;
extern void CreateView(void);

// Required by Waypoints.c
extern int PnlWP, PnlView;
extern long Xmin, Xmax, Ymin, Ymax, Zoom;
extern unsigned long Width, Height;
extern void NextPanel(int Panel);
extern void PreviousPanel(int Panel);
extern void DisplayBox(void);
extern void CreateView(void);
extern BOOL NeedViewUpdate, AutoUpdate;
extern char RegPath[];
extern BOOL UsingGrid(void);


// Exported by Grid.c
#define x2f(x) (( (x)-UpLeftX)/(LowRightX-UpLeftX)*Width)	// flat map meter to file pos
#define y2f(y) ((-(y)-UpLeftY)/(LowRightY-UpLeftY)*Height)
#define Xf2v(X) (((X)-Xmin)/Zoom)	// file pos to view pixel
#define Yf2v(Y) (((Y)-Ymin)/Zoom)

#define Xv2f(X) ((X)*Zoom+Xmin)	// View pixel to file pos
#define Yv2f(Y) ((Y)*Zoom+Ymin)
#define f2x(X) ( (X)*(LowRightX-UpLeftX)/Width +UpLeftX)	// file pos to flat map meter
#define f2y(Y) (-(Y)*(LowRightY-UpLeftY)/Height-UpLeftY)

// Grid overlay
extern int DoGrid, DoGridLabel;
extern int GridColor, GridBcgColor;
extern double UpLeftX, UpLeftY,	// Corners of the grid as found in hdr file
			LowRightX, LowRightY;

extern void DrawGridOverlay(void);
extern void SetGridClick(int X, int Y, double *rLat, double *rLon);
extern void GetGridPos(double Lat, double Lon, int *X, int *Y);


// Exported by OglView.c
// Only implemented those that keep same height/width
#define EFFECT_NONE   0x0
#define EFFECT_FLIP   0x1
#define EFFECT_MIRROR 0x2		// Binary combine

extern int PnlView, PnlProp, PnlCtrl, PnlHist, PnlOpt, PnlOgl, PnlMap, Menu;

extern unsigned long ViewWidth, ViewHeight, ViewRowBytes, FilePixDepth, ImgPixDepth;
extern BOOL Align32, NeedViewUpdate, FromFloat, Swapped, Signed, FromFloat, FromDouble;
long Xmin, Xmax, Ymin, Ymax, Zoom;

extern char PathName[MAX_PATHNAME_LEN], FileName[MAX_FILENAME_LEN], SavePath[MAX_PATHNAME_LEN];
extern char MapFileName[MAX_PATHNAME_LEN];

extern int JpgComp, PngComp, StretchIgnore;
extern float PngGamma, PngResolution, StretchThreshold;
extern BOOL PngInterlace, PngAlpha32;
extern char TxtAuthor[256], TxtDescription[256];
extern BOOL AutoUpdate;

extern int OglCtrl;
extern BOOL DoOgl, OglPossible;

extern void ActivateOGL(BOOL Activate);
extern void SetOglControlAttributes(void);
extern void SetZBox(void);

extern void AddToOgl(void* Value, int i, int j);
extern void DrawOgl(void);

extern void CreateView(void);
extern void NextPanel(int Panel);
extern void PreviousPanel(int Panel);

extern void SaveAs(int Panel, int Control, const char *Comment,
			unsigned char* Bits, int RowBytes, int PixDepth, int Width, int Height);

extern FILE *MapFile;
extern unsigned long MapSize, MapRowBytes, MapPixDepth,
					MapWidth, MapHeight, MapSkip;
extern BOOL MapAlign32, MapSwapped, InterpolateColors;
extern float ZBox;

extern int MissingColor, MapEffect;
extern double MapXXScale, MapXYScale, MapYXScale, MapYYScale, MapXOffset, MapYOffset;

extern char RegPath[];

extern void SetZoom(int Z);

///////////////////////////////////////////////////////////////////////////
// Byte position in the file, using the Effect
#define XViewToMap(X,Y) ( (X)*MapXXScale+(Y)*MapXYScale+MapXOffset )
#define YViewToMap(X,Y) ( (X)*MapYXScale+(Y)*MapYYScale+MapYOffset )
#define MapDirect(X, Y) ( MapSkip + MapRowBytes*(Y) + (X)*MapPixDepth/8 )
#define MapPosition(X, Y) MapDirect( MapEffect & EFFECT_MIRROR ? MapWidth -1-(X) : (X),\
							         MapEffect & EFFECT_FLIP   ? MapHeight-1-(Y) : (Y))

#endif

