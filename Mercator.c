// Not actually used anywhere. See mapx

#include <ansi_c.h>
#include "toolbox.h"

#define Re 6400		// Equatorial radius
#define LonConf	0	// Conformal Longitude
double  Lon0=0.,	// Central Longitude
		Lat1=-70.;	// Central Latitude


// Cylindrical conformal
// Mercator projection, spherical assumption
void Mercator(double Lat, double Lon, double *X, double *Y) {
	*X=Re*(Lon-Lon0);
	*Y=Re*log(tan(PI/4+Lat/2));
}


void InverseMercator(double X, double Y, double *Lat, double *Lon) {
	*Lat=HALF_PI-2*atan(exp(-Y/Re))	;
	*Lon=X/Re+Lon0;
}


// Azimutal plane
// stereographic projection (spherical assumption, north polar aspect)
void Stereographic(double Lat, double Lon, double *X, double *Y) {
	*X= 2*Re*tan(PI/4-Lat/2)*sin(Lon-Lon0);
	*Y=-2*Re*tan(PI/4-Lat/2)*cos(Lon-Lon0);
}


void InverseStereographic(double X, double Y, double *Lat, double *Lon) {
	*Lat=2*atan2(sqrt(X*X+Y*Y), 2*Re)-HALF_PI;
	*Lon=Lon0+atan2(X, Y);
}


// UPS ?

void UPS(double Lat, double Lon, double *X, double *Y) {
	double Radius=Re*cos(Lat)/(1-EULER*EULER*sin(Lat)*sin(Lat))/cos(LonConf);
	double k=2*Radius/(1+sin(Lat1)*sin(Lat)+cos(Lat1)*cos(Lat)*cos(Lon-Lon0));
	*X= k*cos(Lat)*sin(Lon-Lon0);
	*Y= k*(cos(Lat1)*sin(Lat)-sin(Lat1)*cos(Lat)*cos(Lon-Lon0));
}


void InverseUPS(double X, double Y, double *Lat, double *Lon) {
	double Rho=sqrt(X*X+Y*Y);
	double c=2*atan2(Rho, 2*Re);
	*Lat=asin(cos(c)*sin(Lat1)+Y*sin(c)*cos(Lat1)/Rho);
	*Lon=Lon0+atan2(X*sin(c), Rho*cos(Lat1)*cos(c)-Y*sin(Lat1)*sin(c));
}



void DrawGrid(void) {
	double LatStep=2., LonStep=10.;
	double Lat, Lon, X, Y;
	
	for (Lon=0; Lon<=360; Lon+=LonStep)
		for (Lat=-90.-LatStep; Lat<=-60.; Lat+=LatStep)
			UPS(DEG_TO_RAD(Lat), DEG_TO_RAD(Lon), &X, &Y);
}
