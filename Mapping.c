#include "wintools.h"
#include <ansi_c.h>
#include <utility.h>
#include <userint.h>

#include "Def.h"
#include "Primes.h"
#include "LargeImage.h"
#include "Externs.h"

static char MapPath[MAX_PATHNAME_LEN]="";
FILE *MapFile=NULL;

unsigned long MapSize=0, MapRowBytes=0, MapPixDepth=8,
					MapWidth=0, MapHeight=0, MapSkip=0;
BOOL MapAlign32=FALSE, MapSwapped=FALSE, InterpolateColors=FALSE;
int MapEffect=EFFECT_NONE;

int MissingColor=0x404040;
double MapXXScale=1., MapXYScale=0., MapYXScale=1., MapYYScale=1., 
		MapXOffset=0., MapYOffset=0.;

///////////////////////////////////////////////////////////////////////////////
// Warning, limited to 2Gb
static unsigned long GetTotMappingSize(void) {
	MapRowBytes = MapWidth*MapPixDepth/8;
	if (MapAlign32) MapRowBytes = ((MapRowBytes+3)/4)*4;	// 32 bits boundary alignment
	return MapSkip + MapRowBytes*MapHeight;
}


// Update the Properties display
static void DisplayMappingProperties(void) {
	SetCtrlVal(PnlMap, MAP_MISSING, MissingColor);
	SetCtrlVal(PnlMap, MAP_INTERPOLATE, InterpolateColors);

	SetCtrlVal(PnlMap, MAP_ALIGN,  MapAlign32);
	SetCtrlVal(PnlMap, MAP_SWAP,   MapSwapped);
	SetCtrlVal(PnlMap, MAP_EFFECT, MapEffect);

	SetCtrlVal(PnlMap, MAP_XX_SCALE, MapXXScale);
	SetCtrlVal(PnlMap, MAP_XY_SCALE, MapXYScale);
	SetCtrlVal(PnlMap, MAP_YY_SCALE, MapYYScale);
	SetCtrlVal(PnlMap, MAP_YX_SCALE, MapYXScale);
	SetCtrlVal(PnlMap, MAP_X_OFFSET, MapXOffset);
	SetCtrlVal(PnlMap, MAP_Y_OFFSET, MapYOffset);

	SetCtrlVal(PnlMap, MAP_SIZE, (double)MapSize);
	SetCtrlVal(PnlMap, MAP_SKIP_FOOTER, (double)MapSize - GetTotMappingSize());
	GenerateWHmap(MapSize-MapSkip);

	SetCtrlVal(PnlMap, MAP_WIDTH,  MapWidth);
	SetCtrlVal(PnlMap, MAP_HEIGHT,   MapHeight);
	SetCtrlVal(PnlMap, MAP_SKIP_HEADER, MapSkip);
}

void CVICALLBACK cbm_ApplyMapping (int menuBar, int menuItem, void *callbackData, int panel) {
	char DriveName[MAX_PATHNAME_LEN]="", DirName[MAX_DIRNAME_LEN]="", RegRegFile[255]="";
	int Err, S, MapPixDepth, MapPossibleWH;

	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, 0);

	if (MapFile!=NULL) {	// We remove the mapping and return
		fclose(MapFile);	// File was open
		MapFile=NULL;
		SetMenuBarAttribute (Menu, MENU_FILE_MAPPING, ATTR_CHECKED, FALSE);
		// NeedViewUpdate=TRUE;
		if (AutoUpdate) DrawOgl();
		return;
	}
	if (!DoOgl) return;
	
	
	if (MapPath[0]!='\0') {
		SplitPath (MapPath, DriveName, DirName, MapFileName);
		strcat(DriveName, DirName);
	}

	SetMenuBarAttribute (Menu, MENU_FILE_MAPPING, ATTR_CHECKED, FALSE);
	if (VAL_EXISTING_FILE_SELECTED==FileSelectPopup (DriveName, "*.raw;*.bin", "*.raw;*.bin",
		 			"Select mapping file to apply to 3D view", VAL_SELECT_BUTTON,
					 0, 0, 1, 0, MapPath)) {
		if (MapFile!=NULL) fclose(MapFile);	// File was open
		MapFile=NULL;
		
		SplitPath(MapPath, DriveName, DirName, MapFileName);
		SetCtrlVal (PnlMap, MAP_FILENAME, MapFileName);
		if (0!=GetFileSize(MapPath, (long*)&MapSize) or 
			(MapFile = fopen (MapPath, "rb")) ==NULL) {
			MessagePopup("Error", "Could not read mapping file");
			return;
		}
		SetMenuBarAttribute (Menu, MENU_FILE_MAPPING, ATTR_CHECKED, TRUE);

		sprintf(RegRegFile, "%s\\%s\\%s", RegPath, FileName, MapFileName);
			// Save to Registry
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapAlign32", (unsigned long*)&MapAlign32, 1);
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapSwapped", (unsigned long*)&MapSwapped, 1);
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapEffect",  (unsigned long*)&MapEffect, 1);

		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapXXScale", (char*)&MapXXScale, sizeof(double), &S);
		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapXYScale", (char*)&MapXYScale, sizeof(double), &S);
		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapYYScale", (char*)&MapYYScale, sizeof(double), &S);
		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapYXScale", (char*)&MapYXScale, sizeof(double), &S);
		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapXOffset", (char*)&MapXOffset, sizeof(double), &S);
		Err=WT_RegReadBinary(WT_KEY_HKCU, RegRegFile, "MapYOffset", (char*)&MapYOffset, sizeof(double), &S);

		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapPixDepth", (unsigned long*)&MapPixDepth, 1);
		SetCtrlVal(PnlMap, MAP_PIXDEPTH, MapPixDepth);
		cb_MappingPixDepth (PnlProp, PROP_PIXDEPTH, EVENT_COMMIT, NULL, 0, 0);
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapPossibleWH", (unsigned long*)&MapPossibleWH, 1);
		SetCtrlVal(PnlMap, MAP_POSSIBLE_WH, MapPossibleWH);
		cb_MappingPossibleWH (PnlProp, PROP_POSSIBLE_WH, EVENT_COMMIT, NULL, 0,0);

		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapWidth", (unsigned long*)&MapWidth, 1);
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapHeight",(unsigned long*)&MapHeight, 1);
		Err=WT_RegReadULong (WT_KEY_HKCU, RegRegFile, "MapSkip",  (unsigned long*)&MapSkip, 1);
		
		
		DisplayMappingProperties();
		InstallPopup (PnlMap);
	} else {
		if (MapFile!=NULL) fclose(MapFile);	// File was open
		MapFile=NULL;
	}
}


int CVICALLBACK cb_MappingPixDepth (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int MapPixDepth;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_PIXDEPTH, &MapPixDepth);

			switch (MapPixDepth) {
/* B->B */		case  8:MapPixDepth=8; MapSwapped=FALSE;  break;
/* RGB  */		case 24:FilePixDepth=24; break;
/* RGBA */		case 32:FilePixDepth=32; break;
			}

			SetCtrlVal(PnlMap, MAP_SWAP, MapSwapped); 

			GetTotMappingSize();
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Pixel Depth of mapping file",
				"This is the number of bits used to represent each pixel.\n"
				"Currently 3 modes are supported:\n"
				"- 8 bits greyscale\n"
				"- 3x8 bits color RGB\n"
				"- 4x8 bits color RGB + ignored Alpha channel\n"
				"represent the color of the mapping file for each pixel of the 3D view.\n"
			);
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingPossibleWH (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int W;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_POSSIBLE_WH, &W);
			if (SQmap==0 or W==0) return 0;
			
			SetCtrlVal(PnlMap, MAP_WIDTH, MapWidth=W);
			SetCtrlVal(PnlMap, MAP_HEIGHT, MapHeight=SQmap/W);
			GetTotMappingSize();
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Possible MapWidth and MapHeight",
						"The program attempts to determine the possible width and height of the mapping file\n"
						"based on the file size, the skipped header, the pixel depth and the alignement\n"
						"The sizes closest to a square are marked with < > symbols");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingWidth (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_WIDTH, &MapWidth);
			GetTotMappingSize();
			SetCtrlVal(PnlMap, MAP_HEIGHT, MapHeight=(MapSize-MapSkip)/MapRowBytes);
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Width of the mapping file",
				"You can enter the width of the mapping file (in pixels) manually if you know it");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingHeight (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_HEIGHT, &MapHeight);
			GetTotMappingSize();
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Height of the mapping file",
				"You can enter the height of the mapping file (in pixels) manually if you know it");
			break;
	}
	return 0;
}


int CVICALLBACK cb_MappingSkip (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_SKIP_HEADER, &MapSkip);
			GetTotMappingSize();
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Header skip",
				"Number of bytes to skip at the begining of the mapping file.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingSkipF (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Footer skip",
				"Number of bytes that are being skipped at the end of the file.\n"
				"If this value is negative, then Width or Height are too high.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingAlign (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_ALIGN, &MapAlign32);
			GetTotMappingSize();
			DisplayMappingProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("32 bit alignment",
				"If this is set, then the number of bytes per row (RowBytes)\n"
				"is rounded off to the highest 32 bit value.\n"
				"For instance if width=6 and MapPixDepth=8 the file will be organized like this:\n"
				"if no alignement: 111111222222333333...\n"
				"if alignement:    111111PP222222PP333333PP...\n"
				"Where 1 are the bytes of the first line, 2 the 2nd line...\n"
				"And P are ignored padding bytes\n");
			break;
	}
	return 0;
}


int CVICALLBACK cb_MappingSwap (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_SWAP, &MapSwapped);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Swap bytes",
				"This inverts the order of the bytes being read.\n"
				"It is usefull if reading binary data from a system with a different byte ordering.\n"
				"For instance the PC is little-endian (aka LSB) where the least significant byte is first.\n"
				"The Macs are big-endian (aka MSB), the opposite.\n"
				"So you should activate this when reading from across systems.\n"
				"For instance, it will read BGR instead of RGB");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingEffect (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlMap, MAP_EFFECT, &MapEffect);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Effect",
						"Applies simple symetries to the image.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingCancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int State;
	switch (event) {
		case EVENT_COMMIT:
			State = SetBreakOnLibraryErrors (0);
			RemovePopup (1);
			SetBreakOnLibraryErrors (State);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

int CVICALLBACK cb_MappingOK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if ((double)MapSize < GetTotMappingSize()) {
				MessagePopup("Warning !",
					"The width and height that you've set will try to read past the end of the file !");
				return 0;
			}
			GetCtrlVal(PnlMap, MAP_MISSING, &MissingColor);
			GetCtrlVal(PnlMap, MAP_INTERPOLATE, &InterpolateColors);
			GetCtrlVal(PnlMap, MAP_XX_SCALE, &MapXXScale);
			GetCtrlVal(PnlMap, MAP_XY_SCALE, &MapXYScale);
			GetCtrlVal(PnlMap, MAP_YY_SCALE, &MapYYScale);
			GetCtrlVal(PnlMap, MAP_YX_SCALE, &MapYXScale);
			GetCtrlVal(PnlMap, MAP_X_OFFSET, &MapXOffset);
			GetCtrlVal(PnlMap, MAP_Y_OFFSET, &MapYOffset);

			if (strlen(MapFileName)>0) {
				char RegRegFile[MAX_FILENAME_LEN*3]="";
				int Err, MapPixDepth, MapPossibleWH; 
				
				sprintf(RegRegFile, "%s\\%s\\%s", RegPath, FileName, MapFileName);
					// Save to Registry
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapAlign32", *(unsigned long*)&MapAlign32, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapSwapped", *(unsigned long*)&MapSwapped, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapEffect",  *(unsigned long*)&MapEffect, 1);

				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapXXScale", (char*)&MapXXScale, sizeof(double));
				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapXYScale", (char*)&MapXYScale, sizeof(double));
				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapYYScale", (char*)&MapYYScale, sizeof(double));
				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapYXScale", (char*)&MapYXScale, sizeof(double));
				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapXOffset", (char*)&MapXOffset, sizeof(double));
				Err=WT_RegWriteBinary(WT_KEY_HKCU, RegRegFile, "MapYOffset", (char*)&MapYOffset, sizeof(double));

				GetCtrlVal(PnlMap, MAP_PIXDEPTH, &MapPixDepth);
				GetCtrlVal(PnlMap, MAP_POSSIBLE_WH, &MapPossibleWH);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapPixDepth", *(unsigned long*)&MapPixDepth, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapPossibleWH", *(unsigned long*)&MapPossibleWH, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapWidth", *(unsigned long*)&MapWidth, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapHeight",*(unsigned long*)&MapHeight, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegRegFile, "MapSkip",  *(unsigned long*)&MapSkip, 1);
			}
			
			RemovePopup(0);
			if (DoOgl) DrawOgl();
			return 0;
		
	}
	return 0;
}


int CVICALLBACK cb_MissingColor (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Missing Color",
						"If the conversion coordinate of the mapping transform falls outside\n"
						"of the mapping file, use this color as replacement.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_MappingTransform(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Mapping transform",
						"How to get the coordinates of a point within the Mapping file\n"
						"knowing the coordinates in the Source file.\n"
						"The affine transform is as follow:\n"
						"Xmap=X.Sxx+Y.Sxy+Xoffset\n"
						"Ymap=X.Syx+Y.Syy+Yoffset\n"
						"Note that if the file match (they have the same number of rows and columns),\n"
						"You will probably want to use Sxx=1, Sxy=0, Syx=0, Syy=1 for scale\n"
						"and 0 for both offsets.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_MappingInterpolate(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Interpolate colors",
						"Interpolate colors of the mapping.");
			break;
	}
	return 0;
}

