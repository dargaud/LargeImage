#include <ansi_c.h>

#include "Def.h"
#include "LargeImage.h"
#include "Externs.h"

#include "Primes.h"

unsigned long SQ=0, SQmap;


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: PrimeFactors
// PURPOSE: Return a list of prime factors of N
// IN: N: number to split
// OUT: Factor: list created of size NbFact
// NOTE: This does not return 1 nor the number itself.
void PrimeFactors(long N, tFact **Factor, int *NbFact) {
	register long I=5, K=2;
	*NbFact=0;
	
	while (N%2==0) {
		N/=2;
		if ((*Factor)==NULL) { 
			(*Factor)=(tFact*)realloc(*Factor, ++*NbFact * sizeof(tFact)); 
			(*Factor)[*NbFact-1].Prime=2;
			(*Factor)[*NbFact-1].Exp=0;
		}
		(*Factor)[*NbFact-1].Exp++;
	}
	
	while (N%3==0) {
		N/=3;
		if ((*Factor)==NULL or (*Factor)[*NbFact-1].Prime!=3) { 
			(*Factor)=(tFact*)realloc(*Factor, ++*NbFact * sizeof(tFact)); 
			(*Factor)[*NbFact-1].Prime=3;
			(*Factor)[*NbFact-1].Exp=0;
		}
		(*Factor)[*NbFact-1].Exp++;
	}

	do {
		if (N%I==0) {
			if ((*Factor)==NULL or (*Factor)[*NbFact-1].Prime!=I) { 
				(*Factor)=(tFact*)realloc(*Factor, ++*NbFact * sizeof(tFact)); 
				(*Factor)[*NbFact-1].Prime=I;
				(*Factor)[*NbFact-1].Exp=0;
			}
			(*Factor)[*NbFact-1].Exp++;
			N/=I;
		} else {
			I+=K; 
			K=6-K;
		}
	} while (N>=2);
}


///////////////////////////////////////////////////////////////////////////////
// Compute A^B
unsigned long ipow(unsigned long A, unsigned long B) {
	unsigned long Tot=A, i;
	switch (B) {
		case 0: return 1;
		case 1: return A;
		case 2: return A*A;
		case 3: return A*A*A;
		case 4: return (A*A)*(A*A);	// hope the compiler optimizes this
		default: for (i=1; i<B; i++) Tot*=A;	// Yeah, I know, should be optimized based on primes...
				return Tot;
	}
}

static int CmpULong(const void *element1, const void *element2) {
	if ( *(unsigned long*)element1 < *(unsigned long*)element2 ) return -1;
	if ( *(unsigned long*)element1 > *(unsigned long*)element2 ) return 1;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// RH is assumed to be the product of RowBytes x Height and we want those values
void GenerateWH(long RH) {
	tFact *Factor=NULL, *Duplicate=NULL;
	int NbFact=0, i, Pos;
	char Str[100];
	unsigned long Sub, Possibilities=1, *ListW=NULL;
	static unsigned long LastSQ=0, Limit;
	
	SQ=0;
	
	switch (FilePixDepth) {
		case 8: if (Align32 or FromFloat or FromDouble) {
					if (RH%4==0) SQ=RH/4;
					else ;		// Cannot be divided
				} else SQ=RH; 
				break;
		case 16: if (Align32) {
					if (RH%4==0) SQ=RH/2;
					else ;		// Cannot be divided
				} else SQ=RH/2; 
				break;
		case 24: if (Align32) {
					if (RH%4==0) SQ=RH/4;
					else ;		// Cannot be divided
				} else 
					if (RH%3==0) SQ=RH/3;
					else ;		// Cannot be divided
				break;
		case 32: if (RH%4==0) SQ=RH/4;
				else ;		// Cannot be divided
				break;
		case 64: if (RH%8==0) SQ=RH/8;
				else ;		// Cannot be divided
				break;
	}	
	
	if (SQ==LastSQ) return;	// no change
	LastSQ=SQ;
	
	ClearListCtrl (PnlProp, PROP_POSSIBLE_WH);
	InsertListItem (PnlProp, PROP_POSSIBLE_WH, -1, "Manual", 0);
	
	if (SQ==0) return;	// No factors

	PrimeFactors(SQ, &Factor, &NbFact);
	if (Factor==NULL) return;	// No factors

//	for (i=0; i<NbFact; i++) {
//		sprintf(Str, "%d^%d", Factor[i].Prime, Factor[i].Exp);
//		InsertListItem (PnlProp, PROP_POSSIBLE_WH, -1, Str, Factor[i].Prime);
//	}		

	Possibilities=1;
	for (i=0; i<NbFact; i++) Possibilities*=Factor[i].Exp+1;
	
	ListW=Calloc(Possibilities, unsigned long);

	Duplicate=Calloc(NbFact, tFact);
	memcpy(Duplicate, Factor, NbFact*sizeof(tFact));
	
	Pos=Possibilities=0;
	Limit=sqrt(sqrt(SQ))+1;	// To eliminate cases like 100000x2 pixels...
	
	while (Pos<NbFact) {
		Sub=1;
		for (i=0; i<NbFact; i++) Sub*= ipow(Duplicate[i].Prime, Duplicate[i].Exp);
		if (Sub>Limit and SQ/Sub>Limit) 		// Eliminate small cases
			ListW[Possibilities++]=Sub;		
				
		Pos=0;
		while (Pos<NbFact) {	// Check all possibilities
			if (Duplicate[Pos].Exp>0) { --Duplicate[Pos].Exp; break; }
			else {
				Duplicate[Pos].Exp=Factor[Pos].Exp;
				Pos++;
			}
		}		
	}
	
	qsort (ListW, Possibilities, sizeof(unsigned long), CmpULong);
	for (i=0; i<Possibilities; i++) {
		if (i<Possibilities-1 and i>0 and 
			ListW[i]+SQ/ListW[i]<=ListW[i+1]+SQ/ListW[i+1] and
			ListW[i]+SQ/ListW[i]<=ListW[i-1]+SQ/ListW[i-1]) 
			sprintf(Str, "< %d x %d >", ListW[i], SQ/ListW[i]);
		else sprintf(Str, "%d x %d", ListW[i], SQ/ListW[i]);
		InsertListItem (PnlProp, PROP_POSSIBLE_WH, -1, Str, ListW[i]);
	}
	
	free(Factor);
	free(Duplicate);
	free(ListW);
}

///////////////////////////////////////////////////////////////////////////////
// RH is assumed to be the product of RowBytes x Height and we want those values
void GenerateWHmap(long RH) {
	tFact *Factor=NULL, *Duplicate=NULL;
	int NbFact=0, i, Pos;
	char Str[100];
	unsigned long Sub, Possibilities=1, *ListW=NULL;
	static unsigned long LastSQ=0, Limit;
	
	SQ=0;
	
	switch (MapPixDepth) {
		case 8: if (MapAlign32) {
					if (RH%4==0) SQmap=RH/4;
					else ;		// Cannot be divided
				} else SQmap=RH; 
				break;
		case 24: if (MapAlign32) {
					if (RH%4==0) SQmap=RH/4;
					else ;		// Cannot be divided
				} else 
					if (RH%3==0) SQmap=RH/3;
					else ;		// Cannot be divided
				break;
		case 32: if (RH%4==0) SQmap=RH/4;
				else ;		// Cannot be divided
				break;
	}	
	
	if (SQmap==LastSQ) return;	// no change
	LastSQ=SQmap;
	
	ClearListCtrl (PnlMap, MAP_POSSIBLE_WH);
	InsertListItem(PnlMap, MAP_POSSIBLE_WH, -1, "Manual", 0);
	
	if (SQmap==0) return;	// No factors

	PrimeFactors(SQmap, &Factor, &NbFact);
	if (Factor==NULL) return;	// No factors

//	for (i=0; i<NbFact; i++) {
//		sprintf(Str, "%d^%d", Factor[i].Prime, Factor[i].Exp);
//		InsertListItem (PnlMap, MAP_POSSIBLE_WH, -1, Str, Factor[i].Prime);
//	}		

	Possibilities=1;
	for (i=0; i<NbFact; i++) Possibilities*=Factor[i].Exp+1;
	
	ListW=Calloc(Possibilities, unsigned long);

	Duplicate=Calloc(NbFact, tFact);
	memcpy(Duplicate, Factor, NbFact*sizeof(tFact));
	
	Pos=Possibilities=0;
	Limit=sqrt(sqrt(SQmap))+1;	// To eliminate cases like 100000x2 pixels...
	
	while (Pos<NbFact) {
		Sub=1;
		for (i=0; i<NbFact; i++) Sub*= ipow(Duplicate[i].Prime, Duplicate[i].Exp);
		if (Sub>Limit and SQmap/Sub>Limit) 		// Eliminate small cases
			ListW[Possibilities++]=Sub;		
				
		Pos=0;
		while (Pos<NbFact) {	// Check all possibilities
			if (Duplicate[Pos].Exp>0) { --Duplicate[Pos].Exp; break; }
			else {
				Duplicate[Pos].Exp=Factor[Pos].Exp;
				Pos++;
			}
		}		
	}
	
	qsort (ListW, Possibilities, sizeof(unsigned long), CmpULong);
	for (i=0; i<Possibilities; i++) {
		if (i<Possibilities-1 and i>0 and 
			ListW[i]+SQmap/ListW[i]<=ListW[i+1]+SQmap/ListW[i+1] and
			ListW[i]+SQmap/ListW[i]<=ListW[i-1]+SQmap/ListW[i-1]) 
			sprintf(Str, "< %d x %d >", ListW[i], SQmap/ListW[i]);
		else sprintf(Str, "%d x %d", ListW[i], SQmap/ListW[i]);
		InsertListItem (PnlMap, MAP_POSSIBLE_WH, -1, Str, ListW[i]);
	}
	
	free(Factor);
	free(Duplicate);
	free(ListW);
}

