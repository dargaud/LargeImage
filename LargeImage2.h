/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2002. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  CTRL                            1       /* callback function: cbp_Ctrl */
#define  CTRL_X_MIN                      2       /* callback function: cb_MinMax */
#define  CTRL_X_MAX                      3       /* callback function: cb_MinMax */
#define  CTRL_Y_MIN                      4       /* callback function: cb_MinMax */
#define  CTRL_Y_MAX                      5       /* callback function: cb_MinMax */
#define  CTRL_MOVE_UP_LEFT               6       /* callback function: cb_Move */
#define  CTRL_MOVE_UP                    7       /* callback function: cb_Move */
#define  CTRL_MOVE_UP_RIGHT              8       /* callback function: cb_Move */
#define  CTRL_MOVE_LEFT                  9       /* callback function: cb_Move */
#define  CTRL_UPDATE_VIEW                10      /* callback function: cb_UpdateView */
#define  CTRL_MOVE_RIGHT                 11      /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN_LEFT             12      /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN                  13      /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN_RIGHT            14      /* callback function: cb_Move */
#define  CTRL_ZOOM                       15      /* callback function: cb_Zoom */
#define  CTRL_QUIT                       16      /* callback function: cb_Quit */
#define  CTRL_THUMBNAIL                  17      /* callback function: cb_Thumbnail */
#define  CTRL_VIEW_SIZE                  18
#define  CTRL_TIMER_ARGV                 19      /* callback function: cb_Argv */

#define  HIST                            2       /* callback function: cbp_Histograms */
#define  HIST_HISTF                      2       /* callback function: cb_HistF */
#define  HIST_HISTV                      3       /* callback function: cb_HistV */
#define  HIST_HISTT                      4       /* callback function: cb_HistT */

#define  PFILT                           3
#define  PFILT_NAME                      2       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_UL                    3       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_U                     4       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_UR                    5       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_L                     6       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_C                     7       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_R                     8       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_DL                    9       /* callback function: cb_FilterHelp */
#define  PFILT_MAT_D                     10      /* callback function: cb_FilterHelp */
#define  PFILT_MAT_DR                    11      /* callback function: cb_FilterHelp */
#define  PFILT_DIV                       12      /* callback function: cb_FilterHelp */
#define  PFILT_BIAS                      13      /* callback function: cb_FilterHelp */
#define  PFILT_CANCEL                    14      /* callback function: cb_FilterCancel */
#define  PFILT_OK                        15      /* callback function: cd_FilterOK */

#define  PROP                            4       /* callback function: cbp_Properties */
#define  PROP_SIZE                       2
#define  PROP_POSSIBLE_WH                3       /* callback function: cb_PossibleWH */
#define  PROP_WIDTH                      4       /* callback function: cb_Width */
#define  PROP_HEIGHT                     5       /* callback function: cb_Height */
#define  PROP_PIXDEPTH                   6       /* callback function: cb_PixDepth */
#define  PROP_SKIP_HEADER                7       /* callback function: cb_Skip */
#define  PROP_SKIP_FOOTER                8       /* callback function: cb_SkipF */
#define  PROP_ALIGN                      9       /* callback function: cb_Align */
#define  PROP_EFFECT                     10      /* callback function: cb_Effect */
#define  PROP_OK                         11      /* callback function: cb_OK */
#define  PROP_CANCEL                     12      /* callback function: cb_Cancel */
#define  PROP_FILENAME                   13

#define  VIEW                            5       /* callback function: cbp_View */
#define  VIEW_VIEW                       2       /* callback function: cb_View */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU                            1
#define  MENU_FILE                       2
#define  MENU_FILE_OPEN                  3       /* callback function: cbm_Open */
#define  MENU_FILE_SAVEAS                4       /* callback function: cbm_SaveAs */
#define  MENU_FILE_PROPERTIES            5       /* callback function: cbm_Properties */
#define  MENU_FILE_SEPARATOR             6
#define  MENU_FILE_QUIT                  7       /* callback function: cbm_Quit */
#define  MENU_FILTER                     8
#define  MENU_FILTER_SKIP                9       /* callback function: cbm_Skip */
#define  MENU_FILTER_RESAMPLE            10      /* callback function: cbm_Resample */
#define  MENU_FILTER_SEP                 11
#define  MENU_FILTER_HISTOGRAMS          12      /* callback function: cbm_Histograms */
#define  MENU_FILTER_SEPARATOR           13
#define  MENU_HELP                       14
#define  MENU_HELP_QUICKHELP             15      /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE               16      /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                 17      /* callback function: cbm_About */


     /* Callback Prototypes: */ 

void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Histograms(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Open(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Properties(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Resample(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveAs(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Skip(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK cbp_Ctrl(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Histograms(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Properties(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_View(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Align(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Argv(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Cancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Effect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FilterCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FilterHelp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Height(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistV(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MinMax(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Move(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PixDepth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PossibleWH(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Skip(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SkipF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Thumbnail(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_UpdateView(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_View(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Width(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Zoom(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cd_FilterOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
