// MODULE:  Grid.c
// PURPOSE: Draws a grid using the mapx library, an mpp or gdp file has to be provided
// The Map Equatorial Radius parameter should be in meters, 6378273 for Earth
// Also the Unit/Pix and the Unit (km or m) should be set in the FIle Properties

#include <ansi_c.h>
#include <utility.h>
#include <userint.h>
#include <toolbox.h>
#include "wintools.h"

#include "isin.h"	// Added GD
#include "define.h"
#include "keyval.h"
//#define mapx_c_
#include "mapx.h"
//#include "maps.h"

#include "Def.h"
#include "LargeImage.h"
#include "Externs.h"


static char GridName[MAX_FILENAME_LEN];

int DoGrid=FALSE, DoGridLabel=FALSE;
int GridColor=VAL_GREEN, GridBcgColor=VAL_DK_GRAY;
static char GridFile[MAX_PATHNAME_LEN];
static mapx_class *the_map=NULL;
static double Lstep;	// Precision drawing lines
static double LatInterval=5, LonInterval=20;
static double LatMin=-89, LonMin=-180;
static double LatMax=89,  LonMax=180;
static double LatPref=0, LonPref=45;
double UpLeftX=0, UpLeftY=0,	// Corners of the grid
		LowRightX=0, LowRightY=0;

static int Margin=5;		// Number of pixel of vicinity 


///////////////////////////////////////////////////////////////////////////////
// Return TRUE if the grid is in use
BOOL UsingGrid(void) {
	return !(the_map==NULL or UpLeftX==LowRightX or UpLeftY==LowRightY);
}

///////////////////////////////////////////////////////////////////////////////
// X/Y In file coordinates
// You can pass NULL for rLat and rLon
// Return -1/-1 in case of problem
void SetGridClick(int X, int Y, double *rLat, double *rLon) {
	double x, y, Lat, Lon, testx, testy;
	char Str[255]="";
	int status, iLat, iLon, mLat, mLon, sLat, sLon;
	BOOL Disp=!(the_map==NULL or UpLeftX==LowRightX or UpLeftY==LowRightY);

	SetCtrlAttribute(PnlCtrl, CTRL_CLICK_LAT_LON, ATTR_VISIBLE, Disp);
	if (rLat!=NULL) *rLat=0;
	if (rLon!=NULL) *rLon=0;
	if (!Disp) return;
	
	x=f2x(X);
	y=f2y(Y);
	status = inverse_xy_mapx(the_map, x, y, &Lat, &Lon);
	if (status==0 and !IsInvalid(Lat) and !IsInvalid(Lon)) {
		iLat=abs(Lat); mLat=(fabs(Lat)-iLat)*60; sLat=((fabs(Lat)-iLat)*60-mLat)*60;
		iLon=abs(Lon); mLon=(fabs(Lon)-iLon)*60; sLon=((fabs(Lon)-iLon)*60-mLon)*60;
		sprintf(Str, "Click: %d�%02d'%02d\"%c %d�%02d'%02d\"%c",
				iLat, mLat, sLat, Lat>0?'N':'S',
				iLon, mLon, sLon, Lon>0?'E':'W');
//		status = forward_xy_mapx(the_map, Lat, Lon, &testx, &testy);
		if (rLat!=NULL) *rLat=Lat;
		if (rLon!=NULL) *rLon=Lon;
	} else strcpy(Str, "Click: Invalid lat/lon");

	SetCtrlVal(PnlCtrl, CTRL_CLICK_LAT_LON, Str);
}

///////////////////////////////////////////////////////////////////////////////
// Convert to file position
// Warning, may be outside of file position
void GetGridPos(double Lat, double Lon, int *X, int *Y) {
	double x=0, y=0;
	int status;
	
	if (the_map==NULL or UpLeftX==LowRightX or UpLeftY==LowRightY) { 
		*X=*Y=-1; return; }
	status = forward_xy_mapx(the_map, Lat, Lon, &x, &y);
	if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
		*X=x2f(x);	// Meters to file pixel to view pixel
		*Y=y2f(y);
	} else *X=*Y=-1;
}

#define Dist(P,Q) sqrt((double)(Q.x-P.x)*(Q.x-P.x)+(double)(Q.y-P.y)*(Q.y-P.y))

///////////////////////////////////////////////////////////////////////////////
// Draws the grid and then the labels
void DrawGridOverlay(void) {
	double Lat, Lon;
	double x,y;		// In meters (or the unit of Map Equatorial Radius in the mpp file)
	double Px,Py;	// in screen pixels
	double Conv=UnitsPerPixel*((toupper(Unit[0])=='K' and toupper(Unit[1])=='M' and Unit[2]=='\0') ? 1000 : 1);
	int status, i;
	Point P, Dest, P0={-1,-1};
	char Str[255];
	
	// Sanity checks
	if (UpLeftX==LowRightX or UpLeftY==LowRightY) {
		MessagePopup("Error", "UpLeftX must be different from LowRightX\n"
				"Likewise UpLeftY and LowRightY.\n"
				"Go back to the [File][Grid] menu and change that.");
		DoGrid=DoGridLabel=FALSE;
		return;
	}
	if (!DoGrid and !DoGridLabel or the_map==NULL) return;

	// Pen colors
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR, GridColor);
	SetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_PEN_COLOR, GridColor);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_STYLE, VAL_SOLID);
	SetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_PEN_STYLE, VAL_SOLID);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, GridBcgColor);	// For text
	SetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_PEN_FILL_COLOR, GridBcgColor);	// For text

//	CanvasStartBatchDraw(PnlView, VIEW_VIEW);
					  
	
	Lstep=LonInterval/10;
	if (DoGrid)
	for (Lat=ceil(LatMin/LatInterval)*LatInterval; Lat<=floor(LatMax/LatInterval)*LatInterval; Lat+=LatInterval) {
		status = forward_xy_mapx(the_map, Lat, LonMin, &x, &y);
		if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
			Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
			Py=Yf2v(y2f(y/*/Conv*/));
			P=MakePoint(fabs(Px)>2*ViewWidth?P0.x:Px, fabs(Py)>2*ViewHeight?P0.y:Py);
		} else P=P0;
		
		for (Lon=LonMin+Lstep; Lon<=LonMax; Lon+=Lstep) {
			status = forward_xy_mapx(the_map, Lat, Lon, &x, &y);
			if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
				Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
				Py=Yf2v(y2f(y/*/Conv*/));
				if (fabs(Px)>2*ViewWidth or fabs(Py)>2*ViewHeight) Dest=P0;
				else Dest=MakePoint(Px, Py);
//				if (BETWEEN(Xmin,X,Xmax) and BETWEEN(Ymin,Y,Ymax)) {
					if (!(P.x==P0.x and P.y==P0.y) and !(Dest.x==P0.x and Dest.y==P0.y)) {
						CanvasDrawLine(PnlView, VIEW_VIEW, P, Dest);
						CanvasDrawLine(PnlView, VIEW_MAP,  P, Dest);
					}
//				}
				P=Dest;
			}
		}
	}

	Lstep=LatInterval/10;
	if (DoGrid)
	for (Lon=ceil(LonMin/LonInterval)*LonInterval; Lon<floor(LonMax/LonInterval)*LonInterval; Lon+=LonInterval) {
		status = forward_xy_mapx(the_map, LatMin, Lon, &x, &y);
		if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
			Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
			Py=Yf2v(y2f(y/*/Conv*/));
			P=MakePoint(fabs(Px)>2*ViewWidth?P0.x:Px, fabs(Py)>2*ViewHeight?P0.y:Py);
		} else P=P0;

		for (Lat=LatMin+Lstep; Lat<=LatMax; Lat+=Lstep) {
			status = forward_xy_mapx(the_map, Lat, Lon, &x, &y);
			if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
				Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
				Py=Yf2v(y2f(y/*/Conv*/));
				if (fabs(Px)>2*ViewWidth or fabs(Py)>2*ViewHeight) Dest=P0;
				else Dest=MakePoint(Px, Py);
//				if (BETWEEN(Xmin,X,Xmax) and BETWEEN(Ymin,Y,Ymax)) {
					if (!(P.x==P0.x and P.y==P0.y) and !(Dest.x==P0.x and Dest.y==P0.y)) {
						CanvasDrawLine(PnlView, VIEW_VIEW, P, Dest);
						CanvasDrawLine(PnlView, VIEW_MAP,  P, Dest);
					}
//				}
				P=Dest;
			}
		}
	}

	Lstep=LonInterval/10;
	if (DoGridLabel) 
	for (Lat=ceil(LatMin/LatInterval)*LatInterval; Lat<=floor(LatMax/LatInterval)*LatInterval; Lat+=LatInterval) {
		status = forward_xy_mapx(the_map, Lat, LonMin, &x, &y);
		if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
			Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
			Py=Yf2v(y2f(y/*/Conv*/));
			P=MakePoint(fabs(Px)>2*ViewWidth?P0.x:Px, fabs(Py)>2*ViewHeight?P0.y:Py);
		} else P=P0;
		
		for (i=0; TRUE; i++) {
			Lon=floor(LonPref/LonInterval)*LonInterval+(i%2==0?+1:-1)*i/2*LonInterval+LonInterval/2;
			// We alternate 0, 1, -1, 2, -2...
			if (fabs(Lon)>=360) break;	// Could not find position
			if (!BETWEEN(LonMin,Lon,LonMax)) continue;
			status = forward_xy_mapx(the_map, Lat, Lon, &x, &y);
			if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
				Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
				Py=Yf2v(y2f(y/*/Conv*/));
				if (BETWEEN(Margin,Px,ViewWidth-Margin) and BETWEEN(Margin,Py,ViewHeight-Margin)) {
					sprintf(Str, "%.0f�%c", fabs(Lat), Lat>=0?'N':'S');
					CanvasDrawTextAtPoint (PnlView, VIEW_VIEW, Str, VAL_APP_META_FONT,
						MakePoint(Px, Py), VAL_CENTER);
					CanvasDrawTextAtPoint (PnlView, VIEW_MAP,  Str, VAL_APP_META_FONT,
						MakePoint(Px, Py), VAL_CENTER);
					break;
				}
			}
		}
	}

	Lstep=LatInterval/10;
	if (DoGridLabel) 
	for (Lon=ceil(LonMin/LonInterval)*LonInterval; Lon<floor(LonMax/LonInterval)*LonInterval; Lon+=LonInterval) {
		status = forward_xy_mapx(the_map, LatMin, Lon, &x, &y);
		if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
			Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
			Py=Yf2v(y2f(y/*/Conv*/));
			P=MakePoint(fabs(Px)>2*ViewWidth?P0.x:Px, fabs(Py)>2*ViewHeight?P0.y:Py);
		} else P=P0;

		for (i=0; TRUE; i++) {
			Lat=floor(LatPref/LatInterval)*LatInterval+(i%2==0?+1:-1)*i/2*LatInterval+LatInterval/2;
			// We alternate 0, 1, -1, 2, -2...
			if (fabs(Lat)>=180) break;	// Could not find position
			if (!BETWEEN(LatMin,Lat,LatMax)) continue;
			status = forward_xy_mapx(the_map, Lat, Lon, &x, &y);
			if (status==0 and !IsInvalid(x) and !IsInvalid(y)) {
				Px=Xf2v(x2f(x/*/Conv*/));	// Meters to file pixel to view pixel
				Py=Yf2v(y2f(y/*/Conv*/));
				if (BETWEEN(Margin,Px,ViewWidth-Margin) and BETWEEN(Margin,Py,ViewHeight-Margin)) {
					sprintf(Str, "%.0f�%c", fabs(Lon), Lon>=0?'E':'W');
					CanvasDrawTextAtPoint (PnlView, VIEW_VIEW, Str, VAL_APP_META_FONT,
						MakePoint(Px, Py), VAL_CENTER);
					CanvasDrawTextAtPoint (PnlView, VIEW_MAP,  Str, VAL_APP_META_FONT,
						MakePoint(Px, Py), VAL_CENTER);
					break;
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_Grid (int menuBar, int menuItem, void *callbackData, int panel) {
	SetCtrlVal(PnlGrid, GRID_COLOR, GridColor);
	SetCtrlVal(PnlGrid, GRID_BCG_COLOR, GridBcgColor);
	SetCtrlVal(PnlGrid, GRID_DRAW, DoGrid and the_map!=NULL);
	SetCtrlVal(PnlGrid, GRID_LABEL, DoGridLabel and the_map!=NULL);
	SetCtrlAttribute(PnlGrid, GRID_DRAW, ATTR_DIMMED, the_map==NULL);
	SetCtrlAttribute(PnlGrid, GRID_LABEL,ATTR_DIMMED, the_map==NULL);
	SetCtrlVal(PnlGrid, GRID_LAT_INTERVAL, LatInterval);
	SetCtrlVal(PnlGrid, GRID_LON_INTERVAL, LonInterval);
	SetCtrlVal(PnlGrid, GRID_LAT_MIN, LatMin);
	SetCtrlVal(PnlGrid, GRID_LON_MIN, LonMin);
	SetCtrlVal(PnlGrid, GRID_LAT_MAX, LatMax);
	SetCtrlVal(PnlGrid, GRID_LON_MAX, LonMax);
	SetCtrlVal(PnlGrid, GRID_UP_LEFT_X, UpLeftX);
	SetCtrlVal(PnlGrid, GRID_UP_LEFT_Y, UpLeftY);
	SetCtrlVal(PnlGrid, GRID_LOW_RIGHT_X, LowRightX);
	SetCtrlVal(PnlGrid, GRID_LOW_RIGHT_Y, LowRightY);
	SetCtrlVal(PnlGrid, GRID_LAT_PREF, LatPref);
	SetCtrlVal(PnlGrid, GRID_LON_PREF, LonPref);
	
	InstallPopup(PnlGrid);
}


///////////////////////////////////////////////////////////////////////////////
// Reads the various values
int CVICALLBACK cb_GridOK(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char RegFile[MAX_FILENAME_LEN+50];
	int Err;
	
	switch (event) {
		case EVENT_COMMIT:
			
			RemovePopup(0);

			GetCtrlVal(PnlGrid, GRID_COLOR, &GridColor);
			GetCtrlVal(PnlGrid, GRID_BCG_COLOR, &GridBcgColor);
			GetCtrlVal(PnlGrid, GRID_DRAW, &DoGrid);
			GetCtrlVal(PnlGrid, GRID_LABEL, &DoGridLabel);
			GetCtrlVal(PnlGrid, GRID_LAT_INTERVAL, &LatInterval);
			GetCtrlVal(PnlGrid, GRID_LON_INTERVAL, &LonInterval);
			GetCtrlVal(PnlGrid, GRID_LAT_MIN, &LatMin);
			GetCtrlVal(PnlGrid, GRID_LON_MIN, &LonMin);
			GetCtrlVal(PnlGrid, GRID_LAT_MAX, &LatMax);
			GetCtrlVal(PnlGrid, GRID_LON_MAX, &LonMax);
			GetCtrlVal(PnlGrid, GRID_UP_LEFT_X, &UpLeftX);
			GetCtrlVal(PnlGrid, GRID_UP_LEFT_Y, &UpLeftY);
			GetCtrlVal(PnlGrid, GRID_LOW_RIGHT_X, &LowRightX);
			GetCtrlVal(PnlGrid, GRID_LOW_RIGHT_Y, &LowRightY);
			GetCtrlVal(PnlGrid, GRID_LAT_PREF, &LatPref);
			GetCtrlVal(PnlGrid, GRID_LON_PREF, &LonPref);

			sprintf(RegFile, "%s\\%s", RegPath, GridName);
				// Save to Registry
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LatInterval", (char*)&LatInterval, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LonInterval", (char*)&LonInterval, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LatMin", (char*)&LatMin, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LonMin", (char*)&LonMin, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LatMax", (char*)&LatMax, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LonMax", (char*)&LonMax, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LatPref", (char*)&LatPref, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LonPref", (char*)&LonPref, sizeof(double));

			// This is image specific
			sprintf(RegFile, "%s\\%s", RegPath, FileName);
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "UpLeftX", (char*)&UpLeftX, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "UpLeftY", (char*)&UpLeftY, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LowRightX", (char*)&LowRightX, sizeof(double));
			Err=WT_RegWriteBinary(WT_KEY_HKCU, RegFile, "LowRightY", (char*)&LowRightY, sizeof(double));
			
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			else DrawGridOverlay();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Grid Help", "Right click on the various controls to get help on the Grid parameters");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GridCancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup(0);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// See mapx documentation for more info
int CVICALLBACK cb_LoadGrid (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char RegFile[255];
	int Err, S;

	switch (event) {
		case EVENT_COMMIT:
			if (the_map!=NULL) close_mapx(the_map); the_map=NULL;
			SetCtrlVal(PnlGrid, GRID_DRAW, DoGrid=FALSE);
			SetCtrlVal(PnlGrid, GRID_DRAW, DoGridLabel=FALSE);
			SetMenuBarAttribute (Menu, MENU_FILE_GRID, ATTR_CHECKED, FALSE);
			SetCtrlVal(PnlGrid, GRID_FILENAME, "");
		
			if (VAL_EXISTING_FILE_SELECTED==FileSelectPopup ("", "*.mpp;*.gpd", "*.mpp*.gpd",
				 "Select a map projection or grid parameters file",
				 VAL_SELECT_BUTTON, 0, 0, 1, 0, GridFile)) {
				the_map = init_mapx(GridFile);
				SetMenuBarAttribute (Menu, MENU_FILE_GRID, ATTR_CHECKED, the_map!=NULL);
				SetCtrlVal(PnlGrid, GRID_DRAW, DoGrid=(the_map!=NULL));
				SetCtrlVal(PnlGrid, GRID_LABEL, DoGridLabel=(the_map!=NULL));
				SetCtrlAttribute(PnlGrid, GRID_DRAW, ATTR_DIMMED, !DoGrid);
				SetCtrlAttribute(PnlGrid, GRID_LABEL, ATTR_DIMMED, !DoGridLabel);
				SplitPath (GridFile, NULL, NULL, GridName);
				SetCtrlVal(PnlGrid, GRID_FILENAME, GridName);

				sprintf(RegFile, "%s\\%s", RegPath, GridName);
					// Save to Registry
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LatInterval", (char*)&LatInterval, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LonInterval", (char*)&LonInterval, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LatMin", (char*)&LatMin, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LonMin", (char*)&LonMin, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LatMax", (char*)&LatMax, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LonMax", (char*)&LonMax, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LatPref", (char*)&LatPref, sizeof(double), &S);
				Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LonPref", (char*)&LonPref, sizeof(double), &S);
		
				SetCtrlVal(PnlGrid, GRID_LAT_INTERVAL, LatInterval);
				SetCtrlVal(PnlGrid, GRID_LON_INTERVAL, LonInterval);
				SetCtrlVal(PnlGrid, GRID_LAT_MIN, LatMin);
				SetCtrlVal(PnlGrid, GRID_LON_MIN, LonMin);
				SetCtrlVal(PnlGrid, GRID_LAT_MAX, LatMax);
				SetCtrlVal(PnlGrid, GRID_LON_MAX, LonMax);
				SetCtrlVal(PnlGrid, GRID_LAT_PREF, LatPref);
				SetCtrlVal(PnlGrid, GRID_LON_PREF, LonPref);
			}

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Grid", 
					"Select a Map Projection Parameters (.mpp) or\n"
					"Grid Parameter Definition (.gpd) file.\n"
					"(see mapx documentation)");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Help
int CVICALLBACK cb_Grid (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			switch (control) {
				case GRID_LAT_INTERVAL:
				case GRID_LON_INTERVAL:
				case GRID_LAT_MIN:
				case GRID_LON_MIN:
				case GRID_LAT_MAX:
				case GRID_LON_MAX:
					MessagePopup("Help Grid", "The interval and Min/Max boxes\n"
						"control the drawing of the grid.\n"
						"Interval is the instance in � between each line\n"
						"Min/Max are the limits to draw: +-90 for lat, +-180 for lon");
					break;
				case GRID_LAT_PREF:
				case GRID_LON_PREF:
					MessagePopup("Help Grid", "Those are the prefered parallels and meridians\n"
						"where the grid labels should be displayed.\n"
						"In case they are outside the screen, other positions will be tried\n"
						"trying to stay close.");
					break;
				case GRID_COLOR:
				case GRID_BCG_COLOR:
					MessagePopup("Help Grid", "Color of the grid, its text and the background behind");
					break;
				case GRID_LABEL:
				case GRID_DRAW:
					MessagePopup("Help Grid", "Wether to draw the grid and/or the associated labels");
					break;
				case GRID_UP_LEFT_X:
				case GRID_UP_LEFT_Y:
				case GRID_LOW_RIGHT_X:
				case GRID_LOW_RIGHT_Y:
					MessagePopup("Help Grid", "Corners of the image in metric\n"
							"This should be specified in the .hdr file");
					break;
			}
			break;
	}
	return 0;
}
