#ifndef _PRIME_DECOMPOSITION
#define _PRIME_DECOMPOSITION

// This is used for prime factor decomposition to find the possible widths x heigths
typedef struct sFact {
	unsigned long Prime;	// 2,3,5,7...
	unsigned long Exp;		// 1,2,3,4...
} tFact;

extern void PrimeFactors(long N, tFact **Factor, int *NbFact);
extern void GenerateWH(long RH);
extern void GenerateWHmap(long RH);

// Product of WxH, depending on PixDepth and Align32
extern unsigned long SQ, SQmap;

#endif
