#include <ansi_c.h>
#include <utility.h>
// Create a Ramp version at the same dimensions than the DEM file

#define PathRamp "E:\\Large\\RadarSat\\ant_ramp125m.bin"
#define PathDem  "E:\\Large\\RadarSat\\RAMP\\DEM_V2\\200M\\BINARY\\ramp200dem_osu_v2.bin"
#define PathDest "C:\\ant_ramp200m.bin"

#define RampW 48333
#define RampH 41779
#define DemW  28680
#define DemH  24580

#define RampType unsigned char
#define DemType short	// WARNING: swapped

#define Ratio 1.6	// Ramp is 125m, DEM is 200m
#define OffsetX 2440	// Once they are at the same ratio
#define OffsetY -400

#include <cvirte.h>

FILE *DemFile, *RampFile, *DestFile;
RampType V;
RampType Line[DemW];

int main (int argc, char *argv[]) {
	int i, j, x, y, pos;
	
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */
	
//	DemFile = fopen (PathDem, "rb");
	RampFile = fopen (PathRamp,"rb");
	DestFile= fopen (PathDest,"wb");
	
	for (j=0; j<DemH; j++) fwrite (Line, sizeof(RampType), DemW, DestFile);
	
	
	for (j=0; j<DemH; j++)
		for (i=0; i<DemW; i++) {
//			if (fseek (DemFile, (j*DemW+i)*sizeof(DemType), SEEK_SET)==0)
//				fread (V, sizeof(DemType), 1, DemFile);
//			else Breakpoint();
			x=(i+5*OffsetX)*8/5;
			y=(j+5*OffsetY)*8/5;
			pos=y*RampW+x;
			if (0<=pos && pos<RampW*RampH &&
				fseek (RampFile, (y*RampW+x)*sizeof(RampType), SEEK_SET)==0) {
				fread (&V, sizeof(RampType), 1, RampFile);
				if (fseek (DestFile, (j*DemW+i)*sizeof(RampType), SEEK_SET)==0)
					fwrite (&V, sizeof(RampType), 1, DestFile);
				else Breakpoint();
			}
		}	

//	fclose(DemFile);
	fclose(RampFile);
	fclose(DestFile);
	return 0;
}
