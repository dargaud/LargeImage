/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   LargeImage.exe                                                       //
// COPYRIGHT: Guillaume Dargaud 2002-2005 - Freeware                               //
// PURPOSE:   Examine and save portions of very large images (up to 2Gb)           //
// INSTALL:   run SETUP. It will also install the LabWindows/CVI runtime engine.   //
// HELP:      available by right-clicking an item.                                 //
// TUTORIAL:  http://www.gdargaud.net/Hack/LargeImage.html                         //
// TO DO:     >2Gb                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>

#include <ansi_c.h>
#include <utility.h>
#include <userint.h>
#include <cvirte.h>
#include <cviogl.h>


#include "wintools.h"
#include "toolbox.h"
#include "Def.h"

#include "ReadSavePng.h"
#include "ReadSaveJpeg.h"
#include "GraphicFilter.h"
#include "FilterPopup.h"
#include "SwapEndian.h"
#include "GraphPropertiesPopup.h"

#include "Primes.h"
#include "LargeImage.h"
#include "Externs.h"

///////////////////////////////////////////////////////////////////////////////
#define _VER_ "1.6"			// Version number
#define _WEB_ "http://www.gdargaud.net/Hack/LargeImage.html"
#define _COPY_ "� 2002-2005 Guillaume Dargaud"
#define _HELP_ "LargeImage.exe version " _VER_ "\n"\
				"Freeware "_COPY_"\n"				\
				"Last compiled on " __DATE__ "\n"	 \
				"More information and tutorial at "_WEB_
///////////////////////////////////////////////////////////////////////////////

// Panel and menu handles
int PnlView=0, PnlProp=0, PnlCtrl=0, PnlHist=0, PnlOpt=0, PnlOgl=0, PnlMap=0, PnlPrf=0, PnlGrid=0, PnlWP=0, Menu=0;

char PathName[MAX_PATHNAME_LEN]="", FileName[MAX_FILENAME_LEN]="", SavePath[MAX_PATHNAME_LEN]="";
char MapFileName[MAX_PATHNAME_LEN]="";
static FILE *File=NULL;		// The source file stays open during the entire prog

// Should really be bigger than long, like int64
unsigned long FileSize=0, Skip=0, Width=0, Height=0, 
				ImgRowBytes=0, FileRowBytes=0, 
				ImgPixDepth=8, FilePixDepth=8;
BOOL Align32=FALSE, 		// Align rows on 32 bit boundary
	Swapped=FALSE, 			// Swap bytes (if more than one !)
	SwappedPossible=FALSE,	// Possible Swap
	UseScaling=FALSE, 		// Scale values to 0..255 (for float, double, word and long)
	ScalingPossible=FALSE, 	// Scaling can be changed by user
	FromFloat=FALSE, 		// Convert from float value
	FromDouble=FALSE,		// Convert from double value
	Signed=FALSE,			// Signed integers (char/short/long)
	SignedPossible=TRUE;	// Can use the signed/unsigned option

// For the view window. PixDepth is common. No Alignement.
static unsigned char *ViewBits=NULL, *Filtered=NULL;
unsigned long ViewWidth=0, ViewHeight=0, ViewRowBytes=0;

// for the thumbnail window. PixDepth is common. No Alignement.
static unsigned char *ThumbBits=NULL;
static unsigned long ThumbWidth=0, ThumbHeight=0, ThumbRowBytes=0;
static int BoxColor=VAL_RED;

// X/Y/min/max are computed from X/Ycenter and the Zoom factor
long Xcenter=0, Ycenter=0, Xmin, Xmax, Ymin, Ymax, Zoom=1;

// Resample vs Skip/decimation for View
BOOL Resample=FALSE, NeedViewUpdate=TRUE, NeedThumbUpdate=TRUE;		

BOOL AutoUpdate=FALSE;
int JpgComp=75, PngComp=6, StretchIgnore=2;
float PngGamma=0.45455, PngResolution=28., StretchThreshold=0.5;
BOOL PngInterlace=FALSE, PngAlpha32=TRUE;
char TxtAuthor[256]="", TxtDescription[256]="";

static int Effect=EFFECT_NONE;

// Byte position in the file, using the Effect
#define Direct(X, Y) ( Skip + (__int64)FileRowBytes*(Y) + (__int64)(X)*FilePixDepth/8 )
#define Position(X, Y) Direct( Effect & EFFECT_MIRROR ? Width -1-(X) : (X), \
							   Effect & EFFECT_FLIP   ? Height-1-(Y) : (Y) )

// Pointers to the filters to apply, NULL if no filter
static tFilter1x1 *CurrentFilter1x1=NULL;
static tFilter3x3 *CurrentFilter3x3=NULL;
static tFilter5x5 *CurrentFilter5x5=NULL;
static tFilter1C  *CurrentFilter1C=NULL;
static int	FirstFilter1x1Item=0, LastFilter1x1Item=0,
			FirstFilter3x3Item=0, LastFilter3x3Item=0,
			FirstFilter5x5Item=0, LastFilter5x5Item=0,
			FirstFilter1CItem=0,  LastFilter1CItem=0;

// If an argument was passed on the command line
static char* Argv=NULL;
static double MinThumb=0, MaxThumb=0, MinView=0, MaxView=0;


// Display the thumbnail and the view box in it
static int *ColorTable=NULL, IDT=0, IDV=0;


// Histogram displays. Very basic. One for thumbnail, View and post-filtered view
static BOOL DoHistograms=FALSE;
typedef struct sHist {
	unsigned long T[256], R[256], G[256], B[256], A[256];
} tHist;
static tHist THist, VHist, FHist;

// For horizontal scale
float ZBox=10.;				// Percentage flattening of the Z axis with respect to a cube
float UnitsPerPixel=1.;	// Default is one pixel per pixel !
char Unit[255]="Pix",	// If Pix, pix or blank, doesn't display scale
	VertUnit[255]="",		// If blank, doesn't display scale
	*VFrmt="";				// One of the C numerical formats in which to express the vertical values (%d, %s...)
static int ScaleWidth=40;		// Number of pixels on the scale


// Position of where the arrow must point
int ArrowX=0, ArrowY=0;
static char ArrowText[256]="";
static int ArrowColor=VAL_RED, ArrowTextColor=VAL_YELLOW, ArrowTextBackground=VAL_BLACK;

// For profile plot
static int X1profile=-1, X2profile=-1, Y1profile=-1, Y2profile=-1;
static int ProfileColor=VAL_RED, ProfileMapColor=VAL_YELLOW;

// Registry
char RegPath[]="Software\\gdargaud.net\\LargeImage";	// Registry path within current user
static char StrLimits[255]="";	// Altitude limits


///////////////////////////////////////////////////////////////////////////////
// Forward Declarations
void CVICALLBACK cbm_Filter1x1(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Filter3x3(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Filter5x5(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Filter1C (int menubar, int menuItem, void *callbackData, int panel);
static void SwapHist(void);
static void DrawUnits(void);
static void DrawArrow(void);
static void DrawProfile(void);
static void DrawProfileLine(void);



///////////////////////////////////////////////////////////////////////////////
// Warning, limited to 2Gb
unsigned long GetTotSize(void) {
	ImgRowBytes = Width*ImgPixDepth/8;
	if (Align32) ImgRowBytes = ((ImgRowBytes+3)/4)*4;	// 32 bits boundary alignment
	FileRowBytes= Width*FilePixDepth/8;
	if (Align32) FileRowBytes = ((FileRowBytes+3)/4)*4;	// 32 bits boundary alignment
	return Skip + FileRowBytes*Height;
}


// Set the file pointer to the requested position
// return TRUE if error
static BOOL SetPos(__int64 Position) {
//	fpos_t *FilePos;
	errno=0;
//	if (0 != fsetpos (File, FilePos)) {
//	if (0 != fseek  (File, Position, SEEK_SET)) {
	if (0 != fsetpos(File, (fpos_t*)&Position)) {
		// errno
		return TRUE;
	}
	return FALSE;
}


// Update the Properties display
static void DisplayProperties(void) {
	int Err, Len; 
	
	SetCtrlVal(PnlProp, PROP_UNITPERPIX,UnitsPerPixel);
	SetCtrlVal(PnlProp, PROP_UNIT, 		Unit);
	SetCtrlVal(PnlProp, PROP_VERTUNIT, 	VertUnit);

	SetCtrlVal(PnlProp, PROP_ARROW_X, ArrowX);
	SetCtrlVal(PnlProp, PROP_ARROW_Y, ArrowY);
	SetCtrlVal(PnlProp, PROP_ARROW_TEXT, ArrowText);
	
	SetCtrlVal(PnlProp, PROP_ALIGN, Align32);
	SetCtrlVal(PnlProp, PROP_SWAP, Swapped);
	SetCtrlVal(PnlProp, PROP_SCALING, UseScaling);
	SetCtrlVal(PnlProp, PROP_SIGNED, Signed);
	SetCtrlVal(PnlProp, PROP_EFFECT, Effect);

	SetCtrlVal(PnlProp, PROP_SIZE, (double)FileSize);
	SetCtrlVal(PnlProp, PROP_SKIP_FOOTER, (double)FileSize - GetTotSize());
	SetCtrlAttribute(PnlCtrl, CTRL_X_MAX, ATTR_MAX_VALUE, Width-1);
	SetCtrlAttribute(PnlCtrl, CTRL_Y_MAX, ATTR_MAX_VALUE, Height-1);
	GenerateWH(FileSize-Skip);
	if (ArrowX<=0) SetCtrlVal(PnlProp, PROP_ARROW_X, Width/2);
	if (ArrowY<=0) SetCtrlVal(PnlProp, PROP_ARROW_Y, Height/2);
}


// Create the menus for the available filters
static void InitFilters(void) {
	int i, Item, SubMenu;
	
	// Filter Menus
	// Create menu items for present filters
	Item=NewMenuItem (Menu, MENU_FILTER, "1x1 Filters", -1, 0, NULL, NULL);	// Submenu
	SubMenu = NewSubMenu (Menu, Item);
	
	for (i=0; i<NbFilters1x1; i++) {
		Item=NewMenuItem(Menu, SubMenu /*MENU_FILTER*/, Filters1x1[i]->Name, -1, 0, cbm_Filter1x1, Filters1x1[i]);
		if (i==0) {
			FirstFilter1x1Item=Item;
			SetMenuBarAttribute (Menu, Item, ATTR_CHECKED, TRUE);
		}
	} LastFilter1x1Item=Item;

	// InsertSeparator (Menu, MENU_FILTER, -1);
	
	// Create menu items for present filters
	Item=NewMenuItem (Menu, MENU_FILTER, "Channel Filters", -1, 0, NULL, NULL);	// Submenu
	SubMenu = NewSubMenu (Menu, Item);
	
	for (i=0; i<NbFilters1C; i++) {
		Item=NewMenuItem(Menu, SubMenu /*MENU_FILTER*/, Filters1C[i]->Name, -1, 0, cbm_Filter1C, Filters1C[i]);
		if (i==0) {
			FirstFilter1CItem=Item;
			SetMenuBarAttribute (Menu, Item, ATTR_CHECKED, TRUE);
		}
	} LastFilter1CItem=Item;

	// InsertSeparator (Menu, MENU_FILTER, -1);
	
	// Create menu items for present filters
	Item=NewMenuItem (Menu, MENU_FILTER, "3x3 Filters", -1, 0, NULL, NULL);	// Submenu
	SubMenu = NewSubMenu (Menu, Item);
	
	for (i=0; i<NbFilters3x3; i++) {
		Item=NewMenuItem(Menu, SubMenu /*MENU_FILTER*/, Filters3x3[i]->Name, -1, 0, cbm_Filter3x3, Filters3x3[i]);
		if (i==0) {
			FirstFilter3x3Item=Item;
			SetMenuBarAttribute (Menu, Item, ATTR_CHECKED, TRUE);
		}
	} LastFilter3x3Item=Item;

	// InsertSeparator (Menu, MENU_FILTER, -1);
	
	// Create menu items for present filters
	Item=NewMenuItem (Menu, MENU_FILTER, "5x5 Filters", -1, 0, NULL, NULL);	// Submenu
	SubMenu = NewSubMenu (Menu, Item);
	
	for (i=0; i<NbFilters5x5; i++) {
		Item=NewMenuItem(Menu, SubMenu /*MENU_FILTER*/, Filters5x5[i]->Name, -1, 0, cbm_Filter5x5, Filters5x5[i]);
		if (i==0) {
			FirstFilter5x5Item=Item;
			SetMenuBarAttribute (Menu, Item, ATTR_CHECKED, TRUE);
		}
	} LastFilter5x5Item=Item;

}


///////////////////////////////////////////////////////////////////////////////
void InitFromRegistry(void) {
	int Len, Err;
	Err=WT_RegReadString(WT_KEY_HKCU, RegPath, "PathName", PathName, MAX_PATHNAME_LEN, &Len);
	Err=WT_RegReadString(WT_KEY_HKCU, RegPath, "TxtAuthor", TxtAuthor, 256, &Len);
	Err=WT_RegReadString(WT_KEY_HKCU, RegPath, "TxtDescription", TxtDescription, 256, &Len);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "StretchThreshold", (unsigned long*)&StretchThreshold, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "StretchIgnore", (unsigned long*)&StretchIgnore, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "ZBox", (unsigned long*)&ZBox, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "JpgComp", (unsigned long*)&JpgComp, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "PngComp", (unsigned long*)&PngComp, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "PngGamma", (unsigned long*)&PngGamma, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "PngResolution", (unsigned long*)&PngResolution, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "PngInterlace", (unsigned long*)&PngInterlace, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "PngAlpha32", (unsigned long*)&PngAlpha32, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "InterpolateColors", (unsigned long*)&InterpolateColors, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "AutoUpdate", (unsigned long*)&AutoUpdate, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "ScaleWidth", (unsigned long*)&ScaleWidth, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "BoxColor", (unsigned long*)&BoxColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "ArrowColor", (unsigned long*)&ArrowColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "ArrowTextColor", (unsigned long*)&ArrowTextColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "ArrowTextBackground", (unsigned long*)&ArrowTextBackground, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "MissingColor", (unsigned long*)&MissingColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "GridColor", (unsigned long*)&GridColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "GridBcgColor", (unsigned long*)&GridBcgColor, 1);
	SetCtrlVal(PnlOpt, OPT_PNG_AUTHOR, TxtAuthor);
	SetCtrlVal(PnlOpt, OPT_PNG_DESCRIPTION, TxtDescription);
	SetCtrlVal(PnlOpt, OPT_STRETCH_THRESHOLD, StretchThreshold);
	SetCtrlVal(PnlOpt, OPT_STRETCH_IGNORE, StretchIgnore);
	SetCtrlVal(PnlOpt, OPT_ZBOX, ZBox);
	SetCtrlVal(PnlOpt, OPT_JPG_COMP, JpgComp);
	SetCtrlVal(PnlOpt, OPT_PNG_COMP, PngComp);
	SetCtrlVal(PnlOpt, OPT_PNG_GAMMA, PngGamma);
	SetCtrlVal(PnlOpt, OPT_PNG_RESOLUTION, PngResolution);
	SetCtrlVal(PnlOpt, OPT_PNG_INTERLACE, PngInterlace);
	SetCtrlVal(PnlOpt, OPT_PNG_ALPHA32, PngAlpha32);
	SetCtrlVal(PnlOpt, OPT_AUTO_UPDATE, AutoUpdate);
	SetCtrlVal(PnlOpt, OPT_SCALE_WIDTH, ScaleWidth);
	SetCtrlVal(PnlOpt, OPT_BOX, BoxColor);
	SetCtrlVal(PnlOpt, OPT_ARROW, ArrowColor);
	SetCtrlVal(PnlOpt, OPT_TEXT, ArrowTextColor);
	SetCtrlVal(PnlOpt, OPT_BACKGROUND, ArrowTextBackground);
	SetCtrlVal(PnlMap, MAP_INTERPOLATE, InterpolateColors);
	SetCtrlVal(PnlMap, MAP_MISSING, MissingColor);
}

///////////////////////////////////////////////////////////////////////////////
void SaveToRegistry(void) {
	int Len, Err;
	Err=WT_RegWriteString(WT_KEY_HKCU, RegPath, "PathName", PathName);
	Err=WT_RegWriteString(WT_KEY_HKCU, RegPath, "TxtAuthor", TxtAuthor);
	Err=WT_RegWriteString(WT_KEY_HKCU, RegPath, "TxtDescription", TxtDescription);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "AutoUpdate", *(unsigned long*)&AutoUpdate, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "StretchThreshold", *(unsigned long*)&StretchThreshold, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "StretchIgnore", *(unsigned long*)&StretchIgnore, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "ZBox", *(unsigned long*)&ZBox, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "JpgComp", *(unsigned long*)&JpgComp, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "PngComp", *(unsigned long*)&PngComp, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "PngGamma", *(unsigned long*)&PngGamma, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "PngResolution", *(unsigned long*)&PngResolution, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "PngInterlace", *(unsigned long*)&PngInterlace, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "PngAlpha32", *(unsigned long*)&PngAlpha32, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "InterpolateColors", *(unsigned long*)&InterpolateColors, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "ScaleWidth", *(unsigned long*)&ScaleWidth, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "BoxColor", *(unsigned long*)&BoxColor, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "ArrowColor", *(unsigned long*)&ArrowColor, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "ArrowTextColor", *(unsigned long*)&ArrowTextColor, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "ArrowTextBackground", *(unsigned long*)&ArrowTextBackground, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "MissingColor", *(unsigned long*)&MissingColor, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "GridColor", *(unsigned long*)&GridColor, 1);
	Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "GridBcgColor", *(unsigned long*)&GridBcgColor, 1);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	char Str[255]="";
	
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */

	if ((PnlView = LoadPanel (0, "LargeImage.uir", VIEW)) < 0 or
		(PnlCtrl = LoadPanel (0, "LargeImage.uir", CTRL)) < 0 or
		(PnlHist = LoadPanel (0, "LargeImage.uir", HIST)) < 0 or
		(PnlOpt  = LoadPanel (0, "LargeImage.uir", OPT )) < 0 or
		(PnlOgl  = LoadPanel (0, "LargeImage.uir", OGL )) < 0 or
		(PnlMap  = LoadPanel (0, "LargeImage.uir", MAP )) < 0 or
		(PnlPrf  = LoadPanel (0, "LargeImage.uir", PROF)) < 0 or
		(PnlGrid = LoadPanel (0, "LargeImage.uir", GRID)) < 0 or
		(PnlWP   = LoadPanel (0, "LargeImage.uir", WP  )) < 0 or
		(PnlProp = LoadPanel (0, "LargeImage.uir", PROP)) < 0 ) {
		MessagePopup("Error", "File LargeImage.uir missing !");
		return -1;
	}
	Menu = GetPanelMenuBar (PnlCtrl);
	DisplayPanel (PnlCtrl);
	SetCtrlAttribute (PnlCtrl, CTRL_TIMER_ARGV, ATTR_ENABLED, TRUE);
	EnableDragAndDrop (PnlCtrl);
	SwapHist();
	cbp_Profile(PnlPrf, EVENT_PANEL_SIZE, NULL, 0, 0);
	cbp_Waypoints(PnlWP, EVENT_PANEL_SIZE, NULL, 0, 0);

	
	InitFilters();
	InitFromRegistry();

	if (argc>=1) Argv=argv[1];	// for auto file open with timer
	

    // Create OpenGL control on CVI panel    
    OglCtrl = OGLConvertCtrl(PnlOgl, OGL_PICTURE);
    if (OglCtrl<0) {
        OGLGetErrorString (OglCtrl, Str, 255);
        MessagePopup("OGLConvertCtrl Error", Str);
        exit(1);
    }    
    SetOglControlAttributes();		// Setup CVIOGL control once and for all    


	////////////////////
	// Let's get going
	RunUserInterface ();
	////////////////////
	

	SaveToRegistry();
	// Clean Up
    OGLDiscardCtrl(PnlOgl, OglCtrl);  

	if (File!=NULL) fclose(File);

	if (IDT!=0) DiscardBitmap (IDT); IDT=0;
	if (IDV!=0) DiscardBitmap (IDV); IDV=0;
	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;

	if (ViewBits!=NULL)  free(ViewBits);  ViewBits=NULL;
	if (Filtered!=NULL)  free(Filtered);  Filtered=NULL;
	if (ThumbBits!=NULL) free(ThumbBits); ThumbBits=NULL;

	DiscardPanel (PnlView);
	DiscardPanel (PnlCtrl);
	DiscardPanel (PnlProp);
	return 0;
}




///////////////////////////////////////////////////////////////////////////////
// FUNCTION: FindHistogramFeet
// Given a histogram array, finds where the curve starts and where it ends.
// Ignores lowest and highest value
// IN: Hist, array of Nb values
//     Tolerance: surface area that must be covered by the foot/tail before it triggers. 
//                in %. 0 to get the first place when the curve starts.
// OUT: LowHist and HighHist: array positions of the foot and tail
///////////////////////////////////////////////////////////////////////////////
static void	FindHistogramFeet(unsigned long *Hist, int Nb, 
						double Tolerance, int Ignore,
						int *LowHist, int *HighHist) {
	unsigned long Total=0, Sub=0;
	int i;
	
	if (Hist==NULL or Nb<4 or Tolerance<0 or Tolerance>99 or Ignore<0 or Ignore>=Nb/2) 
		return;	// Not valid
	
	for (i=Ignore; i<Nb-Ignore; i++) Total+=Hist[i];
	if (Total==0) { *LowHist=0; *HighHist=Nb-1; return; }
	
	*LowHist=Ignore;
	while ( *LowHist<255 and (Sub+=Hist[++(*LowHist)]) < Total*Tolerance/100 );
	
	Sub=0;
	*HighHist=Nb-Ignore;
	while ( *HighHist>0 and (Sub+=Hist[--(*HighHist)]) < Total*Tolerance/100 );
	
	if (*LowHist >= *HighHist) { *LowHist=0; *HighHist=Nb-1; }
}


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: SetZoom
///////////////////////////////////////////////////////////////////////////////
void SetZoom(int Z) {
	char Str[MAX_FILENAME_LEN*2+100];

	Zoom=FORCEPIN(1, Z, 4096);

	SetCtrlVal(PnlCtrl, CTRL_ZOOM, Zoom);
	// Filename and zooming value
	sprintf(Str, "LargeImage View %s - 1:%d", FileName, Zoom);
	// Add pixel size or unit dimension
	if (UnitsPerPixel!=0. and Unit!=NULL and strcmp(Unit, "")!=0
		and strcmp(Unit, "pix")!=0 and strcmp(Unit, "Pix")!=0) 
		sprintf(Str, "%s - %.0fx%.0f%s", Str, Zoom*ViewWidth*UnitsPerPixel, Zoom*ViewHeight*UnitsPerPixel, Unit);
	else sprintf(Str, "%s - %dx%dpix", Str, Zoom*ViewWidth, Zoom*ViewHeight);
	SetPanelAttribute (PnlView, ATTR_TITLE, Str);
	
	if (MapFile==NULL) 
		sprintf(Str, "LargeImage 3D View - Elevation %s", FileName);
	else sprintf(Str, "LargeImage 3D View - Elevation %s - Mapping %s", FileName, MapFileName);
	
	// is there something to print ?
	if (UnitsPerPixel!=0. and Unit!=NULL and strcmp(Unit, "")!=0
		and strcmp(Unit, "pix")!=0 and strcmp(Unit, "Pix")!=0) 
		sprintf(Str, "%s - %.0fx%.0f%s - %s", 
			Str, 
			Zoom*ViewWidth*UnitsPerPixel, Zoom*ViewHeight*UnitsPerPixel, Unit,
			strlen(StrLimits)>14 ? &StrLimits[14] : "");
	else sprintf(Str, "%s - %dx%dpix - %s", 
			Str, 
			Zoom*ViewWidth, Zoom*ViewHeight,
			strlen(StrLimits)>14 ? &StrLimits[14] : "");

	SetPanelAttribute (PnlOgl, ATTR_TITLE, Str);
	
	sprintf(Str, "%s\n%s", Str, TxtDescription);
	Png_SetTextDescription(Str, FALSE);

	SetMenuBarAttribute (Menu, MENU_FILTER_SKIP, ATTR_DIMMED, Zoom==1);
	SetMenuBarAttribute (Menu, MENU_FILTER_RESAMPLE, ATTR_DIMMED, Zoom==1);
	sprintf(Str, "Resample (%d times slower)", Zoom*Zoom);
	SetMenuBarAttribute (Menu, MENU_FILTER_RESAMPLE, ATTR_ITEM_NAME, Str);

	DrawUnits();
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: DisplayBox
// Check the image limits and draw the thumbnail with a red box on top
///////////////////////////////////////////////////////////////////////////////
void DisplayBox(void) {
	int LoopCnt=0, R;
	double Ratio=(double)Width/ThumbWidth, HH, WW;

	if (PathName[0]='\0') return;
	
	while (Zoom>1 and Zoom*ViewWidth>Width or Zoom*ViewHeight>Height) SetZoom(Zoom-1);
Try:
	++LoopCnt;
	if (LoopCnt>1000 and /*Zoom==1 and*/ ViewWidth *Zoom>Width ) { 
		SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_WIDTH,  ViewWidth =Width /Zoom); 
		LoopCnt=0;
	}
	if (LoopCnt>1000 and /*Zoom==1 and*/ ViewHeight*Zoom>Height) { 
		SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_HEIGHT, ViewHeight=Height/Zoom); 
		LoopCnt=0; 
	}
	if (LoopCnt>1000 and Zoom>1) { SetZoom(Zoom-1); LoopCnt=0; }
	if (LoopCnt>1000 and Zoom==1) return;

	Xmin = Xcenter-Zoom*ViewWidth/2;
	Xmax = Xcenter+Zoom*ViewWidth/2;
	Ymin = Ycenter-Zoom*ViewHeight/2;
	Ymax = Ycenter+Zoom*ViewHeight/2;
	
	if (Xmin<0) { Xcenter++; goto Try;  }
	if (Ymin<0) { Ycenter++; goto Try; }
	if (Xmax>= Width-(Resample?Zoom:0)) { Xcenter--; goto Try; }		// -Zoom because of resample width
	if (Ymax>=Height-(Resample?Zoom:0)) { Ycenter--; goto Try; }

	SetCtrlVal(PnlCtrl, CTRL_X_MIN, Xmin);
	SetCtrlVal(PnlCtrl, CTRL_Y_MIN, Ymin);
	SetCtrlVal(PnlCtrl, CTRL_X_MAX, Xmax);
	SetCtrlVal(PnlCtrl, CTRL_Y_MAX, Ymax);

	CanvasDrawBitmap (PnlCtrl, CTRL_THUMBNAIL, IDT, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);

	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_PEN_COLOR, BoxColor);
//	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_PEN_FILL_COLOR, VAL_RED);
	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_PEN_STYLE, VAL_SOLID);

	#define LowLim 1
	HH=Zoom*ViewHeight/Ratio;
	WW=Zoom*ViewWidth/Ratio;	
	if (HH>LowLim and WW>LowLim)
		R=CanvasDrawRect (PnlCtrl, CTRL_THUMBNAIL, MakeRect (Ymin/Ratio, Xmin/Ratio, Zoom*ViewHeight/Ratio, Zoom*ViewWidth/Ratio), VAL_DRAW_FRAME);
	else if (WW>LowLim) 
		R=CanvasDrawLine (PnlCtrl, CTRL_THUMBNAIL, MakePoint(Xmin/Ratio, Ymin/Ratio), MakePoint(Xmin/Ratio+WW, Ymin/Ratio));
	else if (HH>LowLim) 
		R=CanvasDrawLine (PnlCtrl, CTRL_THUMBNAIL, MakePoint(Xmin/Ratio, Ymin/Ratio), MakePoint(Xmin/Ratio, Ymin/Ratio+HH));
	else 
		R=CanvasDrawPoint(PnlCtrl, CTRL_THUMBNAIL, MakePoint(Xmin/Ratio, Ymin/Ratio));

	CanvasUpdate (PnlCtrl, CTRL_THUMBNAIL, VAL_ENTIRE_OBJECT);
}


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: FromTo
// PURPOSE: Converts between binary types
// POSSIBLE: B->B, W->B, L->B, 
//           F->B, D->B (only on 2nd pass with Min/MaxThumb already set)
//           F->D, D->D, W->D, L->D	(Used on first pass to set Min/MaxThumb)
//           RGB->RGB, RGBA->RGBA
// ToDouble: TRUE on first pass to find bounds by converting to double, FALSE to convert to defaut format
// NOTE:     Transformation can be done in place
///////////////////////////////////////////////////////////////////////////////
void FromTo(void* Source, void* Dest, BOOL ToDouble) {
	
	if (Swapped) Source=SwapEndian(Source, FilePixDepth/8);
	
	#define SCS ( Signed ? *( char*)Source : *(unsigned  char*)Source)
	#define SSS ( Signed ? *(short*)Source : *(unsigned short*)Source)
	#define SLS ( Signed ? *( long*)Source : *(unsigned  long*)Source)

	switch (FilePixDepth) {
/*B->D*/case  8:if (ToDouble)        *(double*)Dest = SCS;
/*B->B*/		else if (UseScaling) *(unsigned char*)Dest=FORCEPIN(0, 256*(SCS-MinThumb)/(MaxThumb-MinThumb), 255);
				else                 *(unsigned char*)Dest=SCS;		
				break;	

/*W->D*/case 16:if (ToDouble)        *(double*)Dest=SSS;				
/*W->B*/		else if (UseScaling) *(unsigned char*)Dest=FORCEPIN(0, 256*(SSS-MinThumb)/(MaxThumb-MinThumb), 255);	// D->B
				else if (Signed)     *(unsigned char*)Dest=((char*)Source)[1];				  // We only take the high bits
				else                 *(unsigned char*)Dest=((unsigned char*)Source)[1];
				break;

		case 24: ((char*)Dest)[0]=((char*)Source)[0]; 
/* RGB->RGB */	 ((char*)Dest)[1]=((char*)Source)[1]; 
				 ((char*)Dest)[2]=((char*)Source)[2];			
				 break;		
		
		case 32: switch (ImgPixDepth+FromFloat) {
/* L->D */			case 8: if (ToDouble)        *(double*)Dest = SLS;
/* L->B */					else if (UseScaling) *(unsigned char*)Dest=FORCEPIN(0, 256*(SLS-MinThumb)/(MaxThumb-MinThumb), 255);	// D->B
							else if (Signed)     *(unsigned char*)Dest=((char*)Source)[3];	// We only take the high bits
							else                 *(unsigned char*)Dest=((unsigned char*)Source)[3];
							break;
					
/* RGBA->RGBA */	case 32: *(unsigned long*)Dest=*(unsigned long*)Source;		break;
					
/* F->D */			case 9: if (ToDouble) *(double*)Dest=*(float*)Source;
/* F->B */					else   *(unsigned char*)Dest=FORCEPIN(0, 256*(*(float*)Source-MinThumb)/(MaxThumb-MinThumb), 255);	// F->B
							break;
				} break;
		
/*D->D*/case 64: if (ToDouble) *(double*)Dest=*(double*)Source;		
/*D->B*/		else    *(unsigned char*)Dest=FORCEPIN(0, 256*(*(double*)Source-MinThumb)/(MaxThumb-MinThumb), 255);	// D->B
				break;
		
		default: Breakpoint();
	}
}


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: CreateThumbnail
// PURPOSE:  Read the image file to extract the thumbnail
///////////////////////////////////////////////////////////////////////////////
static void CreateThumbnail(void) {
	double Ratio=(double)Width/ThumbWidth;
	unsigned long X, Y, i, j;
	unsigned char *Dest, V[8];
	float F;
	double D;
	int Prog;
	char Str[MAX_FILENAME_LEN+20]="";
	BOOL FirstPass=TRUE;
	
	if (File==NULL or ThumbBits==NULL) return;
	SetWaitCursor (TRUE);
	
	sprintf(Str, "%s - 1:%.0f", FileName, Ratio);
	SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_LABEL_TEXT, Str);

	Prog = CreateProgressDialog ("Preparing thumbnail", "Percent Complete", 1, VAL_FULL_MARKERS, "");

	if (UseScaling) { FirstPass=TRUE; MinThumb=1e30; MaxThumb=-1e30; }

Pass:
	// What is the slowest part ? Computation of coordinate ? File seek ? File access ? Bin conversion ?
	for (j=0; j<ThumbHeight; j++) {
		Y=(unsigned long)(Ratio*j);
		UpdateProgressDialog (Prog, 100*j/ThumbWidth, 0);
		for (i=0, Dest=&ThumbBits[ThumbRowBytes*j]; i<ThumbWidth; i++, Dest+=ImgPixDepth/8) {
			X=(unsigned long)(Ratio*i);
			if (!SetPos(Position(X, Y)))
				fread(V, FilePixDepth/8, 1, File);
			else memset(V, 0, 8);

			if (FirstPass and UseScaling) {
				FromTo(V, &D, TRUE);
				if (D<MinThumb) MinThumb=D;
				if (D>MaxThumb) MaxThumb=D;
			} else FromTo(V, Dest, FALSE);
			
		}
	}

	if ((ScalingPossible or UseScaling) and FirstPass) { 
		char Frmt[255]="", Str[255]="";
		sprintf(Frmt, "Thumbnail Min/Max: %s%%s/%s%%s", VFrmt, VFrmt);
		sprintf(Str, Frmt, MinThumb, VertUnit, MaxThumb, VertUnit);
		SetCtrlVal(PnlCtrl, CTRL_THUMB_MINMAX, Str);
		FirstPass=FALSE; 
		goto Pass;
	}

	DiscardProgressDialog (Prog);
	
	if (IDT!=0) DiscardBitmap (IDT); IDT=0;
	
	if (ImgPixDepth<=8 and ColorTable==NULL) {			// We need a color table if 8 bits greyscale...!?!
		ColorTable=Calloc(1<<ImgPixDepth, int);
		if (ColorTable==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
		for (i=0; i<1<<ImgPixDepth; i++)
			ColorTable[i]=MakeColor(i, i, i);
	}


	if (DoHistograms) {				// Compute the histogram only if necessary
		memset (THist.T, 0, 256*sizeof(long));
		memset (THist.R, 0, 256*sizeof(long));
		memset (THist.G, 0, 256*sizeof(long));
		memset (THist.B, 0, 256*sizeof(long));
		memset (THist.A, 0, 256*sizeof(long));
		for (j=0; j<ThumbHeight; j++) {
			Y=Ymin+(unsigned long)(Zoom*j);
			Dest=&ThumbBits[ThumbRowBytes*j];
			for (i=0, X=Xmin; i<ThumbWidth; i++, Dest+=ImgPixDepth/8, X+=Zoom) {
				switch (ImgPixDepth) {
					case 8: THist.T[Dest[0]]++; break;
					case 24:THist.R[Dest[0]]++; 
							THist.G[Dest[1]]++; 
							THist.B[Dest[2]]++; 
							THist.T[(Dest[0]+Dest[1]+Dest[2])/3]++; 
							break;
					case 32:THist.R[Dest[0]]++; 
							THist.G[Dest[1]]++; 
							THist.B[Dest[2]]++; 
							THist.A[Dest[3]]++; 
							THist.T[(Dest[0]+Dest[1]+Dest[2]+Dest[3])/4]++; 
							break;
				}
			}
		}
		DeleteGraphPlot (PnlHist, HIST_HISTT, -1, VAL_DELAYED_DRAW);
		if (ImgPixDepth==32)
			PlotY (PnlHist, HIST_HISTT, THist.A, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_YELLOW);
		if (ImgPixDepth>=24) {
			PlotY (PnlHist, HIST_HISTT, THist.R, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_RED);
			PlotY (PnlHist, HIST_HISTT, THist.G, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
			PlotY (PnlHist, HIST_HISTT, THist.B, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLUE);
		}
		PlotY (PnlHist, HIST_HISTT, THist.T, 256, VAL_UNSIGNED_INTEGER,
			   VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLACK);
	}


	NewBitmap (ThumbRowBytes, ImgPixDepth, ThumbWidth, ThumbHeight, ColorTable, ThumbBits, NULL, &IDT);

	NeedThumbUpdate=FALSE;
	DisplayBox();	
	SetWaitCursor (FALSE);
}


///////////////////////////////////////////////////////////////////////////////
// Read the image file to extract the view; applies all transformations
void CreateView(void) {
	unsigned long X, Y, i, j, k, l, Tot, TotR, TotG, TotB, TotA, Nb, SubX, SubY;
	unsigned char *Dest, V[8];
	static unsigned char *Source=NULL, *Buffer=NULL;
	int Prog, Res=0;
	unsigned char *Tmp, C;
	static BOOL Allocated=TRUE;
	BOOL FirstPass=TRUE;
	long LowHist, HighHist;
	double D;
	
	if (File==NULL or ViewBits==NULL) return;
	SetWaitCursor (TRUE);

	
	DisplayPanel(PnlView);
	SetMenuBarAttribute (Menu, MENU_FILE_SAVEVIEWAS, ATTR_DIMMED, FALSE);
	SetMenuBarAttribute (Menu, MENU_FILE_PRINTVIEW, ATTR_DIMMED, FALSE);
	SetMenuBarAttribute (Menu, MENU_FILE_COPYVIEW, ATTR_DIMMED, ScalingPossible);
	SetMenuBarAttribute (Menu, MENU_FILE_OGL, ATTR_DIMMED, !OglPossible);
	if (not OglPossible) SetMenuBarAttribute (Menu, MENU_FILE_OGL, ATTR_CHECKED, FALSE);
				
	Prog = CreateProgressDialog ("Preparing View", "Percent Complete", 1, VAL_FULL_MARKERS, "");


	if (NeedViewUpdate) {
		MinView=1e30; MaxView=-1e30; 
		Source=realloc(Source, FilePixDepth/8*ViewWidth);
		if (Source==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }

//		if (FromFloat or FromDouble) { FirstPass=TRUE; MinFloat=1e30; MaxFloat=-1e30; }
//Pass:
		if (Zoom==1 and Effect==EFFECT_NONE and !UseScaling and !DoOgl)			// A little optimised to reduce disk accesses
			for (j=0; j<ViewHeight; j++) {
				Y=Ymin+(unsigned long)(Zoom*j);
				Dest=&ViewBits[ViewRowBytes*j];
				UpdateProgressDialog (Prog, 100*j/ViewHeight, 0);
				if (!SetPos(Position(Xmin, Y)))
					fread(Source, FilePixDepth/8, ViewWidth, File);
				else memset (Source, 0, ViewWidth*ImgPixDepth/8);	// No Floats here
				
				for (i=0, l=0; i<ViewWidth; i++, l+=FilePixDepth/8, Dest+=ImgPixDepth/8) {
					FromTo(&Source[l], Dest, FALSE);
					if (ScalingPossible or UseScaling) {
						FromTo(V, &D, TRUE);
						if (D<MinView) MinView=D;
						if (D>MaxView) MaxView=D;
					}
				}
			}
		else 
		if (Resample and Zoom>1) {		// Much slower. Should be optimized by reading lines instead of doing seeks
			Buffer=realloc(Buffer, Zoom*FilePixDepth/8);
			if (Buffer==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
			for (j=0; j<ViewHeight; j++) {
				Y=Ymin+(unsigned long)(Zoom*j);
				Dest=&ViewBits[ViewRowBytes*j];
				UpdateProgressDialog (Prog, 100*j/ViewHeight, 0);
				for (i=0, X=Xmin; i<ViewWidth; i++, Dest+=ImgPixDepth/8, X+=Zoom) {
					Tot=TotR=TotG=TotB=TotA=Nb=0;
					for (SubY=0; SubY<Zoom; SubY++)
						if (!SetPos(Position(X, Y+SubY))) {
							fread(Buffer, FilePixDepth/8, Zoom, File);
							for (SubX=0, Tmp=(unsigned char*)Buffer; SubX<Zoom; Nb++, SubX++, Tmp+=/**/FilePixDepth/8) {
								if (DoOgl) AddToOgl(V, i, j);
								FromTo(Tmp, V, FALSE); 
								if (ScalingPossible or UseScaling) {
									FromTo(Tmp, &D, TRUE);
									if (D<MinView) MinView=D;
									if (D>MaxView) MaxView=D;
								}
								switch (ImgPixDepth) {
									case 8:  Tot +=V[0]; break;
									case 32: TotA+=V[3]; // no break
									case 24: TotR+=V[0];
											 TotG+=V[1];
											 TotB+=V[2]; break;
								}
							}
						}
					switch (ImgPixDepth) {
						case 8:  Dest[0]=Tot /Nb;	break;		// FORCEPIN ?
						case 32: Dest[3]=TotA/Nb;	// no break
						case 24: Dest[0]=TotR/Nb;
								 Dest[1]=TotG/Nb;
								 Dest[2]=TotB/Nb;	break;
					}
				}	
			}
		} // Endif Resample
		else 		// Skip points
			for (j=0; j<ViewHeight; j++) {
				Y=Ymin+(unsigned long)(Zoom*j);
				Dest=&ViewBits[ViewRowBytes*j];
				UpdateProgressDialog (Prog, 100*j/ViewHeight, 0);
				for (i=0, X=Xmin; i<ViewWidth; i++, Dest+=ImgPixDepth/8, X+=Zoom) {
					if (!SetPos(Position(X, Y)))
						fread(V, FilePixDepth/8, 1, File);
					else memset (V, 0, FilePixDepth/8);
					if (DoOgl) AddToOgl(V, i, j);
					if (ScalingPossible or UseScaling) {
						FromTo(V, &D, TRUE);
						if (D<MinView) MinView=D;
						if (D>MaxView) MaxView=D;
					}
					FromTo(V, V, FALSE); 

					// Use memcpy
					switch (ImgPixDepth) {
						case 32:Dest[3]=V[3];	// no break
						case 24:Dest[2]=V[2];
								Dest[1]=V[1];	// no break
						case 8: Dest[0]=V[0];	break;
					}
				}
			}


//		if ((FromFloat or FromDouble) and FirstPass) { FirstPass=FALSE; goto Pass; }

		if (DoHistograms) {
			memset (VHist.T, 0, 256*sizeof(long));
			memset (VHist.R, 0, 256*sizeof(long));
			memset (VHist.G, 0, 256*sizeof(long));
			memset (VHist.B, 0, 256*sizeof(long));
			memset (VHist.A, 0, 256*sizeof(long));
			for (j=0; j<ViewHeight; j++) {
				Y=Ymin+(unsigned long)(Zoom*j);
				Dest=&ViewBits[ViewRowBytes*j];
				for (i=0, X=Xmin; i<ViewWidth; i++, Dest+=ImgPixDepth/8, X+=Zoom) {
					switch (ImgPixDepth) {
						case 8: VHist.T[Dest[0]]++; break;
						case 24:VHist.R[Dest[0]]++; 
								VHist.G[Dest[1]]++; 
								VHist.B[Dest[2]]++; 
								VHist.T[(Dest[0]+Dest[1]+Dest[2])/3]++; 
								break;
						case 32:VHist.R[Dest[0]]++; 
								VHist.G[Dest[1]]++; 
								VHist.B[Dest[2]]++; 
								VHist.A[Dest[3]]++; 
								VHist.T[(Dest[0]+Dest[1]+Dest[2]+Dest[3])/4]++; 
								break;
					}
				}
			}

			// Set the values for the strech histogram function
			FindHistogramFeet(VHist.T, 256, StretchThreshold, StretchIgnore, &LowHist, &HighHist);
			if (LowHist==0 and HighHist==255) {
				FilterHistStretch.Mult=1;
				FilterHistStretch.Div=1;
				FilterHistStretch.Bias=0;
			} else {
				FilterHistStretch.Mult=255;
				FilterHistStretch.Div=HighHist-LowHist;
				FilterHistStretch.Bias=-255*LowHist/(HighHist-LowHist);
			}			
			
			DeleteGraphPlot (PnlHist, HIST_HISTV, -1, VAL_DELAYED_DRAW);
			if (ImgPixDepth==32)
				PlotX (PnlHist, HIST_HISTV, VHist.A, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_YELLOW);
			if (ImgPixDepth>=24) {
				PlotY (PnlHist, HIST_HISTV, VHist.R, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_RED);
				PlotY (PnlHist, HIST_HISTV, VHist.G, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
				PlotY (PnlHist, HIST_HISTV, VHist.B, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLUE);
			}
			PlotY (PnlHist, HIST_HISTV, VHist.T, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLACK);

			
			if (CurrentFilter1x1==&FilterHistStretch) {
				SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_NUM_CURSORS, 2);

				SetCursorAttribute (PnlHist, HIST_HISTV, 1, ATTR_CURSOR_POINT_STYLE, VAL_NO_POINT);
				SetCursorAttribute (PnlHist, HIST_HISTV, 2, ATTR_CURSOR_POINT_STYLE, VAL_NO_POINT);
				SetCursorAttribute (PnlHist, HIST_HISTV, 1, ATTR_CURSOR_COLOR, VAL_MAGENTA);
				SetCursorAttribute (PnlHist, HIST_HISTV, 2, ATTR_CURSOR_COLOR, VAL_MAGENTA);
				SetCursorAttribute (PnlHist, HIST_HISTV, 1, ATTR_CROSS_HAIR_STYLE, VAL_VERTICAL_LINE);
				SetCursorAttribute (PnlHist, HIST_HISTV, 2, ATTR_CROSS_HAIR_STYLE, VAL_VERTICAL_LINE);
				SetCursorAttribute (PnlHist, HIST_HISTV, 1, ATTR_CURSOR_ENABLED, FALSE);
				SetCursorAttribute (PnlHist, HIST_HISTV, 2, ATTR_CURSOR_ENABLED, FALSE);
			
				SetGraphCursor (PnlHist, HIST_HISTV, 1, LowHist, 0);
				SetGraphCursor (PnlHist, HIST_HISTV, 2, HighHist, 0);
			} else SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_NUM_CURSORS, 0);

		} // endif DoHistogram
	
	} // endif NeedViewUpdate	

	if (ScalingPossible or UseScaling) { 
		char Frmt[255]="";
		sprintf(Frmt, "View Min/Max: %s%%s/%s%%s", VFrmt, VFrmt);
		sprintf(StrLimits, Frmt, MinView, VertUnit, MaxView, VertUnit);
	} else strcpy(StrLimits, "");
	SetCtrlVal(PnlCtrl, CTRL_VIEW_MINMAX, StrLimits);

	// Let's keep a copy of the bitmap so that we can redraw quicker if Filter changes
	// if no filter is used, then we just point to ViewBits
	Filtered=realloc(Filtered, ViewRowBytes*ViewHeight);
	if (Filtered==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
	memcpy(Filtered, ViewBits, ViewRowBytes*ViewHeight);
	if (CurrentFilter1x1!=&FilterId1x1) Filter1x1(Filtered, ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight, CurrentFilter1x1);
	if (CurrentFilter1C !=&FilterId1C ) Filter1C (Filtered, ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight, CurrentFilter1C);
	if (CurrentFilter3x3!=&FilterId3x3) Filter3x3(Filtered, ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight, CurrentFilter3x3);
	if (CurrentFilter5x5!=&FilterId5x5) Filter5x5(Filtered, ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight, CurrentFilter5x5);

	if (DoHistograms) {
		memset (FHist.T, 0, 256*sizeof(long));
		memset (FHist.R, 0, 256*sizeof(long));
		memset (FHist.G, 0, 256*sizeof(long));
		memset (FHist.B, 0, 256*sizeof(long));
		memset (FHist.A, 0, 256*sizeof(long));
		for (j=0; j<ViewHeight; j++) {
			Y=Ymin+(unsigned long)(Zoom*j);
			Dest=&Filtered[ViewRowBytes*j];
			for (i=0, X=Xmin; i<ViewWidth; i++, Dest+=ImgPixDepth/8, X+=Zoom) {
				switch (ImgPixDepth) {
					case 8: FHist.T[Dest[0]]++; break;
					case 24:FHist.R[Dest[0]]++; 
							FHist.G[Dest[1]]++; 
							FHist.B[Dest[2]]++; 
							FHist.T[(Dest[0]+Dest[1]+Dest[2])/3]++; 
							break;
					case 32:FHist.R[Dest[0]]++; 
							FHist.G[Dest[1]]++; 
							FHist.B[Dest[2]]++; 
							FHist.A[Dest[3]]++; 
							FHist.T[(Dest[0]+Dest[1]+Dest[2]+Dest[3])/4]++; 
							break;
				}
			}
		}
		DeleteGraphPlot (PnlHist, HIST_HISTF, -1, VAL_DELAYED_DRAW);
		if (ImgPixDepth==32)
			PlotX (PnlHist, HIST_HISTF, FHist.A, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_YELLOW);
		if (ImgPixDepth>=24) {
			PlotY (PnlHist, HIST_HISTF, FHist.R, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_RED);
			PlotY (PnlHist, HIST_HISTF, FHist.G, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
			PlotY (PnlHist, HIST_HISTF, FHist.B, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLUE);
		}
		PlotY (PnlHist, HIST_HISTF, FHist.T, 256, VAL_UNSIGNED_INTEGER, VAL_THIN_STEP, VAL_NO_POINT, VAL_SOLID, 1, VAL_BLACK);
	} // endif DoHistogram


	if (DoOgl) DrawOgl();
	
	DiscardProgressDialog (Prog);
	
	if (IDV!=0) DiscardBitmap (IDV); IDV=0;
	
	if (ImgPixDepth<=8 and ColorTable==NULL) {	/* ADD EXTERNAL PALETTE */
		ColorTable=Calloc(1<<ImgPixDepth, int);
		if (ColorTable==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
		for (i=0; i<1<<ImgPixDepth; i++)
			ColorTable[i]=MakeColor(i, i, i);
	}

	Res=NewBitmap (ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight, ColorTable, Filtered, NULL, &IDV);
	Res=CanvasDrawBitmap (PnlView, VIEW_VIEW, IDV, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
	DrawGridOverlay();
	DrawProfileLine();
	DisplayWaypoints();
	DrawUnits();
	DrawArrow();
		
	CanvasUpdate (PnlView, VIEW_VIEW, VAL_ENTIRE_OBJECT);
	NeedViewUpdate=FALSE;
	SetWaitCursor (FALSE);
}



///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Panel callbacks
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Ctrl (int panel, int event, void *callbackData, int eventData1, int eventData2) {
	int Top, Bitmap=0;
	static unsigned long Last=0;
	static BOOL FirstCall=TRUE;
	
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
		
		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.

		
				case VAL_MENUKEY_MODIFIER | 'P':	// Print Thumbnail
				case VAL_MENUKEY_MODIFIER | 'p':	// Print Thumbnail
	            	SetWaitCursor (TRUE);
				//	PrintPanel (PnlCtrl, "", 1, VAL_FULL_PANEL, 1);
					PrintCtrl (PnlCtrl, CTRL_THUMBNAIL, "", 1, 1);
				    SetWaitCursor (FALSE);
					return 1;

				case VAL_MENUKEY_MODIFIER | 'S':	// Save Thumbnail
				case VAL_MENUKEY_MODIFIER | 's':	// Save Thumbnail
					SaveAs(PnlCtrl, CTRL_THUMBNAIL, "Save Thumbnail as...", NULL, 0,0,0,0);
		            return 1;

				case VAL_MENUKEY_MODIFIER | 'C':	// Copy to clipboard
				case VAL_MENUKEY_MODIFIER | 'c':	// Copy to clipboard
					SetWaitCursor (TRUE);
					GetCtrlBitmap (PnlCtrl, CTRL_THUMBNAIL, 0, &Bitmap);
					ClipboardPutBitmap (Bitmap);
					DiscardBitmap (Bitmap);
		            SetWaitCursor (FALSE);
					return 1;

				case '+':
					if (Zoom>1) {
						SetZoom(Zoom/2);
						DisplayBox();
						NeedViewUpdate=TRUE;
						if (AutoUpdate) CreateView();
					}
					return 1;
				
				case '-':
					if (Zoom<2048) {
						SetZoom(Zoom*2);
						DisplayBox();
						NeedViewUpdate=TRUE;
						if (AutoUpdate) CreateView();
					}
					return 1;
			}
			break;

	
		case EVENT_FILESDROPPED:
			Argv=*(char**)eventData1;	// Should be freed
			if (Argv!=NULL) cbm_Open(0, 0, NULL, 0);
			break;
						
		
		case EVENT_PANEL_SIZE:
			if (FirstCall) Last=ThumbWidth;
			FirstCall=FALSE;
			GetPanelAttribute(PnlCtrl, ATTR_WIDTH,  &ThumbWidth);
			if (ThumbWidth>Width and Width>0) /*SetPanelAttribute(PnlCtrl, ATTR_WIDTH,*/  ThumbWidth=Width;		// We dan't want the thumbnail to be bigger than the source image
			if (ThumbWidth<20) SetPanelAttribute(PnlCtrl, ATTR_WIDTH,  ThumbWidth=20);		// We don't want the thumbnail to be bigger than the source image

			if (Last==ThumbWidth and !NeedThumbUpdate) return 0;	// No change
			NeedThumbUpdate=TRUE;
			
			GetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_TOP,  &Top);
			SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_WIDTH,  ThumbWidth);
			if (Height==0 or Width==0) {	// In case nothing selected yet
				GetPanelAttribute(PnlCtrl, ATTR_HEIGHT,  &ThumbHeight);
				ThumbHeight-=Top; if (ThumbHeight<=1) ThumbHeight=10;
				SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_HEIGHT,  ThumbHeight);
			}
			else SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_HEIGHT, ThumbHeight=Height*ThumbWidth/Width);
			
			SetPanelAttribute(PnlCtrl, ATTR_HEIGHT,  Top+ThumbHeight);
			
			if (ThumbBits!=NULL) free(ThumbBits); ThumbBits=NULL;
			ThumbRowBytes=ThumbWidth*ImgPixDepth/8;	// No align
			ThumbBits=Calloc(ThumbRowBytes*ThumbHeight, unsigned char);
			if (ThumbBits==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }

			CreateThumbnail();
			Last=ThumbWidth;
			break;
		
	}
	return 0;
}


int CVICALLBACK cbp_View (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	char Str[100];
	int pWidth, pHeight, Top, Left;
	int Bitmap=0;
	int PV, PM;
						
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
		
		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.


				case '5':
					NeedViewUpdate=TRUE;
					CreateView();
					return 1;
				
				case '1':
				case VAL_END_VKEY:
					Xcenter -= ViewWidth*Zoom/2; 
					Ycenter += ViewHeight*Zoom/2; 
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '2':
				case VAL_DOWN_ARROW_VKEY:
					Ycenter += ViewHeight*Zoom/2;
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '3':
				case VAL_PAGE_DOWN_VKEY:
					Xcenter += ViewWidth*Zoom/2; 
					Ycenter += ViewHeight*Zoom/2;
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '4':
				case VAL_LEFT_ARROW_VKEY:
					Xcenter -= ViewWidth*Zoom/2; 
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '6':
				case VAL_RIGHT_ARROW_VKEY:
					Xcenter += ViewWidth*Zoom/2; 
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '7':
				case VAL_HOME_VKEY:
					Xcenter -= ViewWidth*Zoom/2; 
					Ycenter -= ViewHeight*Zoom/2;
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '8':
				case VAL_UP_ARROW_VKEY:
					Ycenter -= ViewHeight*Zoom/2;
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				case '9':
				case VAL_PAGE_UP_VKEY:
					Xcenter += ViewWidth*Zoom/2; 
					Ycenter -= ViewHeight*Zoom/2;
					DisplayBox();
					NeedViewUpdate=TRUE;
					if (AutoUpdate) CreateView();
					return 1;
				
				
				case '+':
					if (Zoom>1) {
						SetZoom(Zoom/2);
						DisplayBox();
						NeedViewUpdate=TRUE;
						if (AutoUpdate) CreateView();
					}
					return 1;
				
				case '-':
					if (Zoom<2048) {
						SetZoom(Zoom*2);
						DisplayBox();
						NeedViewUpdate=TRUE;
						if (AutoUpdate) CreateView();
					}
					return 1;

				
				case VAL_MENUKEY_MODIFIER | 'P':	// Print view
				case VAL_MENUKEY_MODIFIER | 'p':	// Print view
					PrintCtrl (PnlView, VIEW_VIEW, "", 1, 1);
					return 1;

				case VAL_MENUKEY_MODIFIER | 'S':	// Save View
				case VAL_MENUKEY_MODIFIER | 's':	// Save View
		            cbm_SaveViewAs(0,0,0,0);
		            return 1;

				case VAL_MENUKEY_MODIFIER | 'C':	// Copy to clipboard
				case VAL_MENUKEY_MODIFIER | 'c':	// Copy to clipboard
					SetWaitCursor (TRUE);
					GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, &PV);
					GetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_ZPLANE_POSITION, &PM);
					GetCtrlBitmap (PnlView, PV<=PM ? VIEW_MAP : VIEW_VIEW, 0, &Bitmap);
					ClipboardPutBitmap (Bitmap);
					DiscardBitmap (Bitmap);
		            SetWaitCursor (FALSE);
					return 1;

				
				case 'M':		// alternate between normal and mapping view
				case 'm':
					if (MapFile==NULL) SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, 0);
					else { 
						GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, &PV);
						GetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_ZPLANE_POSITION, &PM);
						SetCtrlAttribute (PnlView, PV<=PM ? VIEW_MAP : VIEW_VIEW, ATTR_ZPLANE_POSITION, 0);

					}
					return 1;
				

				case VAL_F1_VKEY:
					MessagePopup("View Window Help",
						"Click to center the image.\n"
						"Double-click to center the image and redraw.\n"
						"Right-click to find the X/Y coordinate and the file value.\n"
						"Double-right-click to center the image, zoom-in and redraw.\n"
						"Arrows or numeric keypad to move around the view window\n"
						"[+]/[-] to zoom in/out\n"
						"[5] to redraw the view window\n"
						"[Ctrl-Tab] to cycle between windows.\n"
						"[M] to switch between normal view and mapping view.\n"
						"If using a 3D mapping, press [M] to shift between normal and mapped view."
					); 
					return 1;
			}
			
			// Key not recognised, passed to Main window (if there is one)
			SetActivePanel(PnlCtrl);
			FakeKeystroke (eventData1);
			return 1;

		case EVENT_PANEL_SIZE:
			GetPanelAttribute(PnlView, ATTR_WIDTH,  &pWidth);
			GetPanelAttribute(PnlView, ATTR_HEIGHT, &pHeight);
			GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_LEFT,  &Left); ViewWidth=pWidth-Left;
			GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_TOP, &Top);    ViewHeight=pHeight-Top;
			if (ViewWidth *Zoom>Width)  { SetPanelAttribute(PnlView, ATTR_WIDTH,  pWidth=Width/Zoom+Left); ViewWidth=pWidth-Left; }
			if (ViewHeight*Zoom>Height) { SetPanelAttribute(PnlView, ATTR_HEIGHT, pHeight=Height/Zoom+Top);ViewHeight=pHeight-Top; }

			sprintf(Str, "View %dx%d pix", ViewWidth, ViewHeight);
			SetCtrlVal(PnlCtrl, CTRL_VIEW_PORTION, Str);

			SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_WIDTH,  ViewWidth);
			SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_HEIGHT, ViewHeight);
			SetCtrlAttribute (PnlView, VIEW_MAP, ATTR_WIDTH,  ViewWidth);
			SetCtrlAttribute (PnlView, VIEW_MAP, ATTR_HEIGHT, ViewHeight);

			SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_LEFT,0);
			SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_TOP, 0);
			SetCtrlAttribute (PnlView, VIEW_MAP, ATTR_LEFT, 0);
			SetCtrlAttribute (PnlView, VIEW_MAP, ATTR_TOP,  0);
			
			if (ViewBits!=NULL) free(ViewBits); ViewBits=NULL;
			ViewRowBytes=ViewWidth*ImgPixDepth/8;	// No align
			ViewBits=Calloc(ViewRowBytes*ViewHeight, unsigned char);
			if (ViewBits==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }
			DisplayBox();
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			break;
		
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Histograms (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int W, H;
	switch (event) {
		case EVENT_CLOSE:
			if (CurrentFilter1x1==&FilterHistStretch) return 0;	// Can't close !
			SetMenuBarAttribute (Menu, MENU_FILTER_HISTOGRAMS, ATTR_CHECKED, DoHistograms=FALSE);
			HidePanel(PnlHist);
			break;
		
		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.
			}
			break;

		case EVENT_PANEL_SIZE:
			GetPanelAttribute(PnlHist, ATTR_WIDTH,  &W);
			GetPanelAttribute(PnlHist, ATTR_HEIGHT, &H);
			if (W<20) SetPanelAttribute(PnlView, ATTR_WIDTH,  W=20);
			if (H<20) SetPanelAttribute(PnlView, ATTR_HEIGHT, H=20);
			SetCtrlAttribute (PnlHist, HIST_HISTT, ATTR_WIDTH,  W);
			SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_WIDTH,  W);
			SetCtrlAttribute (PnlHist, HIST_HISTF, ATTR_WIDTH,  W);
			SetCtrlAttribute (PnlHist, HIST_HISTT, ATTR_HEIGHT, H);
			SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_HEIGHT, H);
			SetCtrlAttribute (PnlHist, HIST_HISTF, ATTR_HEIGHT, H);
			SetCtrlAttribute (PnlHist, HIST_HISTT, ATTR_TOP,  0);
			SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_TOP,  0);
			SetCtrlAttribute (PnlHist, HIST_HISTF, ATTR_TOP,  0);
			SetCtrlAttribute (PnlHist, HIST_HISTT, ATTR_LEFT, 0);
			SetCtrlAttribute (PnlHist, HIST_HISTV, ATTR_LEFT, 0);
			SetCtrlAttribute (PnlHist, HIST_HISTF, ATTR_LEFT, 0);
			return 0;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Properties (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	switch (event) {
		case EVENT_CLOSE:
			RemovePopup (1);
			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Callbacks of the CTRL panel
int CVICALLBACK cb_Extrema (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Extrema Help",
				"Those are the highest and lowest values encountered in the file\n"
				"while elaborating the Thumbnail.\n"
				"Those indications are displayed only if you use the [Scaling] option\n"
				"from the [Properties] menu.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_Click (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Position Help",
				"Position and value of the point\n"
				"when you right-click in the View panel."
			);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Zoom (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[100];
	int Z;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlCtrl, CTRL_ZOOM, &Z);
			SetZoom(Z);
			DisplayBox();
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("View reduction Help",
				"This is the reduction factor for the view window.\n"
				"For instance a reduction of 3 will display only one pixel out of 9\n"
				"of the source image, thus reducing it.\n"
				"The reduction can happen two ways, as controled by the Skip/Resample menu:\n"
				" - Skip will only use one pixel out of those possible (1 out of 9 is reduction is 3)\n"
				" - Resample will average all 9 pixels to produce the final pixel (much slower).\n"
				"\nNote that you cannot zoom-in / enlarge the source image.\n"
				"The view window is shown as a red box on the thumbnail view.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Move (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			switch (control) {
				case CTRL_MOVE_UP_RIGHT:
				case CTRL_MOVE_UP_LEFT:
				case CTRL_MOVE_UP:
					Ycenter -= ViewHeight*Zoom/2; break;
			}
			switch (control) {
				case CTRL_MOVE_DOWN_RIGHT:
				case CTRL_MOVE_DOWN_LEFT:
				case CTRL_MOVE_DOWN:
					Ycenter += ViewHeight*Zoom/2; break;
			}
			switch (control) {
				case CTRL_MOVE_DOWN_LEFT:
				case CTRL_MOVE_UP_LEFT:
				case CTRL_MOVE_LEFT:
					Xcenter -= ViewWidth*Zoom/2; break;
			}
			switch (control) {
				case CTRL_MOVE_DOWN_RIGHT:
				case CTRL_MOVE_UP_RIGHT:
				case CTRL_MOVE_RIGHT:
					Xcenter += ViewWidth*Zoom/2; break;
			}
			DisplayBox();
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Move view window Help",
				"Moves the view window by half its size within the thumbnail.");

			break;
	}
	return 0;
}

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			DiscardPanel(PnlWP);	// For file save confirmation
			QuitUserInterface (0);
			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Properties Panel
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Width (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_WIDTH, &Width);
			GetTotSize();
			SetCtrlVal(PnlProp, PROP_HEIGHT, Height=(FileSize-Skip)/FileRowBytes);
			DisplayProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Width of the image - Help",
				"You can enter the width of the image (in pixels) manually if you know it");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Height (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_HEIGHT, &Height);
			GetTotSize();
			DisplayProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Height of the image - Help",
				"You can enter the height of the image (in pixels) manually if you know it");
			break;
	}
	return 0;
}


int CVICALLBACK cb_PixDepth (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int PixDepth;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_PIXDEPTH, &PixDepth);

			switch (PixDepth) {
/* B->B */		case  8:Swapped=FALSE; 
						SwappedPossible=FALSE;FilePixDepth=8;  ImgPixDepth=8; VFrmt="%.0f"; UseScaling=FALSE; ScalingPossible=TRUE;  OglPossible=TRUE; SignedPossible=TRUE;  FromFloat=FALSE; FromDouble=FALSE; break;
/* F->B */		case  9:Signed=TRUE; 
						SwappedPossible=TRUE; FilePixDepth=32; ImgPixDepth=8; VFrmt="%.3e"; UseScaling=TRUE;  ScalingPossible=FALSE; OglPossible=TRUE; SignedPossible=FALSE; FromFloat=TRUE;  FromDouble=FALSE; break;
/* D->B */		case 11:Signed=TRUE; 
						SwappedPossible=TRUE; FilePixDepth=64; ImgPixDepth=8; VFrmt="%.3e"; UseScaling=TRUE;  ScalingPossible=FALSE; OglPossible=TRUE; SignedPossible=FALSE; FromFloat=FALSE; FromDouble=TRUE;  break;
/* W->B */		case 16:SwappedPossible=TRUE; FilePixDepth=16; ImgPixDepth=8; VFrmt="%.0f"; UseScaling=FALSE; ScalingPossible=TRUE;  OglPossible=TRUE; SignedPossible=TRUE;  FromFloat=FALSE; FromDouble=FALSE; break;
/* RGB  */		case 24:Signed=FALSE;
						SwappedPossible=TRUE; FilePixDepth=24; ImgPixDepth=24;VFrmt="%X";   UseScaling=FALSE; ScalingPossible=FALSE; OglPossible=FALSE;SignedPossible=FALSE; FromFloat=FALSE; FromDouble=FALSE; break;
/* RGBA */		case 32:Signed=FALSE;
						SwappedPossible=TRUE; FilePixDepth=32; ImgPixDepth=32;VFrmt="%X";   UseScaling=FALSE; ScalingPossible=FALSE; OglPossible=FALSE;SignedPossible=FALSE; FromFloat=FALSE; FromDouble=FALSE; break;
/* L->B */		case 30:SwappedPossible=TRUE; FilePixDepth=32; ImgPixDepth=8; VFrmt="%.0f"; UseScaling=FALSE; ScalingPossible=TRUE;  OglPossible=TRUE; SignedPossible=TRUE;  FromFloat=FALSE; FromDouble=FALSE; break;
			}
			SetMenuBarAttribute (Menu, MENU_FILTER_GREY_PALETTE, ATTR_DIMMED, ImgPixDepth!=8);
			SetMenuBarAttribute (Menu, MENU_FILTER_LOAD_PALETTE, ATTR_DIMMED, ImgPixDepth!=8);

			SetCtrlVal(PnlProp, PROP_SWAP, Swapped); 
			SetCtrlAttribute(PnlProp, PROP_SWAP, ATTR_DIMMED, !SwappedPossible); 

			SetCtrlVal(PnlProp, PROP_SCALING, UseScaling);
			SetCtrlAttribute(PnlProp, PROP_SCALING, ATTR_DIMMED, not ScalingPossible);
			SetCtrlAttribute(PnlCtrl, CTRL_THUMB_MINMAX, ATTR_VISIBLE, ScalingPossible or UseScaling);
			SetCtrlAttribute(PnlCtrl, CTRL_VIEW_MINMAX,  ATTR_VISIBLE, ScalingPossible or UseScaling);

			SetCtrlVal(PnlProp, PROP_SIGNED, Signed);
			SetCtrlAttribute(PnlProp, PROP_SIGNED, ATTR_DIMMED, !SignedPossible);

			GetTotSize();
			DisplayProperties();
			break;

		case EVENT_RIGHT_CLICK:
			MessagePopup("Pixel Depth Help",
				"This is the number of bits used to represent each pixel.\n"
				"Currently 7 modes are supported:\n"
				"- 8 bits greyscale\n"
				"- 16 bits greyscale\n"
				"- 32 bits greyscale\n"
				"- 3x8 bits color RGB\n"
				"- 4x8 bits color RGB + Alpha channel\n"
				"- 32 bits float greyscale\n"
				"- 64 bits double greyscale\n"
				"They can all be used with byte swapping except the first one.\n"
				"\nSo the size of the image is close to Width*Height*PixDepth/8\n"
				"It is exactly this value if Align32 is not set");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Swap (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_SWAP, &Swapped);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Swap bytes Help",
				"This inverts the order of the bytes being read.\n"
				"It is usefull if reading binary data from a system with a different byte ordering.\n"
				"For instance the PC is little-endian (aka LSB) where the least significant byte is first.\n"
				"The Macs are big-endian (aka MSB), the opposite.\n"
				"So you should activate this when reading form across systems.\n"
				"For instance, it will read BGR instead of RGB");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Skip (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_SKIP_HEADER, &Skip);
			GetTotSize();
			DisplayProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Header skip Help",
				"Number of bytes to skip at the begining of the file.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_SkipF (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Footer skip Help",
				"Number of bytes that are being skipped at the end of the file.\n"
				"If this value is negative, then Width or Height are too high.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Align (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_ALIGN, &Align32);
			GetTotSize();
			DisplayProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("32 bit alignment Help",
				"If this is set, then the number of bytes per row (RowBytes)\n"
				"is rounded off to the highest 32 bit value.\n"
				"For instance if width=6 and PixDepth=8 the file will be organized like this:\n"
				"if no alignement: 111111222222333333...\n"
				"if alignement:    111111PP222222PP333333PP...\n"
				"Where 1 are the bytes of the first line, 2 the 2nd line...\n"
				"And P are ignored padding bytes\n");
			break;
	}
	return 0;
}


int CVICALLBACK cb_Scaling (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_SCALING, &UseScaling);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Use Scaling Help",
				"If this is set, the values are scalled to a full dynamic range.\n"
				"It is ON for float and double, optional for long and words,.\n"
				"and not available with multichannel images (RGB...)\n"
				"For instance, if you read a matrix of longs whose actual values\n"
				"go from 0 to 10000, the colors you will see no the screen\n"
				"will go from 0 (black) to 255 (white) instead of 0 to 1\n"
				"The scaling Min and Max values are found when drawing the thumbnail,\n"
				"so the program might actually miss the real extremas.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Signed (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_SIGNED, &Signed);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Interpret as signed integers Help",
				"When reading from 8bit, 16bit or 32 bit integers,\n"
				"read the value as a signed number, vs an unsigned number."
				);
			break;
	}
	return 0;
}


int CVICALLBACK cb_PossibleWH (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int W;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_POSSIBLE_WH, &W);
			if (SQ==0 or W==0) return 0;
			
			SetCtrlVal(PnlProp, PROP_WIDTH, Width=W);
			SetCtrlVal(PnlProp, PROP_HEIGHT, Height=SQ/W);
			GetTotSize();
			DisplayProperties();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Possible Width and Height Help",
						"The program attempts to determine the possible width and height of the image\n"
						"based on the file size, the skipped header, the pixel depth and the alignement\n"
						"The sizes closest to a square are marked with < >");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Effect (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlProp, PROP_EFFECT, &Effect);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Effect Help",
						"Applies simple symetries to the image.");

			break;
	}
	return 0;
}

int CVICALLBACK cb_Cancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int State;
	switch (event) {
		case EVENT_COMMIT:
			State = SetBreakOnLibraryErrors (0);
			RemovePopup (1);
			SetBreakOnLibraryErrors (State);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

int CVICALLBACK cb_PropOK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int W, H, Err, PossibleWH, PixDepth;
	char RegFile[255];
	
	switch (event) {
		case EVENT_COMMIT:
			if ((double)FileSize < GetTotSize()) {
				MessagePopup("Warning !",
					"The width and height that you've set will try to read past the end of the file !");
				return 0;
			}
			RemovePopup(0);

			GetCtrlVal(PnlProp, PROP_ARROW_X, &ArrowX);
			GetCtrlVal(PnlProp, PROP_ARROW_Y, &ArrowY);
			GetCtrlVal(PnlProp, PROP_ARROW_TEXT, ArrowText);
			// DrawArrow();

			// Force resizes that will redraw all
			GetPanelAttribute(PnlCtrl, ATTR_WIDTH, &W);
			GetPanelAttribute(PnlCtrl, ATTR_HEIGHT, &H);
			SetPanelSize (PnlCtrl, H, W);
			NeedViewUpdate=NeedThumbUpdate=TRUE;
			cbp_Ctrl (PnlCtrl, EVENT_PANEL_SIZE, NULL, 0, 0);
			cbp_View (PnlView, EVENT_PANEL_SIZE, NULL, 0, 0);
			cbp_Histograms (PnlHist, EVENT_PANEL_SIZE, NULL, 0, 0);
			cbp_OGL (PnlOgl, EVENT_PANEL_SIZE, NULL, 0, 0);
			if (AutoUpdate) CreateView();

			if (strlen(FileName)>0) {
				sprintf(RegFile, "%s\\%s", RegPath, FileName);
					// Save to Registry
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "UnitsPerPixel", *(unsigned long*)&UnitsPerPixel, 1);
				Err=WT_RegWriteString(WT_KEY_HKCU, RegFile, "Unit", Unit);
				Err=WT_RegWriteString(WT_KEY_HKCU, RegFile, "VertUnit", VertUnit);
				
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "ArrowX", *(unsigned long*)&ArrowX, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "ArrowY", *(unsigned long*)&ArrowY, 1);
				Err=WT_RegWriteString(WT_KEY_HKCU, RegFile, "ArrowText", ArrowText);

				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Align32", *(unsigned long*)&Align32, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Swapped", *(unsigned long*)&Swapped, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "UseScaling", *(unsigned long*)&UseScaling, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Signed", *(unsigned long*)&Signed, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Effect", *(unsigned long*)&Effect, 1);

				GetCtrlVal(PnlProp, PROP_PIXDEPTH, &PixDepth);
				GetCtrlVal(PnlProp, PROP_POSSIBLE_WH, &PossibleWH);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "PixDepth", *(unsigned long*)&PixDepth, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "PossibleWH", *(unsigned long*)&PossibleWH, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Width", *(unsigned long*)&Width, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Height", *(unsigned long*)&Height, 1);
				Err=WT_RegWriteULong (WT_KEY_HKCU, RegFile, "Skip", *(unsigned long*)&Skip, 1);
			}			
			return 0;
		
	}
	return 0;
}
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_Open (int menuBar, int menuItem, void *callbackData, int panel) {
	char DriveName[MAX_PATHNAME_LEN]="", DirName[MAX_DIRNAME_LEN]="";
	int S;
	
	if (PathName[0]!='\0') {
		SplitPath (PathName, DriveName, DirName, FileName);
		strcat(DriveName, DirName);
	}
	if (Argv!=NULL or VAL_EXISTING_FILE_SELECTED==FileSelectPopup (DriveName, "*.raw;*.bin", "*.raw;*.bin",
		 			"Select large image file to view", VAL_LOAD_BUTTON,
					 0, 0, 1, 0, PathName)) {
		if (Argv!=NULL) strcpy(PathName, Argv);
		Argv=NULL;
		if (File!=NULL) fclose(File);	// File was open
		File=NULL;
		DoOgl=FALSE;
		if (MapFile!=NULL) cbm_ApplyMapping(0,0,0,0);	// Close it
		X2profile=X1profile=Y2profile=Y1profile=-1;
		
		HidePanel(PnlView);
		SetMenuBarAttribute(Menu, MENU_FILE_SAVEVIEWAS, ATTR_DIMMED, TRUE);
		SetMenuBarAttribute(Menu, MENU_FILE_PRINTVIEW,  ATTR_DIMMED, TRUE);
		SetMenuBarAttribute(Menu, MENU_FILE_COPYVIEW,   ATTR_DIMMED, TRUE);
		HidePanel(PnlPrf); 
		HidePanel(PnlOgl); DoOgl= /*OglPossible=*/ FALSE;
		SetMenuBarAttribute(Menu, MENU_FILE_OGL, ATTR_DIMMED, TRUE);
		SetMenuBarAttribute(Menu, MENU_FILE_OGL, ATTR_CHECKED, FALSE);
		SavePath[0]='\0';
		
		SplitPath(PathName, DriveName, DirName, FileName);
		SetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_LABEL_TEXT, FileName);
		SetCtrlVal (PnlProp, PROP_FILENAME, FileName);
		if (0!=GetFileSize (PathName, (long*)&FileSize) or (File = fopen (PathName, "rb")) ==NULL) {
			MessagePopup("Error", "Could not read file");
			return;
		}

		
		if (strlen(FileName)>0) {
			char RegFile[255];
			int Err, PixDepth=0, PossibleWH=0, Len;
			sprintf(RegFile, "%s\\%s", RegPath, FileName);
	
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "UnitsPerPixel", (unsigned long*)&UnitsPerPixel, 1);
			Err=WT_RegReadString(WT_KEY_HKCU, RegFile, "Unit", Unit, 255, &Len);
			Err=WT_RegReadString(WT_KEY_HKCU, RegFile, "VertUnit", VertUnit, 255, &Len);
		
			Err=WT_RegReadString(WT_KEY_HKCU, RegFile, "ArrowText", ArrowText, 255, &Len);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "ArrowX", (unsigned long*)&ArrowX, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "ArrowY", (unsigned long*)&ArrowY, 1);
		
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "PixDepth", (unsigned long*)&PixDepth, 1);
			SetCtrlVal(PnlProp, PROP_PIXDEPTH, PixDepth);
			cb_PixDepth (PnlProp, PROP_PIXDEPTH, EVENT_COMMIT, NULL, 0, 0);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "PossibleWH", (unsigned long*)&PossibleWH, 1);
			SetCtrlVal(PnlProp, PROP_POSSIBLE_WH, PossibleWH);
			cb_PossibleWH (PnlProp, PROP_POSSIBLE_WH, EVENT_COMMIT, NULL, 0,0);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Width", (unsigned long*)&Width, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Height", (unsigned long*)&Height, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Skip", (unsigned long*)&Skip, 1);
			SetCtrlVal(PnlProp, PROP_WIDTH, Width);
			SetCtrlVal(PnlProp, PROP_HEIGHT, Height);
			SetCtrlVal(PnlProp, PROP_SKIP_HEADER, Skip);
			
			
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Align32", (unsigned long*)&Align32, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Swapped", (unsigned long*)&Swapped, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "UseScaling", (unsigned long*)&UseScaling, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Signed", (unsigned long*)&Signed, 1);
			Err=WT_RegReadULong (WT_KEY_HKCU, RegFile, "Effect", (unsigned long*)&Effect, 1);

			Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "UpLeftX", (char*)&UpLeftX, sizeof(double), &S);
			Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "UpLeftY", (char*)&UpLeftY, sizeof(double), &S);
			Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LowRightX", (char*)&LowRightX, sizeof(double), &S);
			Err=WT_RegReadBinary(WT_KEY_HKCU, RegFile, "LowRightY", (char*)&LowRightY, sizeof(double), &S);
		}
	
		
		
		
		DisplayProperties();
		InstallPopup (PnlProp);
		SetMenuBarAttribute (Menu, MENU_FILE_PROPERTIES, ATTR_DIMMED, FALSE);
	}
}


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: SaveAs
// PURPOSE: asks for a filename and save a canvas control OR a bitmap to a jpg/png file
// IN: either Panel/Control or Bits/RowBytes/PixDepth/Width/Height
///////////////////////////////////////////////////////////////////////////////
void SaveAs(int Panel, int Control, const char *Comment,
			unsigned char* Bits, int RowBytes, int PixDepth, int Width, int Height) {
	int R=TRUE, L;
	char DriveName[MAX_PATHNAME_LEN]="", DirName[MAX_DIRNAME_LEN]="", 
		TmpFileName[MAX_FILENAME_LEN]="", Str[MAX_PATHNAME_LEN*2];
	if (SavePath[0]!='\0') {
		SplitPath (SavePath, DriveName, DirName, FileName);
		strcat(DriveName, DirName);
	}
	
	R=FileSelectPopup (DriveName, "*.jpg;*.png", "*.png;*.jpg", Comment,
					 VAL_SAVE_BUTTON, 0, 0, 1, 1, SavePath);
	SetWaitCursor (TRUE);

	if (R==VAL_EXISTING_FILE_SELECTED or R==VAL_NEW_FILE_SELECTED) {
		L=strlen(SavePath);
		R=TRUE;
		if (SavePath[L-4]=='.' and 
			toupper(SavePath[L-3])=='P' and 
			toupper(SavePath[L-2])=='N' and 
			toupper(SavePath[L-1])=='G' ) {
				Png_SetParameters (PngGamma, PngInterlace, PngResolution, PngComp, PngAlpha32);
				SplitPath (SavePath, NULL, NULL, TmpFileName);
				Png_SetTextTitle (TmpFileName, FALSE);
				Png_SetTextAuthor (TxtAuthor, FALSE);
//				Png_SetTextDescription (TxtDescription, FALSE);
				sprintf(Str, "Image extracted from file %s", FileName);
				Png_SetTextComment (Str, FALSE);
				Png_SetTextSoftware("Image Created by LargeImage.exe\n"
					_COPY_"\nFreeware downloadable at "_WEB_, FALSE);
				SetWaitCursor (TRUE);
				if (Bits==NULL) 
					R=Png_SavePanelControlToFile (Panel, Control, SavePath);
				else 
					R=Png_SaveBitmapToFile (Bits, RowBytes, PixDepth, Width, Height, SavePath);
				// if (PixDepth==32 and !PngAlpha32) NeedViewUpdate=TRUE;	// Because ViewBits has been destroyed
				SetWaitCursor (FALSE);
			}
		else if ((SavePath[L-4]=='.' and 
			toupper(SavePath[L-3])=='J' and 
			toupper(SavePath[L-2])=='P' and 
			toupper(SavePath[L-1])=='G' ) or
			(SavePath[L-5]=='.' and 
			toupper(SavePath[L-4])=='J' and 
			toupper(SavePath[L-3])=='P' and 
			toupper(SavePath[L-2])=='E' and 
			toupper(SavePath[L-1])=='G' ) ) {
				SetWaitCursor (TRUE);
				if (Bits==NULL) 
					R=Jpg_SavePanelControlToFile (Panel, Control, SavePath, JpgComp);
				else 
					R=Jpg_SaveBitmapToFile (Bits, RowBytes, PixDepth, Width, Height, SavePath, JpgComp);
				SetWaitCursor (FALSE);
			}
		if (R) MessagePopup("Error", "Could not save to that file.");
	}
	SetWaitCursor (FALSE);
}

void CVICALLBACK cbm_SaveViewAs (int menuBar, int menuItem, void *callbackData, int panel) {
	int PV, PM;
	GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, &PV);
	GetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_ZPLANE_POSITION, &PM);
	
	SaveAs(PnlView, PV<=PM ? VIEW_VIEW : VIEW_MAP, "Save View As...", NULL,0,0,0,0);
//		Filtered, ViewRowBytes, ImgPixDepth, ViewWidth, ViewHeight);
}

void CVICALLBACK cbm_SaveThumbnailAs (int menuBar, int menuItem, void *callbackData, int panel) {
	SaveAs(PnlCtrl, CTRL_THUMBNAIL, "Save Thumbnail As...", NULL,0,0,0,0);
}

void CVICALLBACK cbm_PrintView (int menuBar, int menuItem, void *callbackData, int panel) {
	PrintCtrl (PnlView, VIEW_VIEW, "", 1, 1);
}

void CVICALLBACK cbm_PrintThumbnail (int menuBar, int menuItem, void *callbackData, int panel) {
	PrintCtrl (PnlCtrl, CTRL_THUMBNAIL, "", 1, 1);
}

void CVICALLBACK cbm_CopyView (int menuBar, int menuItem, void *callbackData, int panel) {
	int Bitmap=0;
	int PV, PM;
	GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_ZPLANE_POSITION, &PV);
	GetCtrlAttribute (PnlView, VIEW_MAP,  ATTR_ZPLANE_POSITION, &PM);

	SetWaitCursor (TRUE);
	GetCtrlBitmap (PnlView, PV<=PM ? VIEW_VIEW : VIEW_MAP, 0, &Bitmap);
	ClipboardPutBitmap (Bitmap);
	DiscardBitmap (Bitmap);
    SetWaitCursor (FALSE);
}

void CVICALLBACK cbm_CopyThumbnail (int menuBar, int menuItem, void *callbackData, int panel) {
	int Bitmap=0;
	SetWaitCursor (TRUE);
	GetCtrlBitmap (PnlCtrl, CTRL_THUMBNAIL, 0, &Bitmap);
	ClipboardPutBitmap (Bitmap);
	DiscardBitmap (Bitmap);
    SetWaitCursor (FALSE);
}

void CVICALLBACK cbm_Properties (int menuBar, int menuItem, void *callbackData, int panel) {
	HidePanel(PnlView);
	InstallPopup (PnlProp);
}

void CVICALLBACK cbm_Quit (int menuBar, int menuItem, void *callbackData, int panel) {
	QuitUserInterface (0);
}

void CVICALLBACK cbm_Filter1x1 (int menuBar, int menuItem, void *callbackData, int panel) {
	static int LastFilter=0;
	if (LastFilter!=0) 
		SetMenuBarAttribute (Menu, LastFilter, ATTR_CHECKED, FALSE);
	else SetMenuBarAttribute (Menu, FirstFilter1x1Item, ATTR_CHECKED, FALSE);
	CurrentFilter1x1=callbackData;
	SetMenuBarAttribute (Menu, LastFilter=menuItem, ATTR_CHECKED, TRUE);

	if (CurrentFilter1x1==&FilterCustom1x1) 
		Filter1x1Popup(CurrentFilter1x1);
	if (CurrentFilter1x1==&FilterHistStretch and !DoHistograms) 
		cbm_Histograms (0, 0, NULL, 0);
}

void CVICALLBACK cbm_Filter3x3 (int menuBar, int menuItem, void *callbackData, int panel) {
	static int LastFilter=0;
	if (LastFilter!=0) 
		SetMenuBarAttribute (Menu, LastFilter, ATTR_CHECKED, FALSE);
	else SetMenuBarAttribute (Menu, FirstFilter3x3Item, ATTR_CHECKED, FALSE);
	CurrentFilter3x3=callbackData;
	SetMenuBarAttribute (Menu, LastFilter=menuItem, ATTR_CHECKED, TRUE);

	if (CurrentFilter3x3==&FilterCustom3x3) 
		Filter3x3Popup(CurrentFilter3x3);
}

void CVICALLBACK cbm_Filter5x5 (int menuBar, int menuItem, void *callbackData, int panel) {
	static int LastFilter=0;
	if (LastFilter!=0) 
		SetMenuBarAttribute (Menu, LastFilter, ATTR_CHECKED, FALSE);
	else SetMenuBarAttribute (Menu, FirstFilter5x5Item, ATTR_CHECKED, FALSE);
	CurrentFilter5x5=callbackData;
	SetMenuBarAttribute (Menu, LastFilter=menuItem, ATTR_CHECKED, TRUE);

	if (CurrentFilter5x5==&FilterCustom5x5) 
		Filter5x5Popup(CurrentFilter5x5);
}

void CVICALLBACK cbm_Filter1C (int menuBar, int menuItem, void *callbackData, int panel) {
	static int LastFilter=0;
	if (LastFilter!=0) 
		SetMenuBarAttribute (Menu, LastFilter, ATTR_CHECKED, FALSE);
	else SetMenuBarAttribute (Menu, FirstFilter1CItem, ATTR_CHECKED, FALSE);
	CurrentFilter1C=callbackData;
	SetMenuBarAttribute (Menu, LastFilter=menuItem, ATTR_CHECKED, TRUE);

	if (CurrentFilter1C==&FilterCustom1C) 
		Filter1CPopup(CurrentFilter1C, ImgPixDepth==32);
}

void CVICALLBACK cbm_Skip (int menuBar, int menuItem, void *callbackData, int panel) {
	if (!Resample) return;
	Resample=FALSE;
	SetMenuBarAttribute (Menu, MENU_FILTER_SKIP, ATTR_CHECKED, !Resample);
	SetMenuBarAttribute (Menu, MENU_FILTER_RESAMPLE, ATTR_CHECKED, Resample);
	NeedViewUpdate=TRUE;
	if (AutoUpdate) CreateView();
}

void CVICALLBACK cbm_Resample (int menuBar, int menuItem, void *callbackData, int panel) {
	if (Resample) return;
	Resample=TRUE;
	SetMenuBarAttribute (Menu, MENU_FILTER_SKIP, ATTR_CHECKED, !Resample);
	SetMenuBarAttribute (Menu, MENU_FILTER_RESAMPLE, ATTR_CHECKED, Resample);
	NeedViewUpdate=TRUE;
	if (AutoUpdate) CreateView();
}

void CVICALLBACK cbm_Histograms (int menuBar, int menuItem, void *callbackData, int panel) {
	SetMenuBarAttribute (Menu, MENU_FILTER_HISTOGRAMS, ATTR_CHECKED, DoHistograms=!DoHistograms);
	(DoHistograms ? DisplayPanel : HidePanel)(PnlHist);
	if (DoHistograms) {
		NeedViewUpdate=TRUE;
		CreateThumbnail();
		CreateView();
	}
}

int CVICALLBACK cb_CloseHist (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			cbm_Histograms (0, 0, NULL, 0);
			break;
	}
	return 0;
}


void CVICALLBACK cbm_GreyPalette (int menuBar, int menuItem, void *callbackData, int panel) {
	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
	SetMenuBarAttribute (Menu, MENU_FILTER_GREY_PALETTE, ATTR_CHECKED, TRUE);
	SetMenuBarAttribute (Menu, MENU_FILTER_LOAD_PALETTE, ATTR_CHECKED, FALSE);
	NeedViewUpdate=TRUE;
	if (AutoUpdate) CreateView();
}

void CVICALLBACK cbm_LoadPalette (int menuBar, int menuItem, void *callbackData, int panel) {
	char PalettePath[MAX_PATHNAME_LEN]="", Line[256];
	FILE *File;
	int i, R, G, B;
	
	if (VAL_EXISTING_FILE_SELECTED==FileSelectPopup ("", "*.pal", "*.pal", "Select Palette file",
					 VAL_LOAD_BUTTON, 0, 1, 1, 0, PalettePath)) {
		File= fopen (PalettePath, "r");
		if (File==NULL) {
			MessagePopup("Error", "Could not open Palette file, sorry");
			return;
		}
		if (NULL==fgets (Line, 256, File)) goto Error;	// JASC-PAL
		if (NULL==fgets (Line, 256, File)) goto Error;	// 100
		if (NULL==fgets (Line, 256, File)) goto Error;	// 256
		if (atoi(Line)!=256) goto Error;
		
		if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
		ColorTable=Calloc(1<<ImgPixDepth, int);
		if (ColorTable==NULL) { MessagePopup("Error", "Out of memory, sorry.\nApplication Exit."); exit(1); }

		for (i=0; i<256; i++) {
			if (3 != fscanf (File, "%d %d %d", &R, &G, &B)) goto Error;
			ColorTable[i]=MakeColor(R, G, B);
		}

		fclose(File);
		SetMenuBarAttribute (Menu, MENU_FILTER_GREY_PALETTE, ATTR_CHECKED, FALSE);
		SetMenuBarAttribute (Menu, MENU_FILTER_LOAD_PALETTE, ATTR_CHECKED, TRUE);
		NeedViewUpdate=TRUE;
		if (AutoUpdate) CreateView();
	}
	return;
	

Error:
	MessagePopup("Error", "Palette file seems to be in an invalid format\n"
			"Make sure you used PaintShopPro or GenPalette to create it with 256 colors.");
	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
}


void CVICALLBACK cbm_Options (int menuBar, int menuItem, void *callbackData, int panel) {
	int W, H;
	GetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_WIDTH,  &W); SetCtrlVal(PnlOpt, OPT_THUMB_W, W);
	GetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_HEIGHT, &H); SetCtrlVal(PnlOpt, OPT_THUMB_H, H);
	GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_WIDTH,  &W); SetCtrlVal(PnlOpt, OPT_VIEW_W, W);
	GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_HEIGHT, &H); SetCtrlVal(PnlOpt, OPT_VIEW_H, H);
	GetCtrlAttribute (PnlOgl, OGL_PICTURE, ATTR_WIDTH,  &W); SetCtrlVal(PnlOpt, OPT_OGL_W, W);
	GetCtrlAttribute (PnlOgl, OGL_PICTURE, ATTR_HEIGHT, &H); SetCtrlVal(PnlOpt, OPT_OGL_H, H);
	
	SetCtrlVal(PnlOpt, OPT_SCALE_WIDTH, ScaleWidth);
	SetCtrlVal(PnlOpt, OPT_BOX, BoxColor);
	SetCtrlVal(PnlOpt, OPT_ARROW, ArrowColor);
	SetCtrlVal(PnlOpt, OPT_TEXT, ArrowTextColor);
	SetCtrlVal(PnlOpt, OPT_BACKGROUND, ArrowTextBackground);
//	SetCtrlVal(PnlOpt, OPT_MISSING_MAPPING, MissingColor);

	InstallPopup (PnlOpt);
}

void CVICALLBACK cbm_QuickHelp (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("LargeImage Quick Help", 
				"This program allows you to quickly view binary files as images, even large files.\n"
				"First you should have an idea of the format of your file:\n"
				"- width\n"
				"- height\n"
				"- number of color channels (greyscale, RGB, RGB+A)\n"
				"- if there is any word alignement on lines...\n"
				"\n"
				"After you input those values properly\n"
				"(which may not be easy if you don't know them !),\n"
				"The program displays a thumbnail of the entire file.\n"
				"A red box represents the View Window, click [W] to display it.\n"
				"You can move the view area, change the Reduction/ZoomOut factor,\n"
				"apply basic graphic filters, save the image as JPG or PNG...\n"
				"\n"
				"Right-click on most controls to get specific HELP\n");
}

void CVICALLBACK cbm_Website (int menuBar, int menuItem, void *callbackData, int panel) {
	LaunchExecutable ("EXPLORER.EXE "_WEB_);
}

void CVICALLBACK cbm_About (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("About LargeImage", _HELP_);
}

int CVICALLBACK cb_Argv (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_TIMER_TICK:
			SetCtrlAttribute (PnlCtrl, CTRL_TIMER_ARGV, ATTR_ENABLED, FALSE);
			if (Argv==NULL) cbm_Open(0, 0, NULL, 0);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Thumbnail (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	double Ratio=(double)Width/ThumbWidth;
	int Top;
	BOOL KeyShift, KeyCtrl, KeyAlt;
	
	KeyShift=GetAsyncKeyState(VK_SHIFT)>>8;
	KeyCtrl =GetAsyncKeyState(VK_CONTROL)>>8;
	KeyAlt  =GetAsyncKeyState(VK_MENU)>>8;
	
	switch (event) {
		case EVENT_LEFT_CLICK:
			GetCtrlAttribute (PnlCtrl, CTRL_THUMBNAIL, ATTR_TOP,  &Top);
			Xcenter=eventData2*Ratio;
			Ycenter=(eventData1-Top)*Ratio;
			if (KeyCtrl and Zoom>1) DisplayBox(), SetZoom(Zoom/2);
			else if (KeyShift) DisplayBox(), SetZoom(Zoom*2);
			DisplayBox();
			NeedViewUpdate=TRUE;
			break;
		case EVENT_LEFT_DOUBLE_CLICK:
			CreateView();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Thumbnail View Help", 
						"This is a thumbnail view of the entire file.\n"
						"The red box shows the location of the View Window.\n"
						"Double-click to center and redraw.\n"
						"[+]/[-] to zoom in/out.\n"
						"If this displays only garbage, then you have to try other values\n"
						"in the Properties box.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_MinMax (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	unsigned long V;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &V);
			switch (control) {
				case CTRL_X_MIN:Xcenter=V+Zoom*ViewWidth/2; break;
				case CTRL_X_MAX:Xcenter=V-Zoom*ViewWidth/2+1; break;
				case CTRL_Y_MIN:Ycenter=V+Zoom*ViewHeight/2; break;
				case CTRL_Y_MAX:Ycenter=V-Zoom*ViewHeight/2+1; break;
			}
			DisplayBox();
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Min/Max Coordinates Help", 
						"Those are the coordinates of the View Window\ninside the large file.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_UpdateView (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			CreateView();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Redraw View Window Help", 
						"Click this if you want to automatically redraw the view window.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_View (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Top, Left, X, Y, Color=0xD4D0C8;
	static int Xclick=0, Yclick=0;
	unsigned char V[8];
	char Str[255];
	void *Dest;
	BOOL KeyShift, KeyCtrl, KeyAlt;
	
	KeyShift=GetAsyncKeyState(VK_SHIFT)>>8;
	KeyCtrl =GetAsyncKeyState(VK_CONTROL)>>8;
	KeyAlt  =GetAsyncKeyState(VK_MENU)>>8;

	switch (event) {
		case EVENT_LEFT_CLICK:
			// Find coordinate of click
			GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_LEFT, &Left);
			GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_TOP,  &Top);
			Xclick= Xmin+(eventData2-Left)*Zoom;
			Yclick= Ymin+(eventData1-Top )*Zoom;

			SetCtrlVal(PnlCtrl, CTRL_CLICK_X, X=Xclick);
			SetCtrlVal(PnlCtrl, CTRL_CLICK_Y, Y=Yclick);
			SetGridClick(X,Y,NULL,NULL);

			if (!SetPos(Position(X, Y))) fread(V, FilePixDepth/8, 1, File);
			else memset(V, 0, 8);

//			FromTo(V, Dest, FALSE);		// Doesn't work here as it converts to Byte
			if (Swapped) Dest=SwapEndian(V, FilePixDepth/8);
			else Dest=V;

			#define DEST(Type) 
			switch (FilePixDepth) {
/* B */ 		case  8:sprintf(Str, Signed ? "%d%s" : "%u%s", Signed ? *(char*)Dest : *(unsigned char*)Dest, VertUnit); 
						Color = MakeColor(*(unsigned char*)Dest, *(unsigned char*)Dest, *(unsigned char*)Dest);
						SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
						break;	

/* W */			case 16:sprintf(Str, Signed ? "%d%s" : "%u%s", Signed ? *(short*)Dest : *(unsigned short*)Dest, VertUnit); 
						SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
						break;	

/* RGB */		case 24:Color = MakeColor(((unsigned char*)Dest)[0], ((unsigned char*)Dest)[1], ((unsigned char*)Dest)[2]);
						sprintf(Str, "0x%06X", Color); 
						SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
						break;
			
				case 32: switch (ImgPixDepth+FromFloat) {
/* L */						case 8: sprintf(Str, Signed ? "%d%s" : "%u%s", Signed ? *(long*)Dest : *(unsigned long*)Dest, VertUnit); 
									SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
									break;
					
/* RGBA */					case 32:Color=*(unsigned long*)Dest;
									sprintf(Str, "%08X", Color); 
									SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
									break;
					
/* F */						case 9: sprintf(Str, "%.3e%s", *(float*)Dest, VertUnit); 
									SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
									break;
						} break;
		
/* D */			case 64:sprintf(Str, "%.3e%s", *(double*)Dest, VertUnit); 
						SetCtrlVal (PnlCtrl, CTRL_VALUE, Str);
						break;
		
				default: Breakpoint();
			}
			SetCtrlAttribute (PnlCtrl, CTRL_VALUE, ATTR_TEXT_BGCOLOR, Color);

			if (KeyAlt and KeyShift and KeyCtrl) SetWaypoint(X,Y);
			else if (KeyAlt) {
				X1profile=Xclick;
				Y1profile=Yclick;
				SetCtrlAttribute(PnlPrf, PROF_HELP, ATTR_VISIBLE, TRUE);
				DrawProfile();
			}
			break;

		case EVENT_LEFT_DOUBLE_CLICK:	// recenter and redraw
			Xcenter= Xclick;
			Ycenter= Yclick;
			if (KeyCtrl and Zoom>1) DisplayBox(), SetZoom(Zoom/2);
			else if (KeyShift) DisplayBox(), SetZoom(Zoom*2);
			DisplayBox();
			NeedViewUpdate=TRUE;
			CreateView();
			break;
		
		case EVENT_RIGHT_CLICK:	// recenter and redraw
			if (KeyAlt) {		// Profile
				GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_LEFT, &Left);
				GetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_TOP,  &Top);
				Xclick= Xmin+(eventData2-Left)*Zoom;
				Yclick= Ymin+(eventData1-Top )*Zoom;

				SetCtrlVal(PnlCtrl, CTRL_CLICK_X, X=Xclick);
				SetCtrlVal(PnlCtrl, CTRL_CLICK_Y, Y=Yclick);
				SetGridClick(X,Y,NULL,NULL);

				X2profile=Xclick;
				Y2profile=Yclick;
				SetCtrlAttribute(PnlPrf, PROF_HELP, ATTR_VISIBLE, FALSE);
				DrawProfile();
				break;
			}
			MessagePopup("View help",
						"NumPad/Arrows to move\n"
						"[5] to redraw\n"
						"[+]/[-] to zoom in/out\n"
						"[Ctrl][Tab] to Cycle Panels\n\n"
						"[Left-Click] to get coordinates and pixel properties\n"
						"[Double-left-click] to recenter and redraw\n"
						"[Ctrl][double-click] to zoom in\n"
						"[Shift][double-click] to zoom out\n"
						"[Alt][left-Click] and [Alt][right-click] to select start and end profile points");
			break;
	}
	return 0;
}

//int CVICALLBACK cb_HelpLabel (int panel, int control, int event,
//		void *callbackData, int eventData1, int eventData2) {
//	static BOOL Msg=TRUE;
//	switch (event) {
//		case EVENT_LEFT_CLICK:
//			if (Msg) SetCtrlVal(PnlView, VIEW_TEXTMSG, "Move=NumPad/Arrows, Redraw=[5], Zoom=[+]/[-], Cycle Panels=[Ctrl-Tab]");
//			else SetCtrlVal(PnlView, VIEW_TEXTMSG, "Center=Click, Center+Redraw=Double-Click, Zoom=Double-Right-Click, Coord=Right-Click");
//			Msg=!Msg;
//			break;
//	}
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////
// Swap between histogram views
static void SwapHist(void) {
	static int Visible=2;
	Visible=(Visible+1)%3;
	SetCtrlAttribute(PnlHist, HIST_HISTT, ATTR_VISIBLE, Visible==0);
	SetCtrlAttribute(PnlHist, HIST_HISTV, ATTR_VISIBLE, Visible==1);
	SetCtrlAttribute(PnlHist, HIST_HISTF, ATTR_VISIBLE, Visible==2);
	SetPanelAttribute (PnlHist, ATTR_TITLE, 
			Visible==0 ? "Thumbnail Histogram" : 
			Visible==1 ? "View Histogram (pre-filter)" :
			"View Histogram (post-filter)");
}

int CVICALLBACK cb_HistT (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_LEFT_DOUBLE_CLICK:
			SwapHist();
			break;			
		case EVENT_RIGHT_CLICK:
			MessagePopup("Thumbnail Histogram Help",
					"This is the histogram window of the Thumbnail image.\n"
					"The horizontal axis is the color, from black to white/red/green/blue/alpha.\n"
					"And the vertical axis is the density.\n\n"
					"Use the left click to display the View histogram.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_HistV (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_LEFT_DOUBLE_CLICK:
			SwapHist();
			break;			
		case EVENT_COMMIT:
		case EVENT_RIGHT_CLICK:
			MessagePopup("Unfiltered View Histogram",
					"This is the histogram window of the View window (before application of the filter).\n"
					"The horizontal axis is the color, from black to white/red/green/blue/alpha.\n"
					"And the vertical axis is the density.\n\n"
					"Use the left click to display the histogram of the filtered view.\n"
					"The purple bars that you see show the limits of the histogram (0.5% tolerance)\n"
					"which are used by the [Filter][Histogram Stretch], the left one becomes black while\n"
					"the right one becomes white.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_HistF (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_LEFT_DOUBLE_CLICK:
			SwapHist();
			break;			
		case EVENT_COMMIT:
		case EVENT_RIGHT_CLICK:
			MessagePopup("Filtered View Histogram Help",
					"This is the histogram window of the View window AFTER\n"
					"the application of the selected filter (if any).\n"
					"The horizontal axis is the color, from black to white/red/green/blue/alpha.\n"
					"And the vertical axis is the density.\n\n"
					"Use the left click to display the Thumbnail histogram.");
			break;
	}
	return 0;
}



int CVICALLBACK cb_OptionsOK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int W, H, Wnew, Hnew, Left, Top;
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup (0);
			GetCtrlVal(PnlOpt, OPT_AUTO_UPDATE, &AutoUpdate);
			GetCtrlVal(PnlOpt, OPT_STRETCH_THRESHOLD, &StretchThreshold);
			GetCtrlVal(PnlOpt, OPT_STRETCH_IGNORE, &StretchIgnore);
			GetCtrlVal(PnlOpt, OPT_ZBOX, &ZBox); SetZBox();
			GetCtrlVal(PnlOpt, OPT_JPG_COMP, &JpgComp);
			GetCtrlVal(PnlOpt, OPT_PNG_COMP, &PngComp);
			GetCtrlVal(PnlOpt, OPT_PNG_GAMMA, &PngGamma);
			GetCtrlVal(PnlOpt, OPT_PNG_RESOLUTION, &PngResolution);
			GetCtrlVal(PnlOpt, OPT_PNG_AUTHOR, TxtAuthor);
			GetCtrlVal(PnlOpt, OPT_PNG_DESCRIPTION, TxtDescription);
			GetCtrlVal(PnlOpt, OPT_PNG_INTERLACE, &PngInterlace);
			GetCtrlVal(PnlOpt, OPT_PNG_ALPHA32, &PngAlpha32);
			GetCtrlVal(PnlOpt, OPT_SCALE_WIDTH, &ScaleWidth);
			
			GetCtrlVal(PnlOpt, OPT_BOX, &BoxColor);
			GetCtrlVal(PnlOpt, OPT_ARROW, &ArrowColor);
			GetCtrlVal(PnlOpt, OPT_TEXT, &ArrowTextColor);
			GetCtrlVal(PnlOpt, OPT_BACKGROUND, &ArrowTextBackground);
//			GetCtrlVal(PnlOpt, OPT_MISSING_MAPPING, &MissingColor);

			// Check if sizes have changed:
			GetCtrlAttribute(PnlCtrl, CTRL_THUMBNAIL, ATTR_WIDTH,  &W); 
			GetCtrlAttribute(PnlCtrl, CTRL_THUMBNAIL, ATTR_HEIGHT, &H); 
			GetCtrlVal(PnlOpt, OPT_THUMB_W, &Wnew);
			GetCtrlVal(PnlOpt, OPT_THUMB_H, &Hnew);
			if (W!=Wnew or H!=Hnew) {
				SetPanelAttribute(PnlCtrl, ATTR_WIDTH,  Wnew);
				cbp_Ctrl(PnlCtrl, EVENT_PANEL_SIZE, NULL, 0, 0);
			}
			
			GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_WIDTH,  &W); 
			GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_HEIGHT, &H); 
			GetCtrlVal(PnlOpt, OPT_VIEW_W, &Wnew);
			GetCtrlVal(PnlOpt, OPT_VIEW_H, &Hnew);
			if (W!=Wnew or H!=Hnew) {
				GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_LEFT,&Left); 
				GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_TOP, &Top);  
				SetPanelAttribute(PnlView, ATTR_WIDTH,  Wnew+Left);
				SetPanelAttribute(PnlView, ATTR_HEIGHT, Hnew+Top);
				cbp_View(PnlView, EVENT_PANEL_SIZE, NULL, 0, 0);
			}
			
			GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_WIDTH,  &W); 
			GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_HEIGHT, &H); 
			GetCtrlVal(PnlOpt, OPT_OGL_W, &Wnew);
			GetCtrlVal(PnlOpt, OPT_OGL_H, &Hnew);
			if (W!=Wnew or H!=Hnew) {
				GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_LEFT,&Left); 
				GetCtrlAttribute(PnlOgl, OGL_PICTURE, ATTR_TOP, &Top);  
				SetPanelAttribute(PnlOgl, ATTR_WIDTH,  Wnew+Left);
				SetPanelAttribute(PnlOgl, ATTR_HEIGHT, Hnew+Top);
				cbp_OGL(PnlOgl, EVENT_PANEL_SIZE, NULL, 0, 0);
			}
			break;
	}
	return 0;
}

int CVICALLBACK cb_OptionsCancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup (0);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

static void DrawUnits(void) {
	int Left, Height;
	char Str[255]="";
	
	GetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_HEIGHT, &Height);
	Height-=15;	// We draw near the bottom

	// is there something to print ?
	if (UnitsPerPixel==0. or Unit=='\0' or strcmp(Unit, "")==0
		or strcmp(Unit, "pix")==0 or strcmp(Unit, "Pix")==0) {
		return;
	}
	
//	SetCtrlAttribute(PnlView, VIEW_SCALE, ATTR_PEN_FILL_COLOR, VAL_RED);
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_STYLE, VAL_SOLID);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_STYLE, VAL_SOLID);

	// Make a thicker background
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 3);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_WIDTH, 3);
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ArrowTextBackground);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_COLOR, ArrowTextBackground);
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2,Height+10), MakePoint(2+ScaleWidth-1,Height+10));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2,Height+10), MakePoint(2+ScaleWidth-1,Height+10));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2,Height+10), MakePoint(2,Height+5));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2,Height+10), MakePoint(2,Height+5));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2+ScaleWidth-1,Height+10), MakePoint(2+ScaleWidth-1,Height+5));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2+ScaleWidth-1,Height+10), MakePoint(2+ScaleWidth-1,Height+5));

	// Draw thin line
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ArrowColor);
	SetCtrlAttribute(PnlView, VIEW_MAP , ATTR_PEN_COLOR, ArrowColor);
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2,Height+10), MakePoint(2+ScaleWidth-1,Height+10));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2,Height+10), MakePoint(2+ScaleWidth-1,Height+10));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2,Height+10), MakePoint(2,Height+5));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2,Height+10), MakePoint(2,Height+5));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(2+ScaleWidth-1,Height+10), MakePoint(2+ScaleWidth-1,Height+5));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(2+ScaleWidth-1,Height+10), MakePoint(2+ScaleWidth-1,Height+5));


	sprintf(Str, "%.0f%s", Zoom*ScaleWidth*UnitsPerPixel, Unit);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ArrowTextColor);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_COLOR, ArrowTextColor);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, ArrowTextBackground);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_FILL_COLOR, ArrowTextBackground);
	CanvasDrawTextAtPoint (PnlView, VIEW_VIEW, Str, VAL_APP_META_FONT,
						   MakePoint (2+ScaleWidth+1, Height+5), VAL_CENTER_LEFT);
	CanvasDrawTextAtPoint (PnlView, VIEW_MAP , Str, VAL_APP_META_FONT,
						   MakePoint (2+ScaleWidth+1, Height+5), VAL_CENTER_LEFT);
}


static void DrawArrow(void) {
	static int DiameterArrow=25, DiameterBox=55;
	int Top, Left, Height, Width, 
		ArrowCenterX, ArrowCenterY, 	// Center of the arrow, different form where the arrow is pointing
		Cx, Cy, Px, Py, H, W;
	double Alpha=0, BarbAngle=30./*deg*/, BarbLenght=DiameterArrow/5;
	
	if (strlen(ArrowText)==0) return;
	
	ArrowCenterX=Xmin+DiameterBox/2*Zoom; // DiameterBox/2 in View coord
	ArrowCenterY=Ymin+DiameterBox/2*Zoom; // DiameterBox/2 in View coord
	Alpha=atan2(ArrowY-ArrowCenterY, ArrowX-ArrowCenterX);
	Cx=DiameterArrow*cos(Alpha);
	Cy=DiameterArrow*sin(Alpha);
	
	
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_STYLE, VAL_SOLID);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_STYLE, VAL_SOLID);

	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 3);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_WIDTH, 3);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ArrowTextBackground);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_COLOR, ArrowTextBackground);
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(   DiameterBox/2-Cx,    DiameterBox/2-Cy), 
										MakePoint(Px=DiameterBox/2   , Py=DiameterBox/2));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(   DiameterBox/2-Cx,    DiameterBox/2-Cy), 
										MakePoint(Px=DiameterBox/2   , Py=DiameterBox/2));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI+DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI+DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI+DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI+DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI-DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI-DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI-DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI-DEG_TO_RAD(BarbAngle))));

	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ArrowColor);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_COLOR, ArrowColor);
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(   DiameterBox/2-Cx,    DiameterBox/2-Cy), 
										MakePoint(Px=DiameterBox/2   , Py=DiameterBox/2));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(   DiameterBox/2-Cx,    DiameterBox/2-Cy), 
										MakePoint(Px=DiameterBox/2   , Py=DiameterBox/2));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI+DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI+DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI+DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI+DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI-DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI-DEG_TO_RAD(BarbAngle))));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint(Px, Py), 
										MakePoint(Px+BarbLenght*cos(Alpha+PI-DEG_TO_RAD(BarbAngle)), 
										  		  Py+BarbLenght*sin(Alpha+PI-DEG_TO_RAD(BarbAngle))));


	GetTextDisplaySize (ArrowText, VAL_APP_META_FONT, &H, &W);
	Cx=Max(H,W)/2*cos(Alpha);
	Cy=Max(H,W)/2*sin(Alpha);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR,      ArrowTextColor);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_COLOR,      ArrowTextColor);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, ArrowTextBackground);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_FILL_COLOR, ArrowTextBackground);
	CanvasDrawTextAtPoint (PnlView, VIEW_VIEW, ArrowText, VAL_APP_META_FONT, 
				MakePoint(DiameterBox/2+Cx,DiameterBox/2+Cy), VAL_CENTER);			
	CanvasDrawTextAtPoint (PnlView, VIEW_MAP , ArrowText, VAL_APP_META_FONT, 
				MakePoint(DiameterBox/2+Cx,DiameterBox/2+Cy), VAL_CENTER);			
}


int CVICALLBACK cb_OptionsUnit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	long V;
	switch (event) {
		case EVENT_COMMIT:
			switch (control) {
				case PROP_UNITPERPIX:GetCtrlVal(PnlProp, PROP_UNITPERPIX, &UnitsPerPixel);	
									/* if (UnitsPerPixel==0) UnitsPerPixel=1;*/ DrawUnits(); break;
				case PROP_UNIT:		GetCtrlVal(PnlProp, PROP_UNIT, 		Unit); 	DrawUnits(); break;
				case PROP_VERTUNIT:	GetCtrlVal(PnlProp, PROP_VERTUNIT, 	VertUnit); 		
									NeedViewUpdate=TRUE;
									if (AutoUpdate) {
										CreateThumbnail();	// Needed to display the min/max again
										CreateView();
									}
									break;
			}
			break;
			
		case EVENT_RIGHT_CLICK:
			MessagePopup("Advanced Options Help",
					"\nUnits/pixel       --  Number of chosen units per pixel.\n"
					"Unit              --  Horizontal measurement unit. For instance if you are viewing a map where 200m = 1 pixel,\n"
					"                      enter 200 and 'm' or better 0.2 and 'km' in those two boxes.\n"
					"Vertical unit     --  Measurement unit of the values in the file (for instance vertical).\n"
					"Arrow To          --  Coordinates where to point an arrow (for instance 'South' to 150-150).");
			break;
	}
	return 0;
}

int CVICALLBACK cb_OptionsHelp (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	long V;
	switch (event) {
		case EVENT_VAL_CHANGED:
			switch (control) {
				case OPT_THUMB_H:	GetCtrlVal(PnlOpt, OPT_THUMB_H, &V); SetCtrlVal(PnlOpt, OPT_THUMB_W, V*Width/Height); break;
				case OPT_THUMB_W:	GetCtrlVal(PnlOpt, OPT_THUMB_W, &V); SetCtrlVal(PnlOpt, OPT_THUMB_H, V*Height/Width); break;
			}
			break;


		case EVENT_RIGHT_CLICK:
			MessagePopup("Advanced Options Help",
					"Auto Update View  --  Will redraw the View window each time something changes\n"
					"Stretch Threshold --  Determines the amount of points to exclude from the histogram. 0 to 50%. Default 0.5%.\n"
					"Stretch Ignore    --  Number of colors to ignore a the begining and end of the histogram. Default 2.\n"
					"Z box 3D          --  Flattening of the vertical axis in 3D mode. 100% gives you a cube;\n"
					"                      you can also modify it directly in the [Properties] popup, at [Z axis][3D][Plot Are][Z axis][Start and Size]\n"
					"Pixels on scale   --  Number of pixels on the displayed distance scale.\n"
					"Sizes             --  Manual sizes of the Thumbnail, View and 3D windows.\n"
					"\nJPG Quality       --  Higher quality means bigger image. From 0 to 100 (no compression). Default 75.\n"
					"\nPNG Compression   --  Default 6. Higher compression means longer time to save, but no quality difference.\n"
					"PNG Gamma         --  Gamma ratio of the image to save. Default on a PC monitor: 0.45455.\n"
					"PNG Resolution    --  Resolution of the image in pixels per mm.\n"
					"PNG Author        --  Name of the author of the image.\n"
					"PNG Description   --  Description of the image.\n"
					"PNG Interlaced    --  Wether the image is interlaced or not.\n"
					"PNG save alpha    --  If the image is 32 bit RGB, keep the Alpha channel when saving.");
			break;
	}
	return 0;
}



// int PnlView=0, PnlProp=0, PnlCtrl=0, PnlHist=0, PnlOpt=0, PnlOgl=0, PnlMap=0, PnlPrf=0, PnlGrid=0, PnlWP=0, Menu=0;

void NextPanel(int Panel) {
	int Visible;
	
	if (Panel==PnlCtrl) Panel=PnlView;
	else if (Panel==PnlView) Panel=PnlHist;
	else if (Panel==PnlHist) Panel=PnlOgl;
	else if (Panel==PnlOgl)  Panel=PnlPrf;
	else if (Panel==PnlPrf)  Panel=PnlWP;
	else if (Panel==PnlWP)   Panel=PnlCtrl;
	else Panel=PnlCtrl;
	
	GetPanelAttribute (Panel, ATTR_VISIBLE, &Visible);
	if (!Visible) NextPanel(Panel);
	else SetActivePanel(Panel);
}


void PreviousPanel(int Panel) {
	int Visible;
	
	if (Panel==PnlView) Panel=PnlCtrl;
	else if (Panel==PnlHist) Panel=PnlView;
	else if (Panel==PnlOgl)  Panel=PnlHist;
	else if (Panel==PnlPrf)  Panel=PnlOgl;
	else if (Panel==PnlWP)   Panel=PnlPrf;
	else if (Panel==PnlCtrl) Panel=PnlWP;
	else Panel=PnlCtrl;
	
	GetPanelAttribute (Panel, ATTR_VISIBLE, &Visible);
	if (!Visible) PreviousPanel(Panel);
	else SetActivePanel(Panel);
}


#if 0    /* formerly excluded lines */
#define W 1024
#define H 768
unsigned char  Source[W*H];
unsigned short Dest16[W*H];
unsigned long  Dest32[W*H];
int RGBA[W*H];
float Float[W*H];
double Double[W*H];

int main (int argc, char *argv[]) {
	FILE *SourceFile=fopen("CorkscrewGrey.raw", "rb"), *DestFile;
	int i;
	
	fread (Source, 1, W*H, SourceFile);
	fclose(SourceFile);
	
	for (i=0; i<W*H; i++) {
		Dest16[i]=Source[i]<<8;
		Dest32[i]=Source[i]<<24;
		Float[i]=Source[i]*10;
		Double[i]=(Source[i]-100)*3.1415e5;
		RGBA[i]=MakeColor(Source[i], Source[i], Source[i]);
	}
	
	DestFile=fopen("Bit16.raw", "wb");
	fwrite (Dest16, sizeof(short), W*H, DestFile);
	fclose(DestFile);

	DestFile=fopen("Bit32.raw", "wb");
	fwrite (Dest32, sizeof(long), W*H, DestFile);
	fclose(DestFile);
	
	DestFile=fopen("RGBA.raw", "wb");
	fwrite (RGBA, sizeof(int), W*H, DestFile);
	fclose(DestFile);

	DestFile=fopen("Float.raw", "wb");
	fwrite (Float, sizeof(float), W*H, DestFile);
	fclose(DestFile);
	
	DestFile=fopen("Double.raw", "wb");
	fwrite (Double, sizeof(double), W*H, DestFile);
	fclose(DestFile);
	
	return 0;
}
#endif   /* formerly excluded lines */




///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ProfileClose (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			HidePanel(PnlPrf);
			X2profile=X1profile=Y2profile=Y1profile=-1;
			break;
	}
	return 0;
}

int CVICALLBACK cbp_Profile(int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int W, H, Height;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(PnlPrf, ATTR_WIDTH,  &W);
			GetPanelAttribute(PnlPrf, ATTR_HEIGHT, &H);
			SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_WIDTH,  W);
			SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_HEIGHT, H);
			SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_LEFT,   0);
			SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_TOP,    0);

			SetCtrlAttribute (PnlPrf, PROF_PROPERTIES, ATTR_LEFT, 0);
			GetCtrlAttribute (PnlPrf, PROF_PROPERTIES, ATTR_HEIGHT, &Height);
			SetCtrlAttribute (PnlPrf, PROF_PROPERTIES, ATTR_TOP, H-Height);
			break;

		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.
			}
			break;

	}
	return 0;
}

static void DrawProfileLine(void) {
	// Draw line on view
	if (X2profile==-1 or X1profile==-1 or Y2profile==-1 or Y1profile==-1) return;

	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_COLOR, ProfileColor);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_COLOR, ProfileColor);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (PnlView, VIEW_VIEW, ATTR_PEN_STYLE, VAL_SOLID);
	SetCtrlAttribute (PnlView, VIEW_MAP , ATTR_PEN_STYLE, VAL_SOLID);
	CanvasDrawLine (PnlView, VIEW_VIEW, MakePoint((X1profile-Xmin)/Zoom, (Y1profile-Ymin)/Zoom), 
										MakePoint((X2profile-Xmin)/Zoom, (Y2profile-Ymin)/Zoom));
	CanvasDrawLine (PnlView, VIEW_MAP , MakePoint((X1profile-Xmin)/Zoom, (Y1profile-Ymin)/Zoom), 
										MakePoint((X2profile-Xmin)/Zoom, (Y2profile-Ymin)/Zoom));
}


static void DrawProfile(void) {
	double *Array=NULL, Dist, D;
	int i, X, Y, Progress, Xmap, Ymap;
	unsigned char V[8], *ArrayMap=NULL;
	unsigned long Seek;
	char Dest[4], *Pos, *W;
	
	if (X2profile==-1 or X1profile==-1 or Y2profile==-1 or Y1profile==-1) return;
	SetCtrlAttribute(PnlPrf, PROF_HELP, ATTR_VISIBLE, FALSE);
	
	DrawProfileLine();
	
	Dist=sqrt((X2profile-X1profile)*(X2profile-X1profile) + (Y2profile-Y1profile)*(Y2profile-Y1profile));
	Array=(double*)calloc(Dist+1, sizeof(double));
	ArrayMap=(unsigned char*)calloc(Dist+1, 1);
	if (Array==NULL or ArrayMap==NULL) { MessagePopup("Error", "Out of Memory, exiting program"); exit(1); }

	Progress = CreateProgressDialog ("Computing profile", "Percent Complete", 1, VAL_FULL_MARKERS, "");
	for (i=0; i<=Dist; i++) {
		UpdateProgressDialog (Progress, 100*i/Dist, 0);
		X=(X2profile-X1profile)*i/Dist+X1profile;
		Y=(Y2profile-Y1profile)*i/Dist+Y1profile;
		
		if (!SetPos(Position(X, Y))) {
			fread(V, FilePixDepth/8, 1, File);
			FromTo(V, &D, TRUE);
		}
		else D=0;
		Array[i]=D;

		if (MapFile!=NULL and MapPixDepth==8) {
			Ymap=YViewToMap(X,Y);
			Xmap=XViewToMap(X,Y);
			Seek=MapPosition(Xmap, Ymap);
			if (0</*=*/Seek and Seek<MapSize and 
				0==fseek (MapFile, Seek, SEEK_SET)) {
				fread(Dest, MapPixDepth/8, 1, MapFile);
				if (MapSwapped) W=SwapEndian(Dest, MapPixDepth/8);
				else W=Dest;
				switch (ImgPixDepth) {
					case 8: ArrayMap[i]=W[0];	break;
				}
			} else ArrayMap[i]=0;
		}
	} 

	DiscardProgressDialog (Progress);

	DeleteGraphPlot (PnlPrf, PROF_PROFILE, -1, VAL_NO_DRAW);
	SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
	PlotWaveform (PnlPrf, PROF_PROFILE, Array, Dist+1, VAL_DOUBLE, 1.0, 0.0,
				  0.0, UnitsPerPixel, VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, ProfileColor);


	if (UnitsPerPixel!=0. and Unit!=NULL and strcmp(Unit, "")!=0
		and strcmp(Unit, "pix")!=0 and strcmp(Unit, "Pix")!=0) {
		SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_XLABEL_VISIBLE, TRUE);
		SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_XNAME, Unit);
	}
	else SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_XLABEL_VISIBLE, FALSE);
	
	if (strlen(VertUnit)>0) {
		SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_YLABEL_VISIBLE, TRUE);
		SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_YNAME, VertUnit);
	}
	else SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_YLABEL_VISIBLE, FALSE);
	
	// Draw mapping profile
	SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
	SetCtrlAttribute (PnlPrf, PROF_PROFILE, ATTR_YLABEL_VISIBLE, MapFile!=NULL and MapPixDepth==8);
	if (MapFile!=NULL and MapPixDepth==8)
		PlotWaveform (PnlPrf, PROF_PROFILE, ArrayMap, Dist+1, VAL_UNSIGNED_CHAR, 1.0, 0.0, 0.0,
				  UnitsPerPixel, VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, ProfileMapColor);

	SetActivePanel(PnlPrf);
	free(Array); Array=NULL;
	free(ArrayMap); ArrayMap=NULL;
}



int CVICALLBACK cb_ProfileProperties (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GraphPropertiesPopup(PnlPrf, PROF_PROFILE, 0); 
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help", "This opens up a popup that enables you to change the graph parameters");
			break;
	}
	return 0;
}

