#include <windows.h>

#include <cvirte.h>		
#include <userint.h>
#include <toolbox.h>
#include <wintools.h>

#include "Def.h"
#include "LargeImage.h"
#include "Externs.h"

static int NbWP=0;
static int WaypointColor=VAL_GREEN, WaypointBcgColor=VAL_BLACK;
static long Xcenter, Ycenter;

typedef struct sCoord {
	BOOL Disp;	// Display the waypoint or not;
	union { int X; double Lat; };
	union { int Y; double Lon; };
	BOOL Grid;		// If true, use Lat/Lon, otherwise X/Y
	char *Label;
} tCoord;

//static tCoord *WaypointList=NULL;
static ListType WaypointList=NULL;
static BOOL ChangedWP=FALSE, DoWP=FALSE;

static char WaypointPath[MAX_PATHNAME_LEN]="";
    
///////////////////////////////////////////////////////////////////////////////
void DisplayWaypoints(void) {
	int i, X, Y;
	tCoord Waypoint;
	const int R=4;	// Radius of the dot
	Point P;

	if (!DoWP) return;
	
	DeleteTableRows (PnlWP, WP_TABLE, 1, -1);

	for (i=0; i<NbWP; i++) {
		ListGetItem (WaypointList, &Waypoint, i+1);
		
		if (Waypoint.Disp) {
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, R+1);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_WIDTH, R+1);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_COLOR, WaypointBcgColor);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_COLOR, WaypointBcgColor);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_MODE, VAL_COPY_MODE);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_MODE, VAL_COPY_MODE);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_STYLE, VAL_SOLID);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_STYLE, VAL_SOLID);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, WaypointBcgColor);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_FILL_COLOR, WaypointBcgColor);	// For text and dot

			if (Waypoint.Grid) {
				GetGridPos(Waypoint.Lat, Waypoint.Lon, &X, &Y);
				if (X==-1 and Y==-1) continue;	// invalid value returned
				CanvasDrawPoint (PnlView, VIEW_VIEW, P=MakePoint(Xf2v(X), Yf2v(Y)));
			} else 
				CanvasDrawPoint (PnlView, VIEW_VIEW, P=MakePoint(Xf2v(Waypoint.X), Yf2v(Waypoint.Y)));
			CanvasDrawPoint (PnlView, VIEW_MAP , P);
		
			// Draw central dot
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_WIDTH, R);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_WIDTH, R);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_COLOR, WaypointColor);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_COLOR, WaypointColor);
			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, WaypointColor);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_FILL_COLOR, WaypointColor);	// For text and dot
			CanvasDrawPoint (PnlView, VIEW_VIEW, P);
			CanvasDrawPoint (PnlView, VIEW_MAP , P);

			SetCtrlAttribute(PnlView, VIEW_VIEW, ATTR_PEN_FILL_COLOR, WaypointBcgColor);
			SetCtrlAttribute(PnlView, VIEW_MAP,  ATTR_PEN_FILL_COLOR, WaypointBcgColor);	// For text and dot
			CanvasDrawTextAtPoint (PnlView, VIEW_VIEW, Waypoint.Label, VAL_APP_META_FONT, MakePoint(P.x+R+3,P.y), VAL_CENTER_LEFT);
			CanvasDrawTextAtPoint (PnlView, VIEW_MAP , Waypoint.Label, VAL_APP_META_FONT, MakePoint(P.x+R+3,P.y), VAL_CENTER_LEFT);
		}
		
		InsertTableRows (PnlWP, WP_TABLE, i+1, 1, VAL_USE_MASTER_CELL_TYPE);
		
		SetTableCellVal(PnlWP, WP_TABLE, MakePoint(1,i+1), Waypoint.Disp);
	 	SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_CELL_TYPE, VAL_CELL_NUMERIC);
	 	SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_CELL_TYPE, VAL_CELL_NUMERIC);
	 	if (Waypoint.Grid) {
		 	SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_DATA_TYPE, VAL_DOUBLE);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_DATA_TYPE, VAL_DOUBLE);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_PRECISION, 4);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_PRECISION, 4);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_MIN_VALUE, -90.);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_MIN_VALUE, -180.);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_MAX_VALUE, 90.);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_MAX_VALUE, 180.);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_CHECK_RANGE, VAL_COERCE);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_CHECK_RANGE, VAL_COERCE);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_CTRL_VAL, Waypoint.Lat);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_CTRL_VAL, Waypoint.Lon);
		} else {
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_DATA_TYPE, VAL_INTEGER);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_DATA_TYPE, VAL_INTEGER);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_MIN_VALUE, 0);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_MIN_VALUE, 0);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_MAX_VALUE, Width);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_MAX_VALUE, Height);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_CHECK_RANGE, VAL_NOTIFY);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_CHECK_RANGE, VAL_NOTIFY);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,2,1,1), ATTR_CTRL_VAL, Waypoint.X);
			SetTableCellRangeAttribute(PnlWP, WP_TABLE, MakeRect(i+1,3,1,1), ATTR_CTRL_VAL, Waypoint.Y);
		}
//		SetTableCellVal(PnlWP, WP_TABLE, MakePoint(2,i+1), (double)Waypoint.X);
//		SetTableCellVal(PnlWP, WP_TABLE, MakePoint(3,i+1), (double)Waypoint.Y);

		SetTableCellVal(PnlWP, WP_TABLE, MakePoint(4,i+1), Waypoint.Label);
	}
}


///////////////////////////////////////////////////////////////////////////////
// Set a waypoint at specific file coordinates
void SetWaypoint(int X, int Y) {
	double Lat=0, Lon=0;
	tCoord Waypoint;
	
	SetGridClick(X, Y, &Lat, &Lon);
	if (Lat==0 and Lon==0) { 
		Waypoint.Grid=FALSE; Waypoint.X=X; Waypoint.Y=Y; }
	else { 
		Waypoint.Grid=TRUE; Waypoint.Lat=Lat; Waypoint.Lon=Lon; }
	Waypoint.Disp=TRUE;
	Waypoint.Label=malloc(255);
	sprintf(Waypoint.Label, "WayPoint%03d", NbWP+1);

	if (NbWP==0) WaypointList = ListCreate(sizeof(tCoord));
	ListInsertItem(WaypointList, &Waypoint, END_OF_LIST);
	
	NbWP++;	// Tables are 1-based
//	InsertTableRows (PnlWP, WP_TABLE, -1, 1, VAL_USE_MASTER_CELL_TYPE);
	
	
	NeedViewUpdate=TRUE;
	if (AutoUpdate) CreateView();
	else DisplayWaypoints();
	DisplayPanel(PnlWP);
	SetActiveTableCell (PnlWP, WP_TABLE, MakePoint(4,NbWP));
	ChangedWP=TRUE;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Table (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Size, X, Y;
	tCoord Waypoint;	
	BOOL KeyShift=GetAsyncKeyState(VK_SHIFT)>>8, 
		 KeyCtrl =GetAsyncKeyState(VK_CONTROL)>>8, 
		 KeyAlt  =GetAsyncKeyState(VK_MENU)>>8;

	switch (event) {
		case EVENT_COMMIT:
			ListGetItem(WaypointList, &Waypoint, eventData1);	// Row
			
			switch (eventData2) {	// Column
				case 1:GetTableCellVal(PnlWP, WP_TABLE, MakePoint(eventData2,eventData1), &Waypoint.Disp); break;
				case 2:GetTableCellVal(PnlWP, WP_TABLE, MakePoint(eventData2,eventData1), Waypoint.Grid ? (void*)&Waypoint.Lat : (void*)&Waypoint.X); break;
				case 3:GetTableCellVal(PnlWP, WP_TABLE, MakePoint(eventData2,eventData1), Waypoint.Grid ? (void*)&Waypoint.Lon : (void*)&Waypoint.Y); break;
				case 4://GetCtrlAttribute(panel, control, ATTR_STRING_TEXT_LENGTH, &Size); 
						GetTableCellVal(PnlWP, WP_TABLE, MakePoint(eventData2,eventData1), Waypoint.Label);
						break;
			}
			
			ChangedWP=TRUE;
			ListReplaceItem(WaypointList, &Waypoint, eventData1);
			NeedViewUpdate=TRUE;
			if (AutoUpdate) CreateView();
			else DisplayWaypoints();
			break;

// Does not return row...
#if 0    /* formerly excluded lines */
		case EVENT_LEFT_DOUBLE_CLICK:	
			ListGetItem(WaypointList, &Waypoint, eventData1);	// Row
			if (Waypoint.Grid) {
				GetGridPos(Waypoint.Lat, Waypoint.Lon, &X, &Y);
				if (X==-1 and Y==-1) return 1;	// invalid value returned
				Xcenter=X;
				Ycenter=Y;
			} else {
				Xcenter=Waypoint.X;
				Ycenter=Waypoint.Y;
			}
			DisplayBox();
			NeedViewUpdate=TRUE;
			CreateView();
			return 1;

		case EVENT_LEFT_CLICK:	
			if (KeyShift and KeyCtrl and KeyAlt) {
				ListRemoveItem (WaypointList, 0, eventData1);
				DeleteTableRows(PnlWP, WP_TABLE, eventData1, 1);
				NbWP--;
				return 1;
			}
#endif   /* formerly excluded lines */

		case EVENT_RIGHT_CLICK:
			MessagePopup("Waypoint List", "This is the list of waypoints\n"
				"Waypoints are referenced either by X/Y coordinates\n"
				"(dependant on the displayed file)\n"
				"or by Lat/Lon if a Grid referencing is active\n"
				"(in that case it depends only on the mpp or gpd reference file).\n"
				"Waypoints are added by doing [Shift][Ctrl][Alt][Left-click] on the view window.\n"
//				"They are removed by doing [Shift][Ctrl][Alt][Left-click] on the table\n"
//				"Use [Double-left-click] to center and draw the map around a waypoint"
				);
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_WaypointsOK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			SetMenuBarAttribute(Menu, MENU_FILE_WAYPOINTS, ATTR_CHECKED, DoWP=FALSE);
			HidePanel(PnlWP);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Waypoints (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Width, Height, Top, Left, W1, W2, W3;
	switch (event) {
		case EVENT_KEYPRESS:	
			switch (eventData1) {
				case VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_RIGHT_ARROW_VKEY:
					NextPanel(panel); 
					return 1;	// Ctrl Tab to go from one window to the next.
				
				case VAL_SHIFT_MODIFIER |  VAL_MENUKEY_MODIFIER | VAL_TAB_VKEY:
				case VAL_MENUKEY_MODIFIER | VAL_SHIFT_MODIFIER | VAL_LEFT_ARROW_VKEY:
					PreviousPanel(panel); 
					return 1;		// Ctrl Tab to go from one window to the previous.
			}
			break;


		case EVENT_DISCARD:
		case EVENT_END_TASK:
		case EVENT_CLOSE:
			if (ChangedWP and ConfirmPopup ("Save current waypoints ?",
							  "You have changed the current waypoint list,\n"
							  "Would you like to save it ?"))
				cb_SaveWaypoints(0, 0, EVENT_COMMIT, NULL, 0, 0);
			ChangedWP=FALSE;
			break;
		
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(PnlWP, ATTR_WIDTH, &Width);
			GetPanelAttribute(PnlWP, ATTR_HEIGHT, &Height);
			if (Width<100) SetPanelSize(PnlWP, Height, Width=100);
			if (Height<50) SetPanelSize(PnlWP, Height=50, Width);
			GetCtrlAttribute(PnlWP, WP_TABLE, ATTR_TOP,  &Top);
			GetCtrlAttribute(PnlWP, WP_TABLE, ATTR_LEFT, &Left);
			SetCtrlAttribute(PnlWP, WP_TABLE, ATTR_WIDTH, Width-Left);
			SetCtrlAttribute(PnlWP, WP_TABLE, ATTR_HEIGHT, Height-Top);

			GetTableColumnAttribute (PnlWP, WP_TABLE, 1, ATTR_COLUMN_ACTUAL_WIDTH, &W1);
			GetTableColumnAttribute (PnlWP, WP_TABLE, 2, ATTR_COLUMN_ACTUAL_WIDTH, &W2);
			GetTableColumnAttribute (PnlWP, WP_TABLE, 3, ATTR_COLUMN_ACTUAL_WIDTH, &W3);
			SetTableColumnAttribute (PnlWP, WP_TABLE, 4, ATTR_COLUMN_WIDTH, 
				Width-W1-W2-W3>50 ? Width-W1-W2-W3-14 : 50);	// -14 because of right bar width
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Menu
void CVICALLBACK cbm_Waypoints (int menuBar, int menuItem, void *callbackData, int panel) {
	int Err;
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "WaypointColor", (unsigned long*)&WaypointColor, 1);
	Err=WT_RegReadULong (WT_KEY_HKCU, RegPath, "WaypointBcgColor", (unsigned long*)&WaypointBcgColor, 1);
	SetCtrlVal(PnlWP, WP_COLOR, WaypointColor);
	SetCtrlVal(PnlWP, WP_BCG_COLOR, WaypointBcgColor);
	DisplayPanel(PnlWP);
	SetMenuBarAttribute(Menu, MENU_FILE_WAYPOINTS, ATTR_CHECKED, DoWP=TRUE);
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_LoadWaypoints (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tCoord Waypoint;
	FILE *File;
	char Buff[256];
	int Nb;
	char Disp, *sXLat, *sYLon, *Label;
	BOOL ErrorAlreadyFound=FALSE;
	
	switch (event) {
		case EVENT_COMMIT:
			if (ChangedWP and ConfirmPopup ("Save current waypoints ?",
							  "You have changed the current waypoint list,\n"
							  "Would you like to save it ?"))
				cb_SaveWaypoints(0, 0, EVENT_COMMIT, NULL, 0, 0);
				
			if (VAL_EXISTING_FILE_SELECTED==FileSelectPopup ("", "*.wpt", "*.wpt;*.csv",
				 "Select a waypoint list file", VAL_SELECT_BUTTON, 0, 0, 1, 0, WaypointPath)) {
				ListDispose(WaypointList); WaypointList=NULL;	// Or ListDisposePtrList() ?
				NbWP=0;
				WaypointList = ListCreate(sizeof(tCoord));
				
				File = fopen (WaypointPath, "r");
				if (File==NULL) { MessagePopup("Error", "Could not read file"); return 0; }
				
				while (NULL!=fgets (Buff, 255, File)) {
					if (Buff[0]=='#' or strlen(Buff)<4) continue;
					Buff[strlen(Buff)-1]='\0';
//					Nb = sscanf(Buff, "%c,%s,%s,%s", &Disp, sXLat, sYLon, Label);
					Disp=strtok(Buff, ",")[0];
					sXLat=strtok(NULL, ",");
					sYLon=strtok(NULL, ",");
					Label=strtok(NULL, ",");
					if (sXLat!=NULL and sYLon!=NULL and Label!=NULL) {
						Waypoint.Disp=(toupper(Disp)=='Y' or Disp=='1');
						if (Waypoint.Grid=(strchr(sXLat, '.')!=NULL)) {
							if (!UsingGrid() and !ErrorAlreadyFound) {
								MessagePopup("Warning!", "Your Waypoint file contains Lat/Lon coordinates\n"
								"but you haven't loaded a grid definition file.\n"
								"Those waypoints will be ignored");
								ErrorAlreadyFound=TRUE;
							}
							Waypoint.Lat=atof(sXLat);
							Waypoint.Lon=atof(sYLon);
						} else {
							Waypoint.X=atoi(sXLat);
							Waypoint.Y=atoi(sYLon);
						}
						Waypoint.Label = malloc(255);
						strcpy(Waypoint.Label, Label);
						ListInsertItem(WaypointList, &Waypoint, END_OF_LIST);
						NbWP++;
					}
				}
				fclose(File);
				ChangedWP=FALSE;
				DeleteTableRows (PnlWP, WP_TABLE, 1, -1);
				NeedViewUpdate=TRUE;
				if (AutoUpdate) CreateView();
				else DisplayWaypoints();
			}

			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SaveWaypoints (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tCoord Waypoint;
	FILE *File;
	int Status, i;
	
	switch (event) {
		case EVENT_COMMIT:
			Status = FileSelectPopup ("", "*.wpt", "*.wpt;*.csv", "Save waypoint list",
									  VAL_SAVE_BUTTON, 0, 0, 1, 1, WaypointPath);
			if (Status==VAL_EXISTING_FILE_SELECTED or Status==VAL_NEW_FILE_SELECTED) {
				File = fopen (WaypointPath, "w");
				fprintf(File, "# Waypoint list of LargeImage.exe\n"
								"# Display Y/N, Lat or X, Lon or Y, Label\n");
				for (i=0; i<NbWP; i++) {
					ListGetItem (WaypointList, &Waypoint, i+1);
					if (Waypoint.Grid)
						fprintf(File, "%c,%.6f,%.6f,%s\n", 
							Waypoint.Disp?'Y':'N', Waypoint.Lat, Waypoint.Lon, Waypoint.Label);
					else fprintf(File, "%c,%d,%d,%s\n", 
							Waypoint.Disp?'Y':'N', Waypoint.X, Waypoint.Y, Waypoint.Label);
				}
				fclose(File);
				ChangedWP=FALSE;
			}
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_WaypointColor (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Err;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &WaypointColor);
			Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "WaypointColor", *(unsigned long*)&WaypointColor, 1);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Color", "Color of the text labels for the waypoints");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_WaypointBcg (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Err;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &WaypointBcgColor);
			Err=WT_RegWriteULong (WT_KEY_HKCU, RegPath, "WaypointBcgColor", *(unsigned long*)&WaypointBcgColor, 1);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Color", "Background color for the waypoints");
			break;
	}
	return 0;
}
