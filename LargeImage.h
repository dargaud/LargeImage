/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2005. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  CTRL                            1       /* callback function: cbp_Ctrl */
#define  CTRL_MOVE_UP_LEFT               2       /* callback function: cb_Move */
#define  CTRL_MOVE_UP                    3       /* callback function: cb_Move */
#define  CTRL_MOVE_UP_RIGHT              4       /* callback function: cb_Move */
#define  CTRL_MOVE_LEFT                  5       /* callback function: cb_Move */
#define  CTRL_UPDATE_VIEW                6       /* callback function: cb_UpdateView */
#define  CTRL_MOVE_RIGHT                 7       /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN_LEFT             8       /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN                  9       /* callback function: cb_Move */
#define  CTRL_MOVE_DOWN_RIGHT            10      /* callback function: cb_Move */
#define  CTRL_ZOOM                       11      /* callback function: cb_Zoom */
#define  CTRL_X_MIN                      12      /* callback function: cb_MinMax */
#define  CTRL_X_MAX                      13      /* callback function: cb_MinMax */
#define  CTRL_CLICK_X                    14      /* callback function: cb_Click */
#define  CTRL_Y_MIN                      15      /* callback function: cb_MinMax */
#define  CTRL_Y_MAX                      16      /* callback function: cb_MinMax */
#define  CTRL_CLICK_Y                    17      /* callback function: cb_Click */
#define  CTRL_VALUE                      18      /* callback function: cb_Click */
#define  CTRL_THUMBNAIL                  19      /* callback function: cb_Thumbnail */
#define  CTRL_VIEW_PORTION               20
#define  CTRL_THUMB_MINMAX               21      /* callback function: cb_Extrema */
#define  CTRL_VIEW_MINMAX                22      /* callback function: cb_Extrema */
#define  CTRL_CLICK_LAT_LON              23      /* callback function: cb_Extrema */
#define  CTRL_QUIT                       24      /* callback function: cb_Quit */
#define  CTRL_TIMER_ARGV                 25      /* callback function: cb_Argv */

#define  GRID                            2
#define  GRID_LoadGrid                   2       /* callback function: cb_LoadGrid */
#define  GRID_FILENAME                   3
#define  GRID_TEXTMSG                    4
#define  GRID_TEXTLAT                    5
#define  GRID_TEXTLON                    6
#define  GRID_LAT_INTERVAL               7       /* callback function: cb_Grid */
#define  GRID_LON_INTERVAL               8       /* callback function: cb_Grid */
#define  GRID_LAT_MIN                    9       /* callback function: cb_Grid */
#define  GRID_LON_MIN                    10      /* callback function: cb_Grid */
#define  GRID_LAT_MAX                    11      /* callback function: cb_Grid */
#define  GRID_LON_MAX                    12      /* callback function: cb_Grid */
#define  GRID_LAT_PREF                   13      /* callback function: cb_Grid */
#define  GRID_LON_PREF                   14      /* callback function: cb_Grid */
#define  GRID_COLOR                      15      /* callback function: cb_Grid */
#define  GRID_BCG_COLOR                  16      /* callback function: cb_Grid */
#define  GRID_UP_LEFT_X                  17      /* callback function: cb_Grid */
#define  GRID_UP_LEFT_Y                  18      /* callback function: cb_Grid */
#define  GRID_LOW_RIGHT_X                19      /* callback function: cb_Grid */
#define  GRID_LOW_RIGHT_Y                20      /* callback function: cb_Grid */
#define  GRID_LABEL                      21      /* callback function: cb_Grid */
#define  GRID_DRAW                       22      /* callback function: cb_Grid */
#define  GRID_OK                         23      /* callback function: cb_GridOK */
#define  GRID_CANCEL                     24      /* callback function: cb_GridCancel */

#define  HIST                            3       /* callback function: cbp_Histograms */
#define  HIST_HISTF                      2       /* callback function: cb_HistF */
#define  HIST_HISTV                      3       /* callback function: cb_HistV */
#define  HIST_HISTT                      4       /* callback function: cb_HistT */
#define  HIST_CLOSE                      5       /* callback function: cb_CloseHist */

#define  MAP                             4
#define  MAP_FILENAME                    2
#define  MAP_SIZE                        3
#define  MAP_PIXDEPTH                    4       /* callback function: cb_MappingPixDepth */
#define  MAP_POSSIBLE_WH                 5       /* callback function: cb_MappingPossibleWH */
#define  MAP_WIDTH                       6       /* callback function: cb_MappingWidth */
#define  MAP_HEIGHT                      7       /* callback function: cb_MappingHeight */
#define  MAP_SKIP_HEADER                 8       /* callback function: cb_MappingSkip */
#define  MAP_SKIP_FOOTER                 9       /* callback function: cb_MappingSkipF */
#define  MAP_ALIGN                       10      /* callback function: cb_MappingAlign */
#define  MAP_SWAP                        11      /* callback function: cb_MappingSwap */
#define  MAP_EFFECT                      12      /* callback function: cb_MappingEffect */
#define  MAP_MISSING                     13      /* callback function: cb_MissingColor */
#define  MAP_TEXTMSG                     14
#define  MAP_XX_SCALE                    15      /* callback function: cb_MappingTransform */
#define  MAP_YX_SCALE                    16      /* callback function: cb_MappingTransform */
#define  MAP_X_OFFSET                    17      /* callback function: cb_MappingTransform */
#define  MAP_XY_SCALE                    18      /* callback function: cb_MappingTransform */
#define  MAP_YY_SCALE                    19      /* callback function: cb_MappingTransform */
#define  MAP_Y_OFFSET                    20      /* callback function: cb_MappingTransform */
#define  MAP_INTERPOLATE                 21      /* callback function: cb_MappingInterpolate */
#define  MAP_OK                          22      /* callback function: cb_MappingOK */
#define  MAP_CANCEL                      23      /* callback function: cb_MappingCancel */

#define  OGL                             5       /* callback function: cbp_OGL */
#define  OGL_CLOSE                       2       /* callback function: cb_OglClose */
#define  OGL_TEXTMSG                     3
#define  OGL_PICTURE                     4

#define  OPT                             6
#define  OPT_AUTO_UPDATE                 2       /* callback function: cb_OptionsHelp */
#define  OPT_STRETCH_THRESHOLD           3       /* callback function: cb_OptionsHelp */
#define  OPT_STRETCH_IGNORE              4       /* callback function: cb_OptionsHelp */
#define  OPT_THUMB_W                     5       /* callback function: cb_OptionsHelp */
#define  OPT_THUMB_H                     6       /* callback function: cb_OptionsHelp */
#define  OPT_VIEW_W                      7       /* callback function: cb_OptionsHelp */
#define  OPT_VIEW_H                      8       /* callback function: cb_OptionsHelp */
#define  OPT_OGL_W                       9       /* callback function: cb_OptionsHelp */
#define  OPT_OGL_H                       10      /* callback function: cb_OptionsHelp */
#define  OPT_BOX                         11
#define  OPT_TEXT                        12
#define  OPT_BACKGROUND                  13
#define  OPT_ARROW                       14
#define  OPT_ZBOX                        15      /* callback function: cb_OptionsHelp */
#define  OPT_SCALE_WIDTH                 16      /* callback function: cb_OptionsHelp */
#define  OPT_JPG_COMP                    17      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_COMP                    18      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_GAMMA                   19      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_RESOLUTION              20      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_AUTHOR                  21      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_DESCRIPTION             22      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_INTERLACE               23      /* callback function: cb_OptionsHelp */
#define  OPT_PNG_ALPHA32                 24      /* callback function: cb_OptionsHelp */
#define  OPT_CANCEL                      25      /* callback function: cb_OptionsCancel */
#define  OPT_OK                          26      /* callback function: cb_OptionsOK */

#define  PANEL                           7
#define  PANEL_VIEWMIN                   2       /* callback function: cb_Extrema */
#define  PANEL_SCALE                     3
#define  PANEL_VIEWMAX                   4       /* callback function: cb_Extrema */
#define  PANEL_THUMBMIN                  5       /* callback function: cb_Extrema */
#define  PANEL_THUMBMAX                  6       /* callback function: cb_Extrema */

#define  PROF                            8       /* callback function: cbp_Profile */
#define  PROF_PROPERTIES                 2       /* callback function: cb_ProfileProperties */
#define  PROF_CLOSE                      3       /* callback function: cb_ProfileClose */
#define  PROF_PROFILE                    4
#define  PROF_HELP                       5

#define  PROP                            9       /* callback function: cbp_Properties */
#define  PROP_FILENAME                   2
#define  PROP_SIZE                       3
#define  PROP_PIXDEPTH                   4       /* callback function: cb_PixDepth */
#define  PROP_POSSIBLE_WH                5       /* callback function: cb_PossibleWH */
#define  PROP_WIDTH                      6       /* callback function: cb_Width */
#define  PROP_HEIGHT                     7       /* callback function: cb_Height */
#define  PROP_SKIP_HEADER                8       /* callback function: cb_Skip */
#define  PROP_SKIP_FOOTER                9       /* callback function: cb_SkipF */
#define  PROP_ALIGN                      10      /* callback function: cb_Align */
#define  PROP_SWAP                       11      /* callback function: cb_Swap */
#define  PROP_SCALING                    12      /* callback function: cb_Scaling */
#define  PROP_SIGNED                     13      /* callback function: cb_Signed */
#define  PROP_EFFECT                     14      /* callback function: cb_Effect */
#define  PROP_UNITPERPIX                 15      /* callback function: cb_OptionsUnit */
#define  PROP_UNIT                       16      /* callback function: cb_OptionsUnit */
#define  PROP_VERTUNIT                   17      /* callback function: cb_OptionsUnit */
#define  PROP_ARROW_TEXT                 18      /* callback function: cb_OptionsUnit */
#define  PROP_ARROW_X                    19      /* callback function: cb_OptionsUnit */
#define  PROP_ARROW_Y                    20      /* callback function: cb_OptionsUnit */
#define  PROP_OK                         21      /* callback function: cb_PropOK */
#define  PROP_CANCEL                     22      /* callback function: cb_Cancel */

#define  VIEW                            10      /* callback function: cbp_View */
#define  VIEW_MAP                        2       /* callback function: cb_View */
#define  VIEW_VIEW                       3       /* callback function: cb_View */

#define  WP                              11      /* callback function: cbp_Waypoints */
#define  WP_SAVE_WAYPOINTS               2       /* callback function: cb_SaveWaypoints */
#define  WP_LOAD_WAYPOINTS               3       /* callback function: cb_LoadWaypoints */
#define  WP_OK                           4       /* callback function: cb_WaypointsOK */
#define  WP_TABLE                        5       /* callback function: cb_Table */
#define  WP_COLOR                        6       /* callback function: cb_WaypointColor */
#define  WP_BCG_COLOR                    7       /* callback function: cb_WaypointBcg */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU                            1
#define  MENU_FILE                       2
#define  MENU_FILE_OPEN                  3       /* callback function: cbm_Open */
#define  MENU_FILE_PROPERTIES            4       /* callback function: cbm_Properties */
#define  MENU_FILE_OPTIONS               5       /* callback function: cbm_Options */
#define  MENU_FILE_GRID                  6       /* callback function: cbm_Grid */
#define  MENU_FILE_WAYPOINTS             7       /* callback function: cbm_Waypoints */
#define  MENU_FILE_SEPARATOR_3           8
#define  MENU_FILE_SAVETHUMBAS           9       /* callback function: cbm_SaveThumbnailAs */
#define  MENU_FILE_PRINTTHUMB            10      /* callback function: cbm_PrintThumbnail */
#define  MENU_FILE_COPYTHUMB             11      /* callback function: cbm_CopyThumbnail */
#define  MENU_FILE_SEPARATOR_4           12
#define  MENU_FILE_SAVEVIEWAS            13      /* callback function: cbm_SaveViewAs */
#define  MENU_FILE_PRINTVIEW             14      /* callback function: cbm_PrintView */
#define  MENU_FILE_COPYVIEW              15      /* callback function: cbm_CopyView */
#define  MENU_FILE_SEPARATOR_1           16
#define  MENU_FILE_OGL                   17      /* callback function: cbm_OGL */
#define  MENU_FILE_MAPPING               18      /* callback function: cbm_ApplyMapping */
#define  MENU_FILE_SAVEOGLAS             19      /* callback function: cbm_SaveOglAs */
#define  MENU_FILE_PRINTOGL              20      /* callback function: cbm_PrintOgl */
#define  MENU_FILE_COPYOGL               21      /* callback function: cbm_CopyOGL */
#define  MENU_FILE_SEPARATOR_2           22
#define  MENU_FILE_QUIT                  23      /* callback function: cbm_Quit */
#define  MENU_FILTER                     24
#define  MENU_FILTER_SKIP                25      /* callback function: cbm_Skip */
#define  MENU_FILTER_RESAMPLE            26      /* callback function: cbm_Resample */
#define  MENU_FILTER_SEP                 27
#define  MENU_FILTER_GREY_PALETTE        28      /* callback function: cbm_GreyPalette */
#define  MENU_FILTER_LOAD_PALETTE        29      /* callback function: cbm_LoadPalette */
#define  MENU_FILTER_HISTOGRAMS          30      /* callback function: cbm_Histograms */
#define  MENU_FILTER_SEPARATOR           31
#define  MENU_HELP                       32
#define  MENU_HELP_QUICKHELP             33      /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE               34      /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                 35      /* callback function: cbm_About */


     /* Callback Prototypes: */ 

int  CVICALLBACK cb_Align(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Argv(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Cancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Click(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_CloseHist(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Effect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Extrema(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Grid(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GridCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GridOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Height(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HistV(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_LoadGrid(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_LoadWaypoints(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingAlign(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingEffect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingHeight(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingInterpolate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingPixDepth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingPossibleWH(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingSkip(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingSkipF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingSwap(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingTransform(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MappingWidth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MinMax(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MissingColor(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Move(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OglClose(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OptionsCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OptionsHelp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OptionsOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_OptionsUnit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PixDepth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PossibleWH(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ProfileClose(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ProfileProperties(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PropOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SaveWaypoints(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Scaling(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Signed(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Skip(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SkipF(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Swap(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Table(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Thumbnail(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_UpdateView(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_View(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_WaypointBcg(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_WaypointColor(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_WaypointsOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Width(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Zoom(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_ApplyMapping(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyOGL(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyThumbnail(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyView(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_GreyPalette(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Grid(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Histograms(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_LoadPalette(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_OGL(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Open(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Options(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_PrintOgl(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_PrintThumbnail(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_PrintView(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Properties(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Resample(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveOglAs(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveThumbnailAs(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveViewAs(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Skip(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Waypoints(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK cbp_Ctrl(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Histograms(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_OGL(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Profile(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Properties(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_View(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Waypoints(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
