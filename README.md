PROGRAM:   LargeImage.exe  

VERSION:   1.6  

COPYRIGHT: 2002-2005 Guillaume Dargaud - Freeware - GPL  

PURPOSE:   Graphic viewer for very large binary matrix files and basic cartography.  

INSTALL:   run SETUP.  

HELP:      available by right-clicking an item on the user interface.  

TUTORIAL:  http://www.gdargaud.net/Hack/LargeImage.html  

KNOWN BUGS: The UIR is not very standard. Sorry.  
            If you double click on the 3D display (instead of simple click),  
            sometimes you cannot close the [Properties] popup.  

RELIES:    This code relies on PNG, JPEG and MAPX code (GPL license)  


MINI TUTORIAL  

Example of use with the Radarsat data:  
1 - Find the files ramp200dem_osu_v2.bin and ant125mV2.bin  
If you received this program on a DVD, the files are on it, otherwise go download them from some NASA website.

2 - Install the LargeImage Program. It's either on the DVD or go download it from http://www.gdargaud.net/Hack/LargeImage.html  
Warning, you need at least version 1.5 for the following tutorial.  
To speed up the program you should copy the 2 large files from the DVD to your hard disk.

3 - Example of 2D display.  
Launch LargeImage from your [Start][Programs] menu.  
Open ant125mV2.bin  
Specify the following parameter in the popup box:  
	[Pixel structure]: 8 bits greyscale  
	[Possible WxH]:    48333x41779  
	Leave the rest off and click OK.  
You should see a thumbnail of Antarctica.  
Go in [File][Options] and set the following:  
	[Units/pix]: 0.125  
	[Unit]:      km  
	[Arrow To]:  S/25393/19250  
	Leave the rest and click OK.  
Click in the center and put 20 in the [Reduce] box.  
Click the [W] button to update the view (the vie[W] !)  
You can enlarge the view box (click [W] again to redraw).  
Enhance the view with [Filter][1x1 filters][Histogram Stretch]  
Now move around either with the keyboard numpad keys   
or with the mouse (double-click to center, double-right-click to zoom)  
To zoom out, put a high value in the [Reduce] box or type [-] on the view.  

Interest points: find South Pole; lake Vostok; Berkner Island...  


4 - Example of 3D display.  
Launch LargeImage from your [Start][Programs] menu.  
Open ramp200dem_osu_v2.bin  
Specify the following parameter in the popup box:  
	[Pixel structure]: 16 bits greyscale  
	[Possible WxH]:    28680x24580  
	[Swap bytes]:      on  
	[Scaling]:         on  
	Leave the rest and click OK.  
You should see a thumbnail of Antarctica.  
Go in [File][Options] and set the following:  
	[Units/pix]:     0.2  
	[Unit]:          km  
	[Vertical Unit]: m  
	[Arrow To]:  S/14345/12281  
Click in the center and put 20 in the [Reduce] box.  
Click the [W] button to update the view (the vie[W] !)  
You can enlarge the view box, but keep it small for fast plotting.  

Now start the 3D view with [File][3D view]  
You can enlarge the 3D view box.  
Use Ctrl+MouseMove to enlarge the plot, and MouseMove to rotate it.  
Right-click to change many of the options of the 3D display.  
If you are lost, just select [3D][Towards XY plane] and [3D][Auto] or press Ctrl-R.  
The colors are black at the lowest level and white at the highest.  
If the vertical scale doesn't seem right,  
change it in [File][Options][ZBox] (10% is the default, smaller is flatter).  

You can still move around with the normal View,  
either with the keyboard numpad keys  
or with the mouse (double-click to center, double-right-click to zoom)  
To zoom out, put a high value in the [Reduce] box or type [-] on the view.  
If you have large windows, move from one to the other with Ctrl-Tab.  

Interest points: find Mt Vinson; Dome C; Ross Island...  


5 - Example of 3D display with color mapping.  
Do (4) above to the end.  
Then select [File][Apply file mapping] and open ant125mV2.bin  
Specify the following parameter in the popup box:  
	[Pixel structure]:    8 bits greyscale  
	[Possible WxH]:       48333x41779  
	[Sxx / Syx / Ox]:     1.6 / 0 / 2440  
	[Sxy / Syy / Oy]:     0 / 1.6 / -400  
	[Interpolate Colors]: On  
	Leave the rest and click OK.  
You see the same 3D view as (4)  
but with the radarsat image of (3) mapped on top.  

Interest points: find the Dry Valleys, the Lambert Glacier...  



That's all folks.
